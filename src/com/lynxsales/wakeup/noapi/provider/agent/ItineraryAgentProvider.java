package com.lynxsales.wakeup.noapi.provider.agent;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.provider.SalesContract;
import com.lynx.sales.bundle.provider.SalesContract.Sales;
import com.lynx.sales.bundle.provider.agent.AbstractProviderAgent;

public class ItineraryAgentProvider extends AbstractProviderAgent{

	public static final Uri CONTENT_URI = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "itineraries" ).build();
	
	private static final int ALL_ITINERARIES  		 = 1;
	private static final int SINGLE_ITINERARY 		 = 2;
	private static final int ALL_ITINERARY_ROUTES 	 	 = 3;
	private static final int SINGLE_ITINERARY_ROUTE	 	 = 4;
	private static final int SELLER_ITINERARY 		 = 5;
	private static final int LIMITED_ROUTES 		 = 6;
	private static final int FILTERED_ROUTES 		 = 7;
	
	private SQLiteDatabase db;
	private static UriMatcher matcher;

	static {
		matcher = new UriMatcher(UriMatcher.NO_MATCH);
		matcher.addURI(Sales.PROVIDER_AUTHORITY, "itineraries", ALL_ITINERARIES);
		matcher.addURI(Sales.PROVIDER_AUTHORITY, "itineraries/seller/#", SELLER_ITINERARY);
		matcher.addURI(Sales.PROVIDER_AUTHORITY, "itineraries/#", SINGLE_ITINERARY);
		matcher.addURI(Sales.PROVIDER_AUTHORITY, "itineraries/routes/#", ALL_ITINERARY_ROUTES);
	        matcher.addURI("com.lynx.sales.bundle.authority.sales.provider", "itineraries/routes/#/*", 	FILTERED_ROUTES);
		matcher.addURI("com.lynx.sales.bundle.authority.sales.provider", "itineraries/routes/#/*/*", 	LIMITED_ROUTES);
	}

	public ItineraryAgentProvider(Context context) {
		super(context);
	}

	@Override
	public int delete(Uri uri, String selection, String[] seelctionArgs) 
	{
		int deletedId = -1;
		
		db = Database.getWritableDatabase(context);		
		
		int matchID = matcher.match(uri);
		if( ALL_ITINERARY_ROUTES == matchID ){
			deletedId = db.delete( "routes", "itinerary_id='"+  uri.getLastPathSegment() + "'", null);
		} 
		else if(SELLER_ITINERARY == matchID)	
		{
			deletedId = db.delete( "itineraries", "seller_id='"+  uri.getLastPathSegment() + "'", null);
		} 
		else if (SINGLE_ITINERARY == matchID)
		{
			deletedId = db.delete( "itineraries", "id='"+  uri.getLastPathSegment() + "'", null);
		}
		return deletedId;
	}

	@Override
	public String getType(Uri uri) {

		
		switch( matcher.match(uri) ){
		
		case ALL_ITINERARIES :
			return "vnd.android.cursor.dir/vnd.com.lynx.sales.itineraries";
		case SELLER_ITINERARY:
			return "vnd.android.cursor.item/vnd.com.lynx.sales.itineraries";	
		case ALL_ITINERARY_ROUTES :
			return "vnd.android.cursor.dir/vnd.com.lynx.sales.itineraries.routes";
		case SINGLE_ITINERARY_ROUTE:
			return "vnd.android.cursor.item/vnd.com.lynx.sales.itineraries.routes";	
		case LIMITED_ROUTES:
			return "vnd.android.cursor.item/vnd.com.lynx.sales.itineraries.routes";	
		case FILTERED_ROUTES:
			return "vnd.android.cursor.item/vnd.com.lynx.sales.itineraries.routes";	

		}
		throw new UnsupportedOperationException("Not supported URI");
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {

		db = Database.getWritableDatabase(getContext());

		String table = "";
		if (matcher.match(uri) == ALL_ITINERARIES) {
			table = "itineraries";
		} else if (matcher.match(uri) == ALL_ITINERARY_ROUTES) {
			table = "routes";
		}

		long id  = db.insert(table, null, values);
		
		if (id > -1) {
			Uri insertedId = ContentUris.withAppendedId(CONTENT_URI, id);
			return insertedId;
		}
		return null;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArguments, String sortOrder) {

		//Log.e("Itinerary Provider: *******", uri.toString());

		String table = "";
		Boolean usePersonalizedQuery = false;
		SQLiteDatabase db = Database.getReadableDatabase(this.context);
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
		Cursor cursor = null;
		
		int matchID = matcher.match(uri);
		
		switch (matchID) {
		case SELLER_ITINERARY:
			table = "itineraries";
			builder.appendWhere("seller_id='" + uri.getLastPathSegment() + "'");
			break;
		case SINGLE_ITINERARY:
			table = "itineraries";
			builder.appendWhere("id='" + uri.getLastPathSegment() + "'");
			break;
		case ALL_ITINERARY_ROUTES:
			table = "routes";
			builder.appendWhere("itinerary_id='" + uri.getPathSegments().get(2) + "'");
			break;
		case LIMITED_ROUTES:
			table = "routes";
			usePersonalizedQuery = true;
			//builder.appendWhere("itinerary_id='" + uri.getPathSegments().get(2) + "'");
			cursor = db.rawQuery("select * from " + table + " where itinerary_id='" + uri.getPathSegments().get(2) + "' order by id limit " + uri.getPathSegments().get(3) + " offset " + uri.getPathSegments().get(4), null);
			break;
			
		case FILTERED_ROUTES:
			table = "routes";
			usePersonalizedQuery = true;
			//builder.appendWhere("itinerary_id='" + uri.getPathSegments().get(2) + "'");
			String query = "select * from  " + table + "   where itinerary_id='" + uri.getPathSegments().get(2) + "' AND lazzy_client_id like '%" + uri.getPathSegments().get(3) + "%' or client_name like '%" + uri.getPathSegments().get(3) + "%' order by id ";
			cursor = db.rawQuery(query, null);
			break;
		}

		
		if(cursor == null &&  usePersonalizedQuery == false)
		{
		    builder.setTables(table);
		    cursor = builder.query(Database.getReadableDatabase(getContext()),
				projection, selection, selectionArguments, null, null,
				sortOrder);
		}
		
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] seelctionArgs) {
		return 0;
	}

	@Override
	public boolean resolve(Uri uri) {
		return -1 < matcher.match(uri);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArguments, String sortOrder,
			int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

}

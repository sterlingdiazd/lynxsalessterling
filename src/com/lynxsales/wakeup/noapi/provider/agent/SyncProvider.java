package com.lynxsales.wakeup.noapi.provider.agent;
/*
 * Decompiled with CFR 0_92.
 * 
 * Could not load the following classes:
 *  android.content.ContentUris
 *  android.content.ContentValues
 *  android.content.Context
 *  android.content.UriMatcher
 *  android.database.Cursor
 *  android.database.sqlite.SQLiteDatabase
 *  android.database.sqlite.SQLiteQueryBuilder
 *  android.net.Uri
 *  android.net.Uri$Builder
 *  java.lang.CharSequence
 *  java.lang.Exception
 *  java.lang.String
 *  java.lang.UnsupportedOperationException
 */
 
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.provider.SalesContract;
import com.lynx.sales.bundle.provider.agent.AbstractProviderAgent;
 
public class SyncProvider extends AbstractProviderAgent {
    public static final Uri CONTENT_URI = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("sync").build();
    private static final int SYNC_ALL = 2;
    private static final int SYNC_SINGLE = 1;
    private static UriMatcher matcher = new UriMatcher(-1);
 
    static {
        matcher.addURI("com.lynx.sales.bundle.authority.sales.provider", "sync", 2);
        matcher.addURI("com.lynx.sales.bundle.authority.sales.provider", "sync/*", 1);
    }
 
    public SyncProvider(Context context) {
        super(context);
    }
 
    @Override
    public int delete(Uri uri, String string, String[] arrstring) {
        int n = -1;
        SQLiteDatabase sQLiteDatabase = Database.getWritableDatabase(this.context);
        if (1 == matcher.match(uri)) {
            n = sQLiteDatabase.delete("sync_log", "id='" + uri.getLastPathSegment() + "'", null);
        }
        return n;
    }
 
    @Override
    public String getType(Uri uri) {
        switch (matcher.match(uri)) {
            default: {
                throw new UnsupportedOperationException("Not supported URI");
            }
            case 2: {
                return "vnd.android.cursor.dir/vnd.com.lynx.sales.sync";
            }
            case 1: 
        }
        return "vnd.android.cursor.item/vnd.com.lynx.sales.sync";
    }
 
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
    	long l = Database.getWritableDatabase(getContext()).insert("sync_log", null, contentValues);
        if (l > -1)
        {
            uri = ContentUris.withAppendedId(CONTENT_URI, l);
        }
        return uri;
    }
 
    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     */
    @Override
    public Cursor query(Uri uri, String[] arrstring, String string, String[] arrstring2, String string2) {
        try {
            SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
            switch (matcher.match(uri)) {
                default: {
                    break;
                }
                case 1: {
                    sQLiteQueryBuilder.appendWhere((CharSequence)("entity='" + uri.getLastPathSegment() + "'"));
                }
            }
            sQLiteQueryBuilder.setTables("sync_log");
            return sQLiteQueryBuilder.query(Database.getReadableDatabase(this.getContext()), arrstring, string, arrstring2, null, null, string2);
        }
        catch (Exception var7_7) {
            var7_7.printStackTrace();
            return null;
        }
    }
 

 
    @Override
    public boolean resolve(Uri uri) {
        if (-1 < matcher.match(uri)) {
            return true;
        }
        return false;
    }
 
    @Override
    public int update(Uri uri, ContentValues contentValues, String string, String[] arrstring) {
        return Database.getWritableDatabase(this.getContext()).update("sync_log", contentValues, string, arrstring);
    }

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArguments, String sortOrder,
			int start, int limit) {
		return null;
	}
}
 
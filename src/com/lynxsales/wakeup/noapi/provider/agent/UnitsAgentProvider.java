package com.lynxsales.wakeup.noapi.provider.agent;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.provider.SalesContract;
import com.lynx.sales.bundle.provider.SalesContract.Sales;
import com.lynx.sales.bundle.provider.agent.AbstractProviderAgent;
import static com.lynx.sales.bundle.dto.model.Constants.*;

public class UnitsAgentProvider extends AbstractProviderAgent{

	public static final Uri CONTENT_URI = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( UNITS ).build();
	
	private static final int ALL_UNITS  		 = 1;
	
	private static final int UNITS_BY_MATERIAL		 = 2;
	
/*	private static final int ALL_ITINERARY_ROUTES 	 = 3;
	private static final int SINGLE_ITINERARY_ROUTE	 = 4;
	private static final int SELLER_ITINERARY 		 = 5;*/
	
	private SQLiteDatabase db;
	private static UriMatcher matcher;

	static {
		matcher = new UriMatcher(UriMatcher.NO_MATCH);
		matcher.addURI(Sales.PROVIDER_AUTHORITY, UNITS, ALL_UNITS);
		matcher.addURI(Sales.PROVIDER_AUTHORITY, "units/#", UNITS_BY_MATERIAL);
		/*
		matcher.addURI(Sales.PROVIDER_AUTHORITY, "itineraries/seller/#", SELLER_ITINERARY);
		
		matcher.addURI(Sales.PROVIDER_AUTHORITY, "itineraries/routes/#", ALL_ITINERARY_ROUTES);
		*/
	}

	public UnitsAgentProvider(Context context) {
		super(context);
	}

	@Override
	public int delete(Uri uri, String selection, String[] seelctionArgs) 
	{
		int deletedId = -1;
		
		db = Database.getWritableDatabase(context);		
		
		int matchID = matcher.match(uri);
		if( ALL_UNITS == matchID ){
			deletedId = db.delete( UNITS, null, null);
		} 
		/*else if(SELLER_ITINERARY == matchID)	
		{
			deletedId = db.delete( "itineraries", "seller_id='"+  uri.getLastPathSegment() + "'", null);
		} 
		else if (SINGLE_ITINERARY == matchID)
		{
			deletedId = db.delete( "itineraries", "id='"+  uri.getLastPathSegment() + "'", null);
		}*/
		return deletedId;
	}

	@Override
	public String getType(Uri uri) {

		
		switch( matcher.match(uri) ){
		
		case ALL_UNITS :
			return "vnd.android.cursor.dir/vnd.com.lynx.sales.conditions";
		/*case SELLER_ITINERARY:
			return "vnd.android.cursor.item/vnd.com.lynx.sales.conditions";	*/
		}
		throw new UnsupportedOperationException("Not supported URI");
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {

		db = Database.getWritableDatabase(getContext());

		String table = "";
		if (matcher.match(uri) == ALL_UNITS) {
			table = UNITS;
		}/* else if (matcher.match(uri) == ALL_ITINERARY_ROUTES) {
			table = "routes";
		}*/

		long id  = db.insert(table, null, values);
		
		if (id > -1) {
			Uri insertedId = ContentUris.withAppendedId(CONTENT_URI, id);
			return insertedId;
		}
		return null;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArguments, String sortOrder) {

		//Log.e("Itinerary Provider: *******", uri.toString());

		String table = UNITS;

		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
		int matchID = matcher.match(uri);
		
		switch (matchID) {
		/*case SELLER_ITINERARY:
			table = "itineraries";
			builder.appendWhere("seller_id='" + uri.getLastPathSegment() + "'");
			break;*/
		case UNITS_BY_MATERIAL:
			builder.appendWhere("matnr='" + uri.getLastPathSegment() + "'");
			break;
		case ALL_UNITS:
			//builder.appendWhere("itinerary_id='" + uri.getPathSegments().get(2) + "'");
			break;
		default:
			break;
		}

		builder.setTables(table);
		return builder.query(Database.getReadableDatabase(getContext()),
				projection, selection, selectionArguments, "meinh", null,
				sortOrder);
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] seelctionArgs) {
		return 0;
	}

	@Override
	public boolean resolve(Uri uri) {
		return -1 < matcher.match(uri);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArguments, String sortOrder,
			int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

}

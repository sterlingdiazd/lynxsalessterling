package com.lynxsales.wakeup.noapi.provider.agent;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.provider.SalesContract;
import com.lynx.sales.bundle.provider.SalesContract.Sales;
import com.lynx.sales.bundle.provider.agent.AbstractProviderAgent;

public class ArchiveProvider extends AbstractProviderAgent{

	public static final Uri CONTENT_URI = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "archives" ).build();
	
	private static final int ALL_ARCHIVES     = 1;
	private static final int SINGLE_ARCHIVE   = 2;
	
	
	private static UriMatcher matcher;
	
	static{
		matcher = new UriMatcher( UriMatcher.NO_MATCH );
		matcher.addURI( Sales.PROVIDER_AUTHORITY, "archives",   ALL_ARCHIVES  );
		matcher.addURI( Sales.PROVIDER_AUTHORITY, "archives/#", SINGLE_ARCHIVE ); 
	}	
	
	public ArchiveProvider(Context context) {
		super(context);
	}

	@Override
	public String getType(Uri uri) {
		switch( matcher.match(uri) ){		
		case ALL_ARCHIVES :
			return "vnd.android.cursor.dir/vnd.com.lynx.sales.archives";
		case SINGLE_ARCHIVE:
			return "vnd.android.cursor.item/vnd.com.lynx.sales.archives";					
		}
		throw new UnsupportedOperationException( "Not supported URI" );
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) 
	{
		SQLiteDatabase db = Database.getWritableDatabase( getContext() );
		
		String table = "archives";
		
		long id = db.insert( table, null, values);
		if( id > -1 ){			
			Uri insertedId = ContentUris.withAppendedId(CONTENT_URI, id);						
			return insertedId;
		}
		return null;	
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) 
	{
		SQLiteDatabase db = Database.getWritableDatabase( getContext() );
		String table = "archives";
		int updateID = db.update(table, values, selection, selectionArgs);
		return updateID;
	}

	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArguments, String sortOrder) 
	{		
		//Log.e( "User Provider: *******", uri.toString() );
		
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
		switch ( matcher.match(uri)) {
		case  SINGLE_ARCHIVE:
			builder.appendWhere( "id='"+uri.getLastPathSegment()+"'" );
			break;
		default:
			break;
		}
		builder.setTables( "archives" );				
		return builder.query( Database.getReadableDatabase(getContext()), projection, selection, selectionArguments, null, null, sortOrder);
	}

	
	
	@Override
	public int delete(Uri uri, String selection, String[] seelctionArgs) {
		int deletedId = -1;
		
		SQLiteDatabase db = Database.getWritableDatabase(context);		
				
		if( SINGLE_ARCHIVE == matcher.match(uri) ){
			deletedId = db.delete( "archives", "id='"+  uri.getLastPathSegment() + "'", null);
		}		
		return deletedId;
	}
	
	@Override
	public boolean resolve(Uri uri) {
		return -1 < matcher.match(uri);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArguments, String sortOrder,
			int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}
	
}

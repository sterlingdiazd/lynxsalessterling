package com.lynxsales.wakeup.noapi.provider.agent;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.provider.SalesContract;
import com.lynx.sales.bundle.provider.SalesContract.Sales;
import com.lynx.sales.bundle.provider.agent.AbstractProviderAgent;

public class ProductsAgentProvider extends AbstractProviderAgent
{

    public static final Uri CONTENT_URI = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath("products").build();

    private static final int ALL_PRODUCTS = 1;
    private static final int SINGLE_CLIENT_PRODUCTS = 2;
    private static final int PRODUCT_HAS_OFFER = 3;
    private static final int CENTER_STORE = 4;
    private static final int CENTER_STORE_MATERIAL = 5;
    private static final int FILTERED_PRODUCTS = 7;
    private static final int LIMITED_CENTER_STORE = 6;

    private static String client_id;

    private static UriMatcher matcher;

    static
    {
	matcher = new UriMatcher(UriMatcher.NO_MATCH);
	matcher.addURI(Sales.PROVIDER_AUTHORITY, "products", ALL_PRODUCTS);
	matcher.addURI(Sales.PROVIDER_AUTHORITY, "products/#", SINGLE_CLIENT_PRODUCTS);
	matcher.addURI(Sales.PROVIDER_AUTHORITY, "products/center/*/store/*/matnr/#", CENTER_STORE_MATERIAL);
	matcher.addURI(Sales.PROVIDER_AUTHORITY, "products/offer/#", PRODUCT_HAS_OFFER);
	matcher.addURI(Sales.PROVIDER_AUTHORITY, "products/center/*/store/*", CENTER_STORE);
	matcher.addURI(Sales.PROVIDER_AUTHORITY, "products/center/*/store/*/#/#", LIMITED_CENTER_STORE);
        matcher.addURI(Sales.PROVIDER_AUTHORITY, "products/center/*/store/*/#", FILTERED_PRODUCTS);
    }

    public ProductsAgentProvider(Context context)
    {
	super(context);
    }

    @Override
    public int delete(Uri uri, String selection, String[] seelctionArgs)
    {
	int deletedId = -1;

	SQLiteDatabase db = Database.getWritableDatabase(context);

	if (SINGLE_CLIENT_PRODUCTS == matcher.match(uri))
	{
	    deletedId = db.delete("products", "id_client='" + uri.getLastPathSegment() + "'", null);
	} else if (CENTER_STORE_MATERIAL == matcher.match(uri))
	{
	    deletedId = db.delete("products",
		    " werks like '%" + uri.getPathSegments().get(1) + "%'  and lgort like '%"
			    + uri.getPathSegments().get(3) + "%'    and  matnr ='" + uri.getPathSegments().get(5)
			    + "'  ",
		    null);
	} else if (ALL_PRODUCTS == matcher.match(uri))
	{
	    deletedId = db.delete("products", null, null);

	} else if (CENTER_STORE == matcher.match(uri))
	{
	    deletedId = db.delete("products", " werks like '%" + uri.getPathSegments().get(2) + "%'  and lgort like '%"
		    + uri.getPathSegments().get(4) + "%'  ", null);
	}

	return deletedId;
    }

    @Override
    public String getType(Uri uri)
    {
	switch (matcher.match(uri))
	{
	case ALL_PRODUCTS:
	    return "vnd.android.cursor.dir/vnd.com.lynx.sales.products";
	case SINGLE_CLIENT_PRODUCTS:
	    return "vnd.android.cursor.item/vnd.com.lynx.sales.products";
	case PRODUCT_HAS_OFFER:
	    return "vnd.android.cursor.item/vnd.com.lynx.sales.products";
	case CENTER_STORE:
	    return "vnd.android.cursor.item/vnd.com.lynx.sales.products";
	case CENTER_STORE_MATERIAL:
	    return "vnd.android.cursor.item/vnd.com.lynx.sales.products";
	}

	throw new UnsupportedOperationException("Not supported URI");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
	SQLiteDatabase db = Database.getWritableDatabase(getContext());

	String table = "products";

	// String[] selectionArgs = new String[]{values.getAsString("id")};
	// Cursor cursor = db.query(table, new String[]{"id"}, "id=?",
	// selectionArgs, null, null, null );
	// int deletedId = 0;
	// try {
	// deletedId = db.delete( "products", "id_client='"+
	// values.get(ID_CLIENT).toString() +"' and id='"+
	// values.get(ID).toString() +"' and center like '%"+
	// values.get(CENTER).toString() +"%'", null);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// cursor.close();
	long id = db.insert(table, null, values);

	if (id > -1)
	{
	    Uri insertedId = ContentUris.withAppendedId(CONTENT_URI, id);

	    return insertedId;
	}
	return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArguments, String sortOrder)
    {

	// Log.e( "Products Provider: *******", uri.toString() );

	String table = "";
	Boolean usePersonalizedQuery = false;
	SQLiteDatabase db = Database.getReadableDatabase(this.context);
	SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
	Cursor cursor = null;

	int matched = matcher.match(uri);
	switch (matched)
	{
	case ALL_PRODUCTS:
	    table = "products";
	    break;
	case SINGLE_CLIENT_PRODUCTS:
	    table = "products";
	    client_id = uri.getLastPathSegment();
	    builder.appendWhere("id_client='" + client_id + "'");
	    break;
	case PRODUCT_HAS_OFFER:
	    table = uri.getPathSegments().get(0);
	    builder.appendWhere("id= '" + uri.getLastPathSegment() + "' and id_client='" + client_id + "'");
	    break;
	case CENTER_STORE:
	    table = uri.getPathSegments().get(0);
	    builder.appendWhere(" werks like '%" + uri.getPathSegments().get(2) + "%' and lgort like '%"
		    + uri.getPathSegments().get(4) + "%'  ");
	    break;
	case CENTER_STORE_MATERIAL:
	    table = uri.getPathSegments().get(0);
	    builder.appendWhere(" werks like '%" + uri.getPathSegments().get(2) + "%' and lgort like '%"
		    + uri.getPathSegments().get(4) + "%'   and matnr like '%" + uri.getPathSegments().get(6) + "%'");
	    break;
	    
	case LIMITED_CENTER_STORE:
	    table = uri.getPathSegments().get(0);
	    String queryLimit = "select * from " + table + " where " + "werks" + " LIKE '%" + (String)uri.getPathSegments().get(2) + "%' AND " + "lgort" + " LIKE '%" + (String)uri.getPathSegments().get(4) + "%'" + " order by " + "id" + " " + " limit " + (String)uri.getPathSegments().get(5) + " offset " + (String)uri.getPathSegments().get(6) + " ";
	    cursor = db.rawQuery(queryLimit, null);
	    break;
	    
	case FILTERED_PRODUCTS:
	    table = uri.getPathSegments().get(0);
	    String queryFilter ="select * from " + table+ " where " + "werks" + " LIKE '%" + (String)uri.getPathSegments().get(2) + "%' AND " + "lgort" + " LIKE '%" + (String)uri.getPathSegments().get(4) + "%'" + " AND " + "matnr" + " like '%" + (String)uri.getPathSegments().get(5) + "%' or " + "maktx" + " like '%" + (String)uri.getPathSegments().get(5) + "%'" + " order by " + "id" + " ";
	    cursor = db.rawQuery(queryFilter, null);
	    break;
	}

	if(cursor == null &&  usePersonalizedQuery == false)
	{
	    builder.setTables(table);
	    cursor = builder.query(Database.getReadableDatabase(getContext()),
			projection, selection, selectionArguments, null, null,
			sortOrder);
	}
	
	return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] seelctionArgs)
    {
	return 0;
    }

    @Override
    public boolean resolve(Uri uri)
    {
	return -1 < matcher.match(uri);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArguments, String sortOrder,
	    int start, int limit)
    {
	// TODO Auto-generated method stub
	return null;
    }

}

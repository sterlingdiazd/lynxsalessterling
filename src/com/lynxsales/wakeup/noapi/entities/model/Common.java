package com.lynxsales.wakeup.noapi.entities.model;

import java.util.Collection;

public final class Common {


	
	
	public static final class Functions{
		public static final String join( String separator,Object...objects ){
			StringBuilder sb = new StringBuilder( "" );
			
			if( null != objects ){
				String token = "";
				for( Object item : objects ){
					sb.append( token ).append( item.toString() );
					token = separator;
				}					
			}
			return sb.toString();
		}		
		
		public static final String join( String separator, Collection< String > objects ){
			return join( separator, objects.toArray() );
		}
	}
}

package com.lynxsales.wakeup.noapi.entities.model;


import java.io.Serializable;

/**
 * Created by sterlingdiazd on 5/23/2014.
 */
public class Client implements Serializable
{
    private String id, cedula, name, lastname, email, direccion, regid, geolocalizacion, birthdate, cellphone;

    public Client(String id, String cedula, String name, String lastname, String email, String direccion, String regid, String geolocalizacion, String birthdate, String cellphone) {
        this.id = id;
        this.cedula = cedula;
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.direccion = direccion;
        this.regid = regid;
        this.geolocalizacion = geolocalizacion;
        this.birthdate = birthdate;
        this.cellphone = cellphone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getRegid() {
        return regid;
    }

    public void setRegid(String regid) {
        this.regid = regid;
    }

    public String getGeolocalizacion() {
        return geolocalizacion;
    }

    public void setGeolocalizacion(String geolocalizacion) {
        this.geolocalizacion = geolocalizacion;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }
}

package com.lynxsales.wakeup.noapi.entities;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ListWrapper<T extends List<? extends Object>> implements Serializable {

	T list;

	public ListWrapper() {
	}

	public T getList() {
		return list;
	}

	public void setList(T list) {
		this.list = list;
	}
}

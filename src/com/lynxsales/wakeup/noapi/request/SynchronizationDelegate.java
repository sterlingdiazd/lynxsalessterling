
package com.lynxsales.wakeup.noapi.request;

import java.lang.Character.UnicodeBlock;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lynx.sales.bundle.app.LynxApplication;
import com.lynx.sales.bundle.dto.model.ClientDataModel;
import com.lynx.sales.bundle.dto.model.CompanyDataModel;
import com.lynx.sales.bundle.dto.model.ConditionsDataModel;
import com.lynx.sales.bundle.dto.model.ItineraryDataModel;
import com.lynx.sales.bundle.dto.model.ProductDataModel;
import com.lynx.sales.bundle.dto.model.UnitsDataModel;
import com.lynx.sales.bundle.dto.model.UserDataModel;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.entities.ConditionEntity;
import com.lynx.sales.bundle.entities.Material;
import com.lynx.sales.bundle.entities.UnitEntity;
import com.lynx.sales.bundle.entities.UserEntity;
import com.lynx.sales.bundle.http.request.ContextException;
import com.lynx.sales.bundle.http.request.PhaseHandler;
import com.lynx.sales.bundle.http.request.ServiceLocator;
import com.lynx.sales.bundle.http.request.ServiceName;
import com.lynx.sales.bundle.http.request.ServiceStub;
import com.lynx.sales.bundle.http.request.SynchronizeRequest;
import com.lynx.sales.bundle.http.request.SynchronizeRequestDelegate;
import com.lynxsales.wakeup.activities.DownloadActivity;
import com.lynxsales.wakeup.utils.AlertDialogManager;
import com.lynxsales.wakeup.R;
import com.testflightapp.lib.TestFlight;

public class SynchronizationDelegate implements SynchronizeRequestDelegate<String>{

	private static final String TAG = SynchronizeRequest.class.getSimpleName();
	
	private PhaseHandler handler;
	private Activity activity;
	private String sellerID;
	private String jsonClients;
	private ArrayList<String> clientsIDS ;
	
	public SynchronizationDelegate( Activity activity ){
		this.activity = activity;
	}
	
	@Override
	public void performSynchronization(PhaseHandler handler, String... params) throws ContextException 
	{
		this.handler = handler;		
		this.sellerID = params[0];
		
		fetchSellerInfo( sellerID );
		fetchCompany();
		
		/*
		fetchClients();
		fetchItinerary(sellerID);
		
		fetchProducts();
		fetchConditions(sellerID);
		*/
		
		/**/
		/*		
		//fetchPreferences(sellerID);
		/*
		/*
		String clientsJson  = fetchItineraryClients( sellerID );
		ArrayList<String> clientsIDS = extractClients(clientsJson);
		fetchClientsInfo( clientsIDS );
		*/
	}	

	private void fetchPreferences( String sellerID ) throws ContextException
	{
		Log.e( TAG, activity.getResources().getString(R.string.sync_preferences) );
		
		if( null != handler )
			handler.onFeedback(activity.getResources().getString(R.string.sync_preferences) );
		
		
		ServiceStub service = ServiceLocator.lookup( ServiceName.PREFERENCES);
		if( null != service ){			
			String strUrl = service.getUrl() + service.resolveParams( "1", sellerID );
			
			URI url = URI.create(strUrl);
			HttpGet get = new HttpGet(url);
			HttpClient client = new DefaultHttpClient();
			HttpResponse response = null;

			try {
				response = client.execute(get);
				if( response == null ){
					TestFlight.log( "Missed [ "+strUrl+" ] to be synchronized" );
				}else{					

					ConditionsDataModel.deleteConditions(activity);
					String jsonResponse = EntityUtils.toString( response.getEntity() );
					List<ConditionEntity> conditions = new Gson().fromJson(jsonResponse.trim(), new TypeToken<List<ConditionEntity>>(){}.getType());
					ConditionsDataModel.saveConditions(activity, conditions);

				}

			} catch (Exception ex) {
				ex.printStackTrace();
				TestFlight.sendCrash(1L, "Error Trying to get synchronized",ex.toString());
				TestFlight.sendsCrashes();
				throw new ContextException( ex.toString() );
			}			
		}
	}
	
	private void fetchConditions( String sellerID ) throws ContextException
	{
		Log.e( TAG, activity.getResources().getString(R.string.sync_itinerary) );
		
		if( null != handler )
			handler.onFeedback(activity.getResources().getString(R.string.sync_prices_bonus) );
		
		
		ServiceStub service = ServiceLocator.lookup( ServiceName.CONDITIONS);
		if( null != service )
		{	
			String strUrl = service.getUrl() + service.resolveParams( "6", sellerID );
			
			URI url = URI.create(strUrl);
			HttpGet get = new HttpGet(url);
			HttpClient client = new DefaultHttpClient();
			HttpResponse response = null;

			try {
				response = client.execute(get);
				if( response == null )
				{
					TestFlight.log( "Missed [ "+strUrl+" ] to be synchronized" );
				}
				else
				{					

					ConditionsDataModel.deleteConditions(activity);
					String jsonResponse = EntityUtils.toString( response.getEntity() );
					List<ConditionEntity> conditions = new Gson().fromJson(jsonResponse.trim(), new TypeToken<List<ConditionEntity>>(){}.getType());
					ConditionsDataModel.saveConditions(activity, conditions);

				}

			} catch (Exception ex) {
				ex.printStackTrace();
				TestFlight.sendCrash(1L, "Error Trying to get synchronized",ex.toString());
				TestFlight.sendsCrashes();
				throw new ContextException( ex.toString() );
			}			
		}
	}
	
	private ArrayList<String> extractClients(String jsonClients) 
	{	
		ArrayList<String> clientsIDS = null;
		try 
		{
			JSONObject objClients = new JSONObject(jsonClients);
			JSONArray clients = objClients.getJSONArray("clients");
			clientsIDS =  new ArrayList<String>();
			for(int x = 0; x < clients.length(); x++)
			{
				JSONObject obj = (JSONObject) clients.get(x); 
				String client_id = obj.getString("id");
				clientsIDS.add(client_id);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return clientsIDS;
	}

	private void fetchCompany() 
	{
		Log.e( TAG, "Synchronizing Company..." );
		
		if( null != handler )
			handler.onFeedback( "Synchronizing Company..." );
		
		ServiceStub service = ServiceLocator.lookup( ServiceName.COMPANY);
		if( null != service )
		{			
			String strUrl = service.getUrl() + service.resolveParams( "1" );
			
			URI url = URI.create(strUrl);
			HttpGet get = new HttpGet(url);
			HttpClient client = new DefaultHttpClient();
			HttpResponse response = null;

			try {
				response = client.execute(get);
				if( response == null ){
					TestFlight.log( "Missed [ "+strUrl+" ] to be synchronized" );
				}else{					
					String jsonResponse = EntityUtils.toString( response.getEntity() );
					
					CompanyDataModel.saveCompany(activity, jsonResponse);
				}

			} catch (Exception ex) {
				ex.printStackTrace();
				TestFlight.sendCrash(1L, "Error Trying to get synchronized",ex.toString());
				TestFlight.sendsCrashes();
			}			
		}
	}

	private void fetchItinerary ( String sellerID ) throws ContextException{
		Log.e( TAG, activity.getResources().getString(R.string.sync_itinerary) );
		
		if( null != handler )
			handler.onFeedback(activity.getResources().getString(R.string.sync_itinerary) );
		
		
		ServiceStub service = ServiceLocator.lookup( ServiceName.ITINERARY );
		if( null != service )
		{			
			String strUrl = service.getUrl() + service.resolveParams( "2",sellerID );
			
			URI url = URI.create(strUrl);
			HttpGet get = new HttpGet(url);
			HttpClient client = new DefaultHttpClient();
			HttpResponse response = null;

			try {
				response = client.execute(get);
				if( response == null )
				{
					TestFlight.log( "Missed [ "+strUrl+" ] to be synchronized" );
				}
				else
				{					
					String jsonResponse = EntityUtils.toString( response.getEntity() );
					ItineraryDataModel.saveItinerary( activity,sellerID,jsonResponse, handler);
				}

			} catch (Exception ex) {
				ex.printStackTrace();
				TestFlight.sendCrash(1L, "Error Trying to get synchronized",ex.toString());
				TestFlight.sendsCrashes();
				throw new ContextException( ex.toString() );
			}			
		}
	}
	
	private String fetchItineraryClients( String sellerId ) throws ContextException
	{
		String jsonResponse = "";
		Log.e( TAG, activity.getResources().getString(R.string.sync_itinerary) );
		
		if( null != handler )
			handler.onFeedback(activity.getResources().getString(R.string.sync_itinerary) );
		
		
		ServiceStub service = ServiceLocator.lookup( ServiceName.ITINERARY_CLIENTS );
		if( null != service ){			
			String strUrl = service.getUrl() + service.resolveParams( "4",sellerId );
			
			URI url = URI.create(strUrl);
			HttpGet get = new HttpGet(url);
			HttpClient client = new DefaultHttpClient();
			HttpResponse response = null;

			try {
				response = client.execute(get);
				if( response == null ){
					TestFlight.log( "Missed [ "+strUrl+" ] to be synchronized" );
				}else{					
					jsonResponse = EntityUtils.toString( response.getEntity() );
					
					
					//ItineraryDataModel.saveItinerary( getContext(),sellerId,jsonResponse);
				}

			} catch (Exception ex) {
				ex.printStackTrace();
				TestFlight.sendCrash(1L, "Error Trying to get synchronized",ex.toString());
				TestFlight.sendsCrashes();
				throw new ContextException( ex.toString() );
			}			
		}
		return jsonResponse;
	}
	
	private void fetchClients() throws ContextException
	{
		Log.e( TAG, "Synchronizing itinerary clients..." );
		
		if( null != handler )
			handler.onFeedback( activity.getResources().getString(R.string.sync_clients));
		
		ServiceStub service = ServiceLocator.lookup( ServiceName.CUSTOMER);
		
		String strUrl = null;
		if(service != null)
		{
			strUrl = service.getUrl() + service.resolveParams( "1" );	
			URI url = URI.create(strUrl);
			Log.e("e", strUrl);
			HttpGet get = new HttpGet(url);
			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = null;

			String jsonResponse = null;
			List<ClientEntity> clients = new ArrayList<ClientEntity>();
			try {
				response = httpClient.execute(get);
				if( response == null ){
					TestFlight.log( "Missed [ "+strUrl+" ] to be synchronized" );
				}else{		
					ClientDataModel.deleteClients(activity);
					jsonResponse = EntityUtils.toString( response.getEntity() );				
					//Gson gson = new GsonBuilder().create();
					
					JSONArray clientsResponse = new JSONArray(jsonResponse);
					
					for( int x = 0; x < clientsResponse.length(); x++)
					{
						JSONObject jsonObject =  clientsResponse.getJSONObject((x));
						ClientEntity client = new Gson().fromJson(jsonObject.toString().trim(), new TypeToken<ClientEntity>(){}.getType());
						clients.add(client);
					}
					ClientDataModel.saveClient(activity, clients );
				}
			} 
			catch (Exception ex) 
			{
				ex.printStackTrace();
				TestFlight.sendCrash(1L, "Error Trying to get synchronized",ex.toString());
				TestFlight.sendsCrashes();
				throw new ContextException( ex.toString() );
			}		
		} else {
			Log.e("Service", " is null");
		}
		
		
		
	}
	
	private String fetchProducts( ) throws ContextException{	
		
		Log.e( TAG, "Synchronizing products..." );
		
		if( null != handler )
			handler.onFeedback(activity.getResources().getString(R.string.sync_products));
		
		ServiceStub service = ServiceLocator.lookup( ServiceName.PRODUCTS);
		
		String strUrl = service.getUrl() + service.resolveParams( "1" );
		
		URI url = URI.create(strUrl);
		HttpGet get = new HttpGet(url);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = null;

		String jsonResponse = null;
		
		try {
			response = client.execute(get);
			if( response == null ){
				TestFlight.log( "Missed [ "+strUrl+" ] to be synchronized" );
			}
			else
			{					
				jsonResponse = EntityUtils.toString( response.getEntity() );
				JSONArray productsResponse = new JSONArray(jsonResponse);
				
				ProductDataModel.deleteProducts(activity);
				UnitsDataModel.deleteUnits(activity);
				
				List<Material> materiales = new ArrayList<Material>();
				
				String tempCenter = "";
				
				for( int x = 0; x < productsResponse.length(); x++)
				{
					JSONObject jsonObject =  productsResponse.getJSONObject((x));
					String center = jsonObject.getString("center");
					
						JSONArray materials = jsonObject.getJSONArray("materials");
						
						for(int y = 0; y < materials.length(); y++)
						{
							JSONObject materialObject = (JSONObject) materials.get(y);
							String stringMaterials = materialObject.toString().trim();
							JSONArray unidades = materialObject.getJSONArray("unidades");
							Material material = new Gson().fromJson(stringMaterials, new TypeToken<Material>(){}.getType()); 
							
							List<UnitEntity> unitList = new Gson().fromJson(unidades.toString().trim(), new TypeToken<List<UnitEntity>>(){}.getType());
							material.setUnidades(unitList);
							materiales.add(material);
						}
						
						ProductDataModel.bulkSaveProducts(activity, materiales, center);
						materiales.clear();
					}
								
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			TestFlight.sendCrash(1L, "Error Trying to get synchronized",ex.toString());
			TestFlight.sendsCrashes();
			throw new ContextException( ex.toString() );
		}		
		
		return jsonResponse;
	}
	/*
	private String fetchItineraryProducts( String sellerId ) throws ContextException{	
		
		Log.e( TAG, "Synchronizing itinerary products..." );
		
		if( null != handler )
			handler.onFeedback( this.getContext().getResources().getString(R.string.sync_products));
		
		ServiceStub service = ServiceLocator.lookup( ServiceName.PRODUCTS );
		
		String strUrl = service.getUrl() + service.resolveParams( "3",sellerId );
		
		URI url = URI.create(strUrl);
		HttpGet get = new HttpGet(url);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = null;

		String jsonResponse = null;
		
		try {
			response = client.execute(get);
			if( response == null ){
				TestFlight.log( "Missed [ "+strUrl+" ] to be synchronized" );
			}else{					
				jsonResponse = EntityUtils.toString( response.getEntity() );				
				ProductDataModel.bulkSaveClients(getContext(), jsonResponse);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			TestFlight.sendCrash(1L, "Error Trying to get synchronized",ex.toString());
			TestFlight.sendsCrashes();
			throw new ContextException( ex.toString() );
		}		
		
		return jsonResponse;
	}
	*/
	
	private void fetchSellerInfo( String id ) throws ContextException{
			
			Log.e( TAG, "Synchronizing seller information..." );
			if( null != handler )
				handler.onFeedback( "Synchronizing seller info..." );			
			
			ServiceStub service = ServiceLocator.lookup( ServiceName.SELLER_INFO );	
			
			LynxApplication app = ( LynxApplication ) activity.getApplication();
			UserEntity entity = app.getUser();
			
			String urlStr = service.getUrl() + service.resolveParams( "1", entity.getId() );			
			
			URI url = URI.create( urlStr );
			HttpGet get = new HttpGet(url);
			HttpClient client = new DefaultHttpClient();
			HttpResponse response = null;

			try {
				response = client.execute(get);
				if( response == null ){
					TestFlight.log( "Missed [ "+urlStr+" ] to be synchronized" );
				}else{									
					String jsonResponse = EntityUtils.toString( response.getEntity() );
					UserDataModel.saveUser(activity, jsonResponse);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				TestFlight.sendCrash(1L, "Error Trying to get clientInfo",ex.toString());
				TestFlight.sendsCrashes();
				throw new ContextException( ex.toString() );
			}				
	}
	
	/*
	private void fetchClients( ArrayList<String> clientsIDS ) throws ContextException{
		
		Log.e( TAG, "Synchronizing clients..." );
		if( null != handler )
			handler.onFeedback( "Synchronizing clients..." );		
		
		ServiceStub service = ServiceLocator.lookup( ServiceName.CLIENT_INFO );			
		
		for(int x = 0; x < clientsIDS.size(); x++){		
			
			String urlStr = service.getUrl() + service.resolveParams( "1", clientsIDS.get(x) );	
			
			URI url = URI.create( urlStr );
			HttpGet get = new HttpGet(url);
			HttpClient client = new DefaultHttpClient();
			HttpResponse response = null;

			try {
				response = client.execute(get);
				if( response == null ){
					TestFlight.log( "Missed [ "+urlStr+" ] to be synchronized" );
				}else{									
					String jsonResponse = EntityUtils.toString( response.getEntity() );
					ClientDataModel.saveClient(getContext(),jsonResponse );
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				TestFlight.sendCrash(1L, "Error Trying to get clientInfo",ex.toString());
				TestFlight.sendsCrashes();
				throw new ContextException( ex.toString() );
			}			
		}		
	}
	*/
	
	private void fetchClientsInfo( ArrayList<String> clientsIDS )throws ContextException{				
		
			Log.e( TAG, "Synchronizing client( societies,sales_area ) information..." );			
			if( null != handler )
				handler.onFeedback( "Synchronizing client info..." );				
						
			ServiceStub service = ServiceLocator.lookup( ServiceName.CLIENT_SALES_AREA );
			
			for(int x = 0; x < clientsIDS.size(); x++){	
				
				String urlStr = service.getUrl() + service.resolveParams( "2", clientsIDS.get(x) );
				
				URI url = URI.create( urlStr );
				HttpGet get = new HttpGet(url);
				HttpClient client = new DefaultHttpClient();
				HttpResponse response = null;

				try {
					response = client.execute(get);
					if( response == null ){
						TestFlight.log( "Missed [ "+urlStr+" ] to be synchronized" );
					}else{									
						String jsonResponse = EntityUtils.toString( response.getEntity() );
						ClientDataModel.saveClientInfo(activity, clientsIDS.get(x), jsonResponse );
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					TestFlight.sendCrash(1L, "Error Trying to get clientSales",ex.toString());
					TestFlight.sendsCrashes();
					throw new ContextException( ex.toString() );
				}			
			}				
	}

	private Object[] extractClientsIds( String response )
	{
		List< String > ids = new ArrayList< String >();
		try{
			JSONObject jsonResponse = new JSONObject( response );
			JSONArray clients = jsonResponse.getJSONArray( "clients" );
			for( int i=0;i<clients.length();++i ){
				JSONObject client = clients.getJSONObject( i );		
				ids.add(  client.getString( "id" ) );
			}
		}catch( JSONException ex ){
			TestFlight.log( "Error Trying to get product clients ids: Reason:"+ ex.toString());
			TestFlight.sendCrash(1L, "Error Trying to get product clients ids",ex.toString());
			TestFlight.sendsCrashes();
			
			ex.printStackTrace();
		}
		return ids.toArray();
	}	

}

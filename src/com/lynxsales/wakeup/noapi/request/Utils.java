package com.lynxsales.wakeup.noapi.request;

import java.util.Locale;

import android.content.Context;
import android.provider.Settings.Secure;

public class Utils {

	
	public static final String formatDate( String value ){		
		String year = value.substring(0, 4);
		String month = value.substring(4, 6);
		String day = value.substring(6,value.length() );
		return year+"/"+month+"/"+day;
	}
	
	
	public static final String getUserHashCode( Context cxt,String username ){
		String id = Secure.getString( cxt.getContentResolver(),Secure.ANDROID_ID);
		return id+":"+getCammelInitLetters( username );
	}
	
	
	public static final String getCammelInitLetters( String line ){
		StringBuilder builder = new StringBuilder();
		
		String[] words = line.split( " " );
		for(  String word : words ){
			builder.append( word.substring(0, 1).toUpperCase( Locale.getDefault() ) );
		}
		return builder.toString();
	}
	
}

package com.lynxsales.wakeup.noapi.request;

import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.content.Context;

import com.lynx.sales.bundle.http.request.ContextException;
import com.lynx.sales.bundle.http.request.PhaseHandler;
import com.lynx.sales.bundle.http.request.Requestor;
import com.lynx.sales.bundle.http.request.ResponseStub;
import com.lynx.sales.bundle.http.request.ServiceStub;
import com.testflightapp.lib.TestFlight;

public class DefaultRequestor extends Requestor {
	
	public DefaultRequestor( Context cxt,PhaseHandler handler) {
		super(cxt,handler);
	}

	@Override	
	public ResponseStub handleRequest(ServiceStub stub, String... params)throws ContextException{
				
		String strUrl = stub.getUrl();
		if( params != null && params.length >0 )			
			strUrl += stub.resolveParams(params);
		
				
		URI url = URI.create( strUrl );
		HttpGet get = new HttpGet(url);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = null;
	
		try{
			response = client.execute( get );
			
			if( response != null ){
				String resp = EntityUtils.toString( response.getEntity() );
				return new ResponseStub( resp );
			}
			
		}catch( Exception ex ){			
			ex.printStackTrace();			
			TestFlight.sendCrash(1L ,"Error Trying to get logged",ex.toString() );
			throw new ContextException( "Network error" );
		}
		
		return null;
		
	}
}

package com.lynxsales.wakeup.noapi.request;


import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lynx.sales.bundle.dto.model.ProductDataModel;
import com.lynx.sales.bundle.dto.model.SyncDataModel;
import com.lynx.sales.bundle.dto.model.UnitsDataModel;
import com.lynx.sales.bundle.entities.Material;
import com.lynx.sales.bundle.entities.Sync;
import com.lynx.sales.bundle.entities.UnitEntity;
import com.lynx.sales.bundle.http.request.ContextException;
import com.lynx.sales.bundle.http.request.PhaseHandler;
import com.lynx.sales.bundle.http.request.ServiceLocator;
import com.lynx.sales.bundle.http.request.ServiceName;
import com.lynx.sales.bundle.http.request.ServiceStub;
import com.lynx.sales.bundle.http.request.SynchronizeRequest;
import com.lynx.sales.bundle.http.request.SynchronizeRequestDelegate;
import com.lynxsales.wakeup.R;
import com.testflightapp.lib.TestFlight;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class SynchronizationDelegateProductos implements SynchronizeRequestDelegate<String>
{
    private static final String TAG = SynchronizeRequest.class.getSimpleName();
    private Activity activity;
    private ArrayList clientsIDS;
    private PhaseHandler handler;
    private String jsonClients;
    private String sellerID;

    public SynchronizationDelegateProductos(Activity activity)
    {
	this.activity = activity;
    }


	@Override
	public void performSynchronization(PhaseHandler handler, String... params) throws ContextException 
	{
	    this.handler = handler;
	    this.sellerID = params[0].toString();
	    fetchProducts(this.sellerID);
		
	}

private String fetchProducts( String sellerID ) throws ContextException{	
	
	Log.e( TAG, "Synchronizing products..." );
	
	if( null != handler )
		handler.onFeedback(activity.getResources().getString(R.string.sync_products));
	
	ServiceStub service = ServiceLocator.lookup( ServiceName.PRODUCTS);
	
	String strUrl = service.getUrl() + service.resolveParams( "1", sellerID );
	
	URI url = URI.create(strUrl);
	HttpGet get = new HttpGet(url);
	HttpClient client = new DefaultHttpClient();
	HttpResponse response = null;

	String jsonResponse = null;
	
	try {
		response = client.execute(get);
		if( response == null ){
			TestFlight.log( "Missed [ "+strUrl+" ] to be synchronized" );
		}
		else
		{					
			jsonResponse = EntityUtils.toString( response.getEntity() );
			JSONArray productsResponse = new JSONArray(jsonResponse);
			
			ProductDataModel.deleteProducts(activity);
			UnitsDataModel.deleteUnits(activity);
			
			List<Material> materiales = new ArrayList<Material>();
			
			String tempCenter = "";
			
			for( int x = 0; x < productsResponse.length(); x++)
			{
				JSONObject jsonObject =  productsResponse.getJSONObject((x));
				String center = jsonObject.getString("center");
				
					JSONArray materials = jsonObject.getJSONArray("materials");
					
					for(int y = 0; y < materials.length(); y++)
					{
						JSONObject materialObject = (JSONObject) materials.get(y);
						String stringMaterials = materialObject.toString().trim();
						JSONArray unidades = materialObject.getJSONArray("unidades");
						Material material = new Gson().fromJson(stringMaterials, new TypeToken<Material>(){}.getType()); 
						
						List<UnitEntity> unitList = new Gson().fromJson(unidades.toString().trim(), new TypeToken<List<UnitEntity>>(){}.getType());
						material.setUnidades(unitList);
						materiales.add(material);
					}
					
					ProductDataModel.bulkSaveProducts(activity, materiales, center);
					materiales.clear();
				}
			Sync sync = new Sync(null, new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()), "products", "synced");
		        SyncDataModel.setSync((Context)this.activity, sync);
		}

	} catch (Exception ex) {
		ex.printStackTrace();
		TestFlight.sendCrash(1L, "Error Trying to get synchronized",ex.toString());
		TestFlight.sendsCrashes();
		throw new ContextException( ex.toString() );
	}		
	
	return jsonResponse;
}


}
package com.lynxsales.wakeup.noapi.request;

import com.lynx.sales.bundle.entities.ItineraryEntity;


public class ItineraryResult {

	private ItineraryEntity itinerary;

	public ItineraryResult(ItineraryEntity result) {
		this.itinerary = result;
	}

	public ItineraryEntity getItinerary() {
		return itinerary;
	}

	public void setItinerary(ItineraryEntity itinerary) {
		this.itinerary = itinerary;
	}

}

package com.lynxsales.wakeup.noapi.request;



import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lynx.sales.bundle.dto.model.ClientDataModel;
import com.lynx.sales.bundle.dto.model.ConditionsDataModel;
import com.lynx.sales.bundle.dto.model.ItineraryDataModel;
import com.lynx.sales.bundle.dto.model.SyncDataModel;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.entities.ConditionEntity;
import com.lynx.sales.bundle.entities.Sync;
import com.lynx.sales.bundle.http.request.ContextException;
import com.lynx.sales.bundle.http.request.PhaseHandler;
import com.lynx.sales.bundle.http.request.ServiceLocator;
import com.lynx.sales.bundle.http.request.ServiceName;
import com.lynx.sales.bundle.http.request.ServiceStub;
import com.lynx.sales.bundle.http.request.SynchronizeRequest;
import com.lynx.sales.bundle.http.request.SynchronizeRequestDelegate;
import com.lynxsales.wakeup.R;
import com.testflightapp.lib.TestFlight;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class SynchronizationDelegateClient implements SynchronizeRequestDelegate<String>
{
    private static final String TAG = SynchronizeRequest.class.getSimpleName();
    private Activity activity;
    private ArrayList clientsIDS;
    private PhaseHandler handler;
    private String jsonClients;
    private String sellerID;

    public SynchronizationDelegateClient(Activity activity)
    {
	this.activity = activity;
    }

    @Override
	public void performSynchronization(PhaseHandler handler, String... params) throws ContextException 
	{
	    this.handler = handler;
	    this.sellerID = params[0].toString();
	    fetchClients();
		
	}

    private void fetchClients() throws ContextException
    {
	Log.e(TAG, "Synchronizing itinerary clients...");

	if (null != handler)
	    handler.onFeedback(activity.getResources().getString(R.string.sync_clients));

	ServiceStub service = ServiceLocator.lookup(ServiceName.CUSTOMER);

	String strUrl = null;
	if (service != null)
	{
	    strUrl = service.getUrl() + service.resolveParams("1");
	    URI url = URI.create(strUrl);
	    Log.e("e", strUrl);
	    HttpGet get = new HttpGet(url);
	    HttpClient httpClient = new DefaultHttpClient();
	    HttpResponse response = null;

	    String jsonResponse = null;
	    List<ClientEntity> clients = new ArrayList<ClientEntity>();
	    try
	    {
		response = httpClient.execute(get);
		if (response == null)
		{
		    TestFlight.log("Missed [ " + strUrl + " ] to be synchronized");
		} else
		{
		    ClientDataModel.deleteClients(activity);
		    jsonResponse = EntityUtils.toString(response.getEntity());
		    JSONArray clientsResponse = new JSONArray(jsonResponse);

		    for (int x = 0; x < clientsResponse.length(); x++)
		    {
			JSONObject jsonObject = clientsResponse.getJSONObject((x));
			ClientEntity client = new Gson().fromJson(jsonObject.toString().trim(),
				new TypeToken<ClientEntity>()
				{
				}.getType());
			clients.add(client);
		    }
		    ClientDataModel.saveClient(activity, clients);
		    Sync sync = new Sync(null, new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()), "clients", "synced");
		        SyncDataModel.setSync((Context)this.activity, sync);
		}
	    } catch (Exception ex)
	    {
		ex.printStackTrace();
		TestFlight.sendCrash(1L, "Error Trying to get synchronized", ex.toString());
		TestFlight.sendsCrashes();
		throw new ContextException(ex.toString());
	    }
	} else
	{
	    Log.e("Service", " is null");
	}

    }
  

}
package com.lynxsales.wakeup.noapi.request;



import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lynx.sales.bundle.dto.model.ConditionsDataModel;
import com.lynx.sales.bundle.dto.model.SyncDataModel;
import com.lynx.sales.bundle.entities.ConditionEntity;
import com.lynx.sales.bundle.entities.Sync;
import com.lynx.sales.bundle.http.request.ContextException;
import com.lynx.sales.bundle.http.request.PhaseHandler;
import com.lynx.sales.bundle.http.request.ServiceLocator;
import com.lynx.sales.bundle.http.request.ServiceName;
import com.lynx.sales.bundle.http.request.ServiceStub;
import com.lynx.sales.bundle.http.request.SynchronizeRequest;
import com.lynx.sales.bundle.http.request.SynchronizeRequestDelegate;
import com.lynxsales.wakeup.R;
import com.testflightapp.lib.TestFlight;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class SynchronizationDelegateCondiciones implements SynchronizeRequestDelegate<String>
{
    private static final String TAG = SynchronizeRequest.class.getSimpleName();
    private Activity activity;
    private ArrayList clientsIDS;
    private PhaseHandler handler;
    private String jsonClients;
    private String sellerID;

    public SynchronizationDelegateCondiciones(Activity activity)
    {
	this.activity = activity;
    }

    @Override
	public void performSynchronization(PhaseHandler handler, String... params) throws ContextException 
	{
	    this.handler = handler;
	    this.sellerID = params[0].toString();
	    fetchConditions(this.sellerID);
		
	}

    private void fetchConditions( String sellerID ) throws ContextException
   	{
   		Log.e( TAG, activity.getResources().getString(R.string.sync_itinerary) );
   		
   		if( null != handler )
   			handler.onFeedback(activity.getResources().getString(R.string.sync_prices_bonus) );
   		
   		
   		ServiceStub service = ServiceLocator.lookup( ServiceName.CONDITIONS);
   		if( null != service )
   		{	
   			String strUrl = service.getUrl() + service.resolveParams( "6", sellerID );
   			
   			URI url = URI.create(strUrl);
   			HttpGet get = new HttpGet(url);
   			HttpClient client = new DefaultHttpClient();
   			HttpResponse response = null;

   			try {
   				response = client.execute(get);
   				if( response == null )
   				{
   					TestFlight.log( "Missed [ "+strUrl+" ] to be synchronized" );
   				}
   				else
   				{					

   					ConditionsDataModel.deleteConditions(activity);
   					String jsonResponse = EntityUtils.toString( response.getEntity() );
   					List<ConditionEntity> conditions = new Gson().fromJson(jsonResponse.trim(), new TypeToken<List<ConditionEntity>>(){}.getType());
   					ConditionsDataModel.saveConditions(activity, conditions);
   					Sync sync = new Sync(null, new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()), "conditions", "synced");
   				        SyncDataModel.setSync((Context)this.activity, sync);

   				}

   			} catch (Exception ex) {
   				ex.printStackTrace();
   				TestFlight.sendCrash(1L, "Error Trying to get synchronized",ex.toString());
   				TestFlight.sendsCrashes();
   				throw new ContextException( ex.toString() );
   			}			
   		}
   	}
  

}
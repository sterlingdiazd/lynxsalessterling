package com.lynxsales.wakeup.features;

import android.content.Context;

import com.lynx.sales.bundle.prefs.utils.PreferenceHelper;

public final class KnowledgeFeature {	
	
	private static final String KNOWLEDGE_EFATURE_PREFS;
	
	static{
		KNOWLEDGE_EFATURE_PREFS = String.valueOf( "com.lynx.sales.user.KNOWLEDGE_EFATURE_PREFS".hashCode() );
	}
	
	public static final boolean recogizedFeature( Context cxt,Feature feature ){
		return PreferenceHelper.getBoolean( cxt, KNOWLEDGE_EFATURE_PREFS, feature.id() );
	}
	
	public static final void setRecognizedFeature( Context cxt,Feature feature,boolean value ){
		PreferenceHelper.setBoolean( cxt, KNOWLEDGE_EFATURE_PREFS, feature.id(), value );
	}	
}

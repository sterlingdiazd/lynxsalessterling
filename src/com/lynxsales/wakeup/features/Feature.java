package com.lynxsales.wakeup.features;

public enum Feature {
	
	DRAWER_VIEW{
		@Override
		 String id() {
			return String.valueOf( "com.lynx.sales.user.Feature.DRAWER_VIEW".hashCode() );
		}
	}
	
	;
	
	 abstract String id();
}

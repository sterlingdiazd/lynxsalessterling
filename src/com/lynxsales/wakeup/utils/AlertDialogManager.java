package com.lynxsales.wakeup.utils;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lynx.sales.bundle.dto.model.ClientDataModel;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynxsales.printer.Configuration;
import com.lynxsales.wakeup.activities.adapter.AdapterListViewClient;
import com.lynxsales.wakeup.activities.bprocess.BillActivity;
import com.lynxsales.wakeup.R;


public class AlertDialogManager {
	/**
	 * Function to display simple Alert Dialog
	 * @param activity - application context
	 * @param title - alert dialog title
	 * @param message - alert message
	 * @param status - success/failure (used to set icon)
	 * 				 - pass null if you don't want icon
	 * */
	private Configuration configuracion;


    public AlertDialogManager() {
		super();
	}

	public void showAlertDialog(final Activity activity, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);

		if(status != null)

        if(status)
        {
            alertDialog.setIcon(R.drawable.ic_ok);
        } else {
            alertDialog.setIcon(R.drawable.ic_logout);
        }

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}
    
	public void dialogAttachOrderToClient(final Activity activity, String title, String message) 
	{

		configuracion = Configuration.getInstance();
		configuracion.configureSharePreference(activity);
		
		Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(message);
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setPositiveButton(activity.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) 
			{
				Configuration.getInstance().configureSharePreference(activity);
				Configuration.getInstance().getPrefsEditor().putBoolean("AttachOrderToClient", true);
				Configuration.getInstance().getPrefsEditor().commit();
				
				( (BillActivity) activity).changeClient();
				
			}
		});
		
		builder.setNegativeButton(activity.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();				
			}
		});
		
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	public void clientListDialog(final Activity activity)
	 {

		 	/*
	        String client = ( (EditText) placeholderFragment.getView().findViewById(R.id.editTextCartClient) ).getText().toString();
	        List<Client> clients = new ClientDatabase(activity).getClientByName(client);
	        */
	        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( activity );
	        alertDialogBuilder.setTitle( activity.getResources().getString(R.string.clients) );
	        //alertDialogBuilder.setIcon( );

	        LayoutInflater inflater = LayoutInflater.from(activity);
	        View rootView = inflater.inflate(R.layout.dialog_client_list, null);

	        ListView listViewClientList = (ListView) rootView.findViewById(R.id.listViewClientList);
	        AdapterListViewClient adapterListViewClient = new AdapterListViewClient(activity);
	        adapterListViewClient.setClients(ClientDataModel.getNewClients(activity));
	        listViewClientList.setAdapter(adapterListViewClient);
	        listViewClientList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	            {
	            	ClientEntity client = (ClientEntity) parent.getItemAtPosition(position);
	            	Configuration.getInstance().configureSharePreference(activity);
	            	Configuration.getInstance().getPrefsEditor().putString("client_id", client.getId());
	            	Configuration.getInstance().getPrefsEditor().commit();
	            	
	            	((BillActivity) activity).changeClientID();
	            }
	        });

	        alertDialogBuilder.setView(rootView);

	        alertDialogBuilder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int which) {
	                dialog.cancel();
	            }
	        });

	        alertDialogBuilder.create();
	        alertDialogBuilder.show();
	    }
   

}

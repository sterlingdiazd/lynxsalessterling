package com.lynxsales.wakeup.utils;

import android.app.Dialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

/**
 * Created by sterlingdiazd on 4/13/2014.
 */
public class LocationUtility extends Fragment implements LocationListener {

    private GoogleMap googleMap;
    private double latitude = 0;
    private double longitude = 0;
    private MarkerOptions marker;
    private Location location;
    private LocationTracker tracker;
    private Context context;
    private static LocationUtility locationUtility;
    private SupportMapFragment supportMapFragment;

    public SupportMapFragment getSupportMapFragment() {
        return supportMapFragment;
    }

    public void setSupportMapFragment(SupportMapFragment supportMapFragment) {
        this.supportMapFragment = supportMapFragment;
    }

    public static synchronized LocationUtility getInstance(Context context) {
        if (locationUtility == null) {
            locationUtility = new LocationUtility(context);
        }
        return locationUtility;
    }

    private LocationUtility(Context context) {
        this.context = context;
        /*
        configuration = Configuration.getInstance();
        configuration.configureSharePreference(context);
        */
    }

    public GoogleMap initilizeMap()
    {

        try
        {
            if (googleMap == null)
            {
//                SupportMapFragment supportMapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.map);
                googleMap = supportMapFragment.getMap();
                googleMap.setMyLocationEnabled(true);
            }
            else
            {
                Log.e("Google map", "Map is not null");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return googleMap;
    }

    public boolean verifyGooglePlayStatus()
    {
        boolean isGooglePlayAvailable = false;
        int responseCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (responseCode != ConnectionResult.SUCCESS)
        {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(responseCode, getActivity(), requestCode);
            dialog.show();
        }
        else
        {
            isGooglePlayAvailable = true;
        }
        return isGooglePlayAvailable;
    }


    public Location getUserLocation()
    {
        try {
            tracker = new LocationTracker(context);
            if(tracker.canGetLocation())
            {
                location = tracker.getLocation();
            }
            else {
                tracker.showSettingsAlert();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return location;
    }


    public void centerMapOnMyLocation(Location location)
    {
        if (location != null) {
            onLocationChanged(location);
        } else {
            Toast.makeText(context, "Active la localización en Configuraciones", Toast.LENGTH_LONG).show();
            Log.e("Location not available", "Location not available");
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(drawMarker(location), 13));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private LatLng drawMarker(Location location)
    {
        //googleMap.clear();
        LatLng currentPosition = new LatLng(location.getLatitude(),location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(currentPosition);
        markerOptions.snippet("Lat:" + location.getLatitude() + "Lng:" + location.getLongitude());
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        markerOptions.title("Ubicación actual");
        googleMap.addMarker(markerOptions);

        return currentPosition;
    }
    /*
    public void saveUserLocation(Location location)
    {
        try
        {
            if (location != null) {

                latitude = location.getLatitude();
                longitude = location.getLongitude();

                configuration.getPrefsEditor().putString(configuration.LATITUD, String.valueOf(latitude));
                configuration.getPrefsEditor().putString(configuration.LONGITUD, String.valueOf(longitude));
                configuration.getPrefsEditor().commit();

            } else {
                Toast.makeText(context, "lOCATION IS NULL", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
	*/
}

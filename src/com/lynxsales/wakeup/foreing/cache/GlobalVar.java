/**
 * 
 */
package com.lynxsales.wakeup.foreing.cache;

import android.os.Environment;

/**
 * @author eonoe
 *
 */
public interface GlobalVar {

	public static final String	APPROOTNAME					= "Vistazo";
	public final static String	STORAGE_DIR					= "/" + GlobalVar.APPROOTNAME + "/";
	public final static String	MEDIAPATH					= Environment.getExternalStorageDirectory().getAbsolutePath()
																	+ STORAGE_DIR;

}

/**
 * 
 */
package com.lynxsales.wakeup.foreing.cache;

import java.lang.ref.WeakReference;

import com.lynxsales.wakeup.R;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;




public class DownloadedDrawable extends BitmapDrawable {

	private final WeakReference<BitmapDownloaderTask> bitmapDownloaderTaskReference;

	public DownloadedDrawable(BitmapDownloaderTask bitmapDownloaderTask, Context ctx) {
//		super(R.drawable.nodisp);
		
		super(BitmapFactory.decodeResource(ctx.getResources(), R.drawable.nodisp ));
		bitmapDownloaderTaskReference = new WeakReference<BitmapDownloaderTask>(bitmapDownloaderTask);
		
	}

	public BitmapDownloaderTask getBitmapDownloaderTask() {
		return bitmapDownloaderTaskReference.get();
	}
}
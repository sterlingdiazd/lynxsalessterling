package com.lynxsales.wakeup.foreing.cache;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

/**
 * @author eonoe
 *
 */

public class ImageUtil {

	private static final String	TAG	= "ImageUtil";

	/**
	 * Method to retrieve a bitmap image from a URL over the network. The
	 * built-in methods do not seem to work, as they return a FileNotFound
	 * exception.
	 * 
	 * Note that this does not perform any threading -- it blocks the call while
	 * retrieving the data.
	 * 
	 * @param url
	 *            The URL to read the bitmap from.
	 * @return A Bitmap image or null if an error occurs.
	 */
	public static Bitmap readBitmapFromNetwork(URL url) {
		InputStream is = null;
		BufferedInputStream bis = null;
		Bitmap bmp = null;
		try {
			URLConnection conn = url.openConnection();
			conn.connect();
			/*
			is = conn.getInputStream();
			bis = new BufferedInputStream(is);
			bmp = BitmapFactory.decodeStream(bis);
			*/
		} catch (MalformedURLException e) {
			//Log.e(TAG, "Bad ad URL", e);
		} catch (IOException e) {
			//Log.e(TAG, "Could not get remote ad image", e);
		} finally {
			try {
				if (is != null) is.close();
				if (bis != null) bis.close();
			} catch (IOException e) {
				//Log.w(TAG, "Error closing stream.");
			}
		}

		writeImageToDisk(bmp, url.toString());
		return bmp;
	}

	private static boolean writeImageToDisk(Bitmap image, String imageName) {
		boolean ret = true;
		File dir = new File(StorageUtil.getStoragePath());
		FileOutputStream fos = null;
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				Log.e(TAG, "Problem creating Image cache folder");
				return false;
			}
		}

		String fileName = Integer.toString((imageName.hashCode()));
		File file = new File(dir.toString(), fileName);

		if (image != null) {
			try {
				fos = new FileOutputStream(file);
				image.compress(Bitmap.CompressFormat.PNG, 100, fos);

			} catch (FileNotFoundException e) {
				Log.e(TAG, "Failed to write file", e);
				return false;
			} finally {
				try {
					if (fos != null) {
						fos.flush();
						fos.close();
					}
				} catch (IOException e) {
					Log.w(TAG, "Error closing stream.");
					return false;
				}
			}

		}
		return ret;
	}
	
	public static Bitmap downloadBitmap(String url){
		if (!TextUtils.isEmpty(url)){
			String fileName = Integer.toString((url.hashCode()));
			/*
			Bitmap cacheFile = BitmapFactory.decodeFile(StorageUtil.getStoragePath() + fileName);
			if (cacheFile != null){
				return cacheFile;
			}else{
				//Get Form network
				try {
					return readBitmapFromNetwork(new URL(url));
				} catch (MalformedURLException e) {
					//e.printStackTrace();
				}
			}
			*/

		}
		return null;
	}

	public static Bitmap getBitmapFromFile(String url) {
		if (!TextUtils.isEmpty(url)) {
			String fileName = Integer.toString((url.hashCode()));
			Bitmap cacheFile = BitmapFactory.decodeFile(StorageUtil.getStoragePath() + fileName);
			return cacheFile;
		}
		return null;
	}

	public static Bitmap getPicFromPath(int targetW, int targetH, String mCurrentPhotoPath) {
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;

		int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;

		Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
		return bitmap;
	}

	/**
	 * Decode and sample down a bitmap from a file to the requested width and
	 * height.
	 * 
	 * @param filename
	 *            The full path of the file to decode
	 * @param reqWidth
	 *            The requested width of the resulting bitmap
	 * @param reqHeight
	 *            The requested height of the resulting bitmap
	 * @return A bitmap sampled down from the original with the same aspect
	 *         ratio and dimensions that are equal to or greater than the
	 *         requested width and height
	 */
	public static Bitmap decodeSampledBitmapFromFile(String filename, int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filename, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(filename, options);
	}

	/**
	 * Calculate an inSampleSize for use in a {@link BitmapFactory.Options}
	 * object when decoding bitmaps using the decode* methods from
	 * {@link BitmapFactory}. This implementation calculates the closest
	 * inSampleSize that will result in the final decoded bitmap having a width
	 * and height equal to or larger than the requested width and height. This
	 * implementation does not ensure a power of 2 is returned for inSampleSize
	 * which can be faster when decoding but results in a larger bitmap which
	 * isn't as useful for caching purposes.
	 * 
	 * @param options
	 *            An options object with out* params already populated (run
	 *            through a decode* method with inJustDecodeBounds==true
	 * @param reqWidth
	 *            The requested width of the resulting bitmap
	 * @param reqHeight
	 *            The requested height of the resulting bitmap
	 * @return The value to be used for inSampleSize
	 */

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}
		return inSampleSize;
	}

	/**
	 * Decode and sample down a bitmap from resources to the requested width and
	 * height.
	 * 
	 * @param res
	 *            The resources object containing the image data
	 * @param resId
	 *            The resource id of the image data
	 * @param reqWidth
	 *            The requested width of the resulting bitmap
	 * @param reqHeight
	 *            The requested height of the resulting bitmap
	 * @return A bitmap sampled down from the original with the same aspect
	 *         ratio and dimensions that are equal to or greater than the
	 *         requested width and height
	 */
	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
		
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		Bitmap bitmap = BitmapFactory.decodeResource(res, resId, options);
		return bitmap;
	}

	public static File convertImageUriToFile(Uri imageUri, Activity activity) {
		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID,
					MediaStore.Images.ImageColumns.ORIENTATION };
			cursor = activity.managedQuery(imageUri, proj, // Which columns to
															// return
					null, // WHERE clause; which rows to return (all rows)
					null, // WHERE clause selection arguments (none)
					null); // Order-by clause (ascending by name)
			int file_ColumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			// int orientation_ColumnIndex =
			// cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.ORIENTATION);
			if (cursor.moveToFirst()) {
				// String orientation =
				// cursor.getString(orientation_ColumnIndex);
				return new File(cursor.getString(file_ColumnIndex));
			}
			return null;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public static int getImageWidth(String path) {
		ExifInterface exif;
		int imageWidth = 0;
		int imageHeight = 0;
		if (!TextUtils.isEmpty(path)) {
			try {
				exif = new ExifInterface(path);
				// imageWidth =
				// exif.getAttribute(ExifInterface.TAG_IMAGE_WIDTH);
				imageWidth = Integer.parseInt(exif.getAttribute(ExifInterface.TAG_IMAGE_LENGTH));
				imageHeight = Integer.parseInt(exif.getAttribute(ExifInterface.TAG_IMAGE_WIDTH));
				Log.e("", "TAG_IMAGE_LENGTH >> " + imageWidth);
				Log.e("", "TAG_IMAGE_WIDTH >>" + imageHeight);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return imageHeight;
	}

}

/**
 * 
 */
package com.lynxsales.wakeup.foreing.cache;

import java.io.File;

import android.app.ProgressDialog;
import android.os.Environment;
import android.util.Log;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.TextView;


/**
 * @author eonoe
 *
 */
public class StorageUtil implements GlobalVar{

	private final static String TAG = StorageUtil.class.getSimpleName();

	private static ProgressDialog dialog;

	public void FileLog() {
		if (!new File(MEDIAPATH).isDirectory()) {
			File f = new File(MEDIAPATH);
			if (!f.mkdirs()) {
				Log.e(TAG, "Make directory failed.");
			}
		}
	}
	
	public static String getStoragePath() {
		if (isExternalStoragePresent()) {
			return MEDIAPATH;
		} else {
			return Environment.getRootDirectory().getAbsolutePath() + STORAGE_DIR;
		}
	}

	public static String getStoragePathRelative() {
		if (isExternalStoragePresent()) {
			return MEDIAPATH;
		} else {
			return Environment.getRootDirectory().getPath() + STORAGE_DIR;
		}
	}

	
	private static boolean isExternalStoragePresent() {
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else {
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		if (!((mExternalStorageAvailable) && (mExternalStorageWriteable))) {
			Log.w(TAG, "SD card not present");
		}
		return (mExternalStorageAvailable) && (mExternalStorageWriteable);
	}

	
	
	public static void setupDescriptionText( final TextView textDesc ){		
		final ViewTreeObserver observer = textDesc.getViewTreeObserver();
		observer.addOnGlobalLayoutListener( new OnGlobalLayoutListener() {			
			@Override
			public void onGlobalLayout() {
		        int maxLines = (int) textDesc.getHeight()  / textDesc.getLineHeight();
		        textDesc.setMaxLines(maxLines);
		        textDesc.getViewTreeObserver().removeGlobalOnLayoutListener(this);
			}
		});
	}
	
		
}

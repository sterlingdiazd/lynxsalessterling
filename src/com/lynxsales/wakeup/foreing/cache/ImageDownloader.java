/**
 * 
 */
package com.lynxsales.wakeup.foreing.cache;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.lynxsales.wakeup.R;



/**
 * @author eonoe
 *
 */
public class ImageDownloader {

    public void download(String url, ImageView imageView, Context ctx) {
    	
    	if (cancelPotentialDownload(url, imageView)) {
    		BitmapDownloaderTask task = new BitmapDownloaderTask(imageView);
    		imageView.setImageResource(R.drawable.nodisp);
    		DownloadedDrawable downloadedDrawable = new DownloadedDrawable(task, ctx);
    		imageView.setImageDrawable(downloadedDrawable);
    		task.execute(url);
    	}
    }

    private static boolean cancelPotentialDownload(String url, ImageView imageView) {
        BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);

        if (bitmapDownloaderTask != null) {
            String bitmapUrl = bitmapDownloaderTask.url;
            if ((bitmapUrl == null) || (!bitmapUrl.equals(url))) {
                bitmapDownloaderTask.cancel(true);
            } else {
                // The same URL is already being downloaded.
                return false;
            }
        }
        return true;
    }    
    
    private static BitmapDownloaderTask getBitmapDownloaderTask(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof DownloadedDrawable) {
                DownloadedDrawable downloadedDrawable = (DownloadedDrawable)drawable;
                return downloadedDrawable.getBitmapDownloaderTask();
            }
        }
        return null;
    }
    
}

package com.lynxsales.wakeup.fg.bprocess;

import java.util.Arrays;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.entities.UserEntity;
import com.lynxsales.wakeup.activities.adapter.DocTypeAdapter;
import com.lynxsales.wakeup.R;

public class HeaderDialogFragment extends DialogFragment{
	
	public  static final String EXTRA_BUSSINESS_CLIENT 	=	"com.lynx.sales.fg.bprocess.HeaderDialogFragment.EXTRA_BUSSINESS_CLIENT";
	public  static final String EXTRA_SELLER		 	=	"com.lynx.sales.fg.bprocess.HeaderDialogFragment.EXTRA_SELLER_CENTER";
	
	private ClientEntity client;
	private UserEntity user;
	private ActionListener callback;	
	
	public HeaderDialogFragment() {		
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		getDialog().setTitle( getString( R.string.header_dialog_title ) ); 
		View view = inflater.inflate( R.layout.fragment_header , container,false );
		return view;
	}
		
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		init( getView() );
	}
		
	@SuppressWarnings("unchecked")
	private void init( View v )
	{
		if( getArguments() != null )
		{
			client   = ( ClientEntity ) getArguments().get( EXTRA_BUSSINESS_CLIENT );
			user = ( UserEntity ) getArguments().get( EXTRA_SELLER );
		}
		
		setAcceptButton(v);
		setFields( v );
	}
	
	
	
	private void setAcceptButton( View v ){
		(( Button ) v.findViewById( R.id.dialog_accept )).setOnClickListener( new OnClickListener() {			
			@Override
			public void onClick(View view) {
				onAccept();
			}
		});		
	}
	
	
	private void setFields( View v ){
		
		(( TextView ) v.findViewById( R.id.client_id )).setText( ( client.getStatus().equalsIgnoreCase("SINCRONIZADO") ) ? client.getKunnr() : client.getId() );
		(( TextView ) v.findViewById( R.id.sector )).setText( (client.getSector() != null ) ? client.getSector() : "" );

		Spinner spDocs = ( Spinner ) v.findViewById( R.id.doc_type );
		DocTypeAdapter docAdapter = new DocTypeAdapter(getActivity(), user.getDocTypes());
		//ArrayAdapter<String> docAdapter = new ArrayAdapter<String>(getActivity(), R.layout.entry_spinner_list_item,Arrays.asList( user.getDocType() ));
		spDocs.setAdapter(docAdapter);
		
		
		spDocs.setOnItemSelectedListener( new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				//client.setDocType( user.getDocType() );												//	ARREGLAR CON LA CONFIGURACION
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {				
			}			
		});
		
		
		//final String[] societies = client.getClientInfo().getSocieties();
		/*
		Spinner societySpinner = ( Spinner ) v.findViewById( R.id.society );
		ArrayAdapter<String> societyAdapter = new ArrayAdapter<String>(getActivity(), R.layout.entry_spinner_list_item,societies);
		societySpinner.setAdapter(societyAdapter);		
		societySpinner.setOnItemSelectedListener( new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int position, long arg3) {
				client.setSociety( societies[ position ] );
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {				
			}			
		});
		
		
		
		final List< String > sales = client.getClientInfo().getSalesResume();
		
		Spinner salesSpinner = ( Spinner ) v.findViewById( R.id.sales_org );
		ArrayAdapter<String> salesAdapter = new ArrayAdapter<String>(getActivity(), R.layout.entry_spinner_list_item,sales);
		salesSpinner.setAdapter(salesAdapter);		
		salesSpinner.setOnItemSelectedListener( new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int position, long arg3) {
				client.setSalesOrg( sales.get( position ) );
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {				
			}			
		});
		
		
		
		final List< String > channels = client.getClientInfo().getChannelAsResume();
		
		Spinner channelSpinner = ( Spinner ) v.findViewById( R.id.channel );
		ArrayAdapter<String> channelAdapter = new ArrayAdapter<String>(getActivity(), R.layout.entry_spinner_list_item,channels);
		channelSpinner.setAdapter( channelAdapter );	
		channelSpinner.setOnItemSelectedListener( new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int position, long arg3) {
				client.setChannel( channels.get( position )); 
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {				
			}			
		});
		
		
		
		
		final String[] interlocutor = client.getClientInfo().getSalesArea().get(0).getInterlocutors();
		
		Spinner interSpinner = ( Spinner ) v.findViewById( R.id.iterlocutor );
		ArrayAdapter<String> interAdapter = new ArrayAdapter<String>(getActivity(), R.layout.entry_spinner_list_item,interlocutor);
		interSpinner.setAdapter( interAdapter );
		interSpinner.setOnItemSelectedListener( new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int position, long arg3) {
				client.setInterloctor( interlocutor[ position] );
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {				
			}			
		});		
		
		client.setSector( "01" );
		*/
	}
	
	
	
	public void setActionListener( HeaderDialogFragment.ActionListener callback){
		this.callback = callback;
	}
	
	
	
	public static interface ActionListener{
		public abstract void  onCancel( HeaderDialogFragment header );
		public abstract void  onAccept( HeaderDialogFragment header );
	}
	
	
	
	
	public void onAccept(){
		if( callback != null )
			callback.onAccept(this);
		this.dismiss();
	}
}

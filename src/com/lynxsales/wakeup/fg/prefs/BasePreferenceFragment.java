package com.lynxsales.wakeup.fg.prefs;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

public abstract class BasePreferenceFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener{
	
	@Override
	public void onStart() {
		super.onStart();		
		PreferenceManager
			.getDefaultSharedPreferences( this.getActivity() )
			.registerOnSharedPreferenceChangeListener( this );
	}
	
	@Override
	public void onPause() {
		super.onPause();
		PreferenceManager
			.getDefaultSharedPreferences( this.getActivity() )
			.unregisterOnSharedPreferenceChangeListener( this );
	}
	
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,String key) {	
	}	
	
}

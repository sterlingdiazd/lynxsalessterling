package com.lynxsales.wakeup.fg.prefs;

import android.os.Bundle;

import com.lynxsales.wakeup.R;

public class LogPreference extends BasePreferenceFragment{

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.log_preferences);
		
		getActivity().getActionBar().setTitle( R.string.setting_error_log );
	}

}

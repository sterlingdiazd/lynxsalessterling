package com.lynxsales.wakeup.fg.prefs;

import android.os.Bundle;

import com.lynxsales.wakeup.R;

public class SyncPreference extends BasePreferenceFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource( R.xml.sync_preferences );
		
		getActivity().getActionBar().setTitle( R.string.setting_sync );
	}
	
}

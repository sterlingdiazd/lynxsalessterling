package com.lynxsales.wakeup.fg.prefs;

import android.os.Bundle;

import com.lynxsales.wakeup.R;

public class BusinessPreference extends BasePreferenceFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource( R.xml.business_preferences);
		
		getActivity().getActionBar().setTitle( R.string.setting_business );
	}
	
}

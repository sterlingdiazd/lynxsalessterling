package com.lynxsales.wakeup.activities.bprocess;


import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.lynx.charges.Utility;
import com.lynxsales.wakeup.R;
import com.lynx.sales.bundle.dto.model.ScheduleDataModel;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob.Status;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.msg.MessageFormatter.Message;
import com.lynxsales.printer.Configuration;
import com.lynxsales.wakeup.ProcessMemento;
import com.lynxsales.wakeup.activities.adapter.PendingAdapter;
import com.lynxsales.wakeup.utils.CommonUtils;

public class PendingList extends ListActivity{	
	
	public static final String ID_ARCHIVE = "ID_ARCHIVE";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView( R.layout.list_activity_content );
		init();
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) 
		{
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return false;
		}
	}
	
	private void init()
	{
		initActionBar();
		setAdapter();		
	}
	
	private void initActionBar()
	{
		getActionBar().setDisplayHomeAsUpEnabled( true );
		getActionBar().setTitle( getResources().getString(R.string.pending_jobs) );
	}
	
	private void setAdapter()
	{
		List< ScheduleJob > pendings = ScheduleDataModel.getShedulesJobsByStatus(this, Status.PENDING );		
		List< ScheduleJob > realPendingJobs = new ArrayList<ScheduleJob>();
		
		for(ScheduleJob job : pendings)
		{
			if( job.getStatus().toString().equalsIgnoreCase(ScheduleJob.Status.PENDING.toString()))
			{
				realPendingJobs.add(job);
			}
		}
		PendingAdapter adapter = new PendingAdapter(this, R.layout.entry_pending, realPendingJobs);		
		setListAdapter(adapter);		
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) 
	{
		ScheduleJob schedule = (ScheduleJob) l.getItemAtPosition(position);
		archiveScheduleDialog(v.getContext(), schedule).show();
	}


	@Override
	protected void onResume() 
	{
		super.onResume();
		setAdapter();
	}


	private Dialog archiveScheduleDialog(final Context context, final ScheduleJob schedule)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( context );
		alertDialogBuilder.setTitle( context.getResources().getString( R.string.error) );
		alertDialogBuilder.setIcon( context.getResources().getDrawable( R.drawable.ic_cancel) );
		
		alertDialogBuilder.setMessage(getResources().getString(R.string.archive_order));
		
		alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener()
		{
			public void onClick( DialogInterface dialog, int which )
			{
				dialog.cancel();
			}
		} );
		
		alertDialogBuilder.setPositiveButton( "Yes", new DialogInterface.OnClickListener()
		{
			public void onClick( DialogInterface dialog, int which )
			{
				dialog.cancel();
				archiveSchedule(schedule, context);
			}
		} );
		
		return alertDialogBuilder.create();
	}
	
	private void archiveSchedule(ScheduleJob schedule, Context context)
	{
		String query = schedule.getQuery();
		
		int articleStart = query.indexOf("articles") + 9;
		int articleEnd = query.indexOf("code") - 1;
		String articlesString = query.substring(articleStart, articleEnd);
		/*
		String [] articlesArray = articlesString.split(";");
		
		List<String> articleList = new ArrayList<String>();
		String articles = "";
		for(int x = 0; x < articlesArray.length; x++)
		{
			String article = articlesArray[x];
			String [] articleSegments = article.split(":");
			article = articleSegments[0] + ":" + articleSegments[1] + ":" + articleSegments[3];
			if(x+1 < articlesArray.length)
			{
				article += ",";
			}
			articles += article;
		}
*/
		ContentValues values = new ContentValues();

		ClientEntity client = CommonUtils.getClientByQuery(context, query);
		
		values.put("archive_type", "order");
		values.put("owner", ( client.getStatus().equalsIgnoreCase("CREADO") ) ? client.getId() : client.getKunnr());
		values.put("state", articlesString);
		values.put("date", new Utility().getDateTime());
		values.put("owner_name", client.getName());

		int idArchive = ProcessMemento.save(context, values);
		
		ScheduleDataModel.deleteScheduleJob(context, schedule.getId());

		Intent intent = new Intent( context, BillActivity.class );		
		intent.putExtra(BillActivity.EXTRA_CLIENT_ENTITY, client );
		intent.putExtra(ID_ARCHIVE, idArchive);
		context.startActivity( intent );
	}
	

}

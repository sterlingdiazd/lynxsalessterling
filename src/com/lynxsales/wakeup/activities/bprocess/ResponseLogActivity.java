package com.lynxsales.wakeup.activities.bprocess;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.lynx.charges.Utility;
import com.lynxsales.wakeup.ProcessMemento;
import com.lynxsales.wakeup.activities.adapter.PendingAdapter;
import com.lynxsales.wakeup.utils.CommonUtils;
import com.lynxsales.wakeup.R;
import com.lynx.sales.bundle.dto.model.ScheduleDataModel;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob.Status;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.entities.ProductEntity;
import com.lynx.sales.bundle.http.request.ContextException;
import com.lynx.sales.bundle.msg.MessageFormatter;
import com.lynx.sales.bundle.msg.MessageFormatter.FilterHandler;
import com.lynx.sales.bundle.msg.MessageFormatter.Message;
import com.lynx.sales.bundle.msg.MessageFormatter.MessageHandler;

public class ResponseLogActivity extends ListActivity{
	
	
	private List<ScheduleJob> jobs;
	public static final String ID_ARCHIVE = "ID_ARCHIVE";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView( R.layout.list_activity_content );
		init();
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return false;
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setAdapter();
	}


	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) 
	{
		ScheduleJob schedule = (ScheduleJob) l.getItemAtPosition(position);
		if(schedule.getStatus() == Status.REJECTED)
		{
			createReprocessDialog(v.getContext(), schedule).show();;
		}
		else if(schedule.getStatus() == Status.PROCCESED)
		{
			createDialog( jobs.get(position) );
		} 
		else if (schedule.getStatus() == Status.WARNING)
		{
			createDialog( jobs.get(position) );
		}
		
	}
	
	private void init(){
		initActionBar();
		setAdapter();		
	}
	
	
	private void initActionBar(){
		getActionBar().setDisplayHomeAsUpEnabled( true );
		getActionBar().setTitle( getResources().getString(R.string.error_log) );
	}
	
	
	private void setAdapter(){
		jobs = getProccesedSchedules();
		PendingAdapter adapter = new PendingAdapter(this, R.layout.entry_pending, jobs );		
		setListAdapter(adapter);		
	}
	
	private List<ScheduleJob> getProccesedSchedules(){
		List<ScheduleJob> schedules = ScheduleDataModel.getShedulesJobs( this );
		List<ScheduleJob> permitted = new ArrayList<ScheduleJob>();
		
		if( null != schedules ){
			for( int i=0; schedules.size()> i;++i ){
				ScheduleJob job = schedules.get( i );
				if( Status.PENDING != job.getStatus() )
					permitted.add( job );
			}
		}
		return permitted;
	}
	
	private List<ScheduleJob> getRejectedSchedules(){
		List<ScheduleJob> processedschedules = ScheduleDataModel.getShedulesJobs( this );
		List<ScheduleJob> rejected = new ArrayList<ScheduleJob>();
		
		if( null != processedschedules ){
			for( int i=0; processedschedules.size()> i;++i ){
				ScheduleJob job = processedschedules.get( i );
				if( Status.REJECTED == job.getStatus() )
					rejected.add( job );
			}
		}
		return rejected;
	}
	
	private void createDialog( ScheduleJob job ){	
		Message msg = getFormattedMessage( job.getResponse() );
		
		if( null != msg ){
			Builder builder = new AlertDialog.Builder( this );
			builder.setCancelable( true );
			String mensaje = msg.toString();
			if(job.getStatus().toString().equalsIgnoreCase(Status.WARNING.toString()))
			{
				mensaje = mensaje + " " + getResources().getString(R.string.credit_limit_locking);
			}
			builder.setMessage( mensaje );
			builder.setTitle( job.getResponsible() );
			builder.setPositiveButton("OK", null );
			builder.create().show();
		} else {
			Builder builder = new AlertDialog.Builder( this );
			builder.setCancelable( true );
			builder.setMessage( "There is not SAP message. Maybe the connection timed out." );
			builder.setTitle( "Error" );
			builder.setPositiveButton("OK", null );
			builder.create().show();
		}
	}
	
	private Dialog createReprocessDialog(final Context context, final ScheduleJob schedule)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( context );
		alertDialogBuilder.setTitle( context.getResources().getString( R.string.error) );
		alertDialogBuilder.setIcon( context.getResources().getDrawable( R.drawable.ic_cancel) );
		
		Message msg = getFormattedMessage( schedule.getResponse() );
		String mensaje = "";
		if(msg == null)
		{
			JSONObject obj;
			try {
				obj = new JSONObject(schedule.getResponse());
				mensaje = obj.getString("Message");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		}
		alertDialogBuilder.setMessage( msg.subject + "\n" + getResources().getString(R.string.resend_order));
		
		alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener()
		{
			public void onClick( DialogInterface dialog, int which )
			{
				dialog.cancel();
			}
		} );
		
		alertDialogBuilder.setPositiveButton( getResources().getString(R.string.yes), new DialogInterface.OnClickListener()
		{
			public void onClick( DialogInterface dialog, int which )
			{
				dialog.cancel();
				Toast.makeText(context, getResources().getString(R.string.now_archived), Toast.LENGTH_SHORT).show();
				archiveSchedule(schedule, context);
			}
		} );
		
		return alertDialogBuilder.create();
	}
	
	private void archiveSchedule(ScheduleJob schedule, Context context)
	{
		String query = schedule.getQuery();
		
		int articleStart = query.indexOf("articles") + 9;
		int articleEnd = query.indexOf("code") - 1;
		String articlesString = query.substring(articleStart, articleEnd);
		/*
		String [] articlesArray = articlesString.split(";");
		
		List<String> articleList = new ArrayList<String>();
		String articles = "";
		for(int x = 0; x < articlesArray.length; x++)
		{
			String article = articlesArray[x];
			String [] articleSegments = article.split(";");

			if(x+1 < articlesArray.length)
			{
				article += ",";
			}
			
			articles += ;
		}
		*/
		ContentValues values = new ContentValues();

		ClientEntity client = CommonUtils.getClientByQuery(context, query);
		
		values.put("archive_type", "order");
		values.put("owner", (client.getStatus().equalsIgnoreCase("CREADO")) ? client.getId() : client.getKunnr());
		values.put("state",  articlesString);
		values.put("date", new Utility().getDateTime());
		//ClientEntity client = ClientRetrieval.getClient(context, owner);
		values.put("owner_name", client.getName());

		int idArchive = ProcessMemento.save(context, values);
		ScheduleDataModel.deleteScheduleJob(context, schedule.getId());
		

		Intent intent = new Intent( context, BillActivity.class );		
		intent.putExtra(BillActivity.EXTRA_CLIENT_ENTITY, client );
		intent.putExtra(ID_ARCHIVE, idArchive);
		context.startActivity( intent );
	}
	
	private String getSerializedProductState(ProductEntity product) {
		StringBuilder state = new StringBuilder();
		state.append(product.getId()).append(":").append(product.getAmount());
		return state.toString();
	}
	
	private Message getFormattedMessage( String response ){	
		final MessageFormatter msgFormatter  = MessageFormatter.getInstance( this );
		
		try 
		{
			if( response.toString().contains( "type" ) )
			{	
				Message msg = msgFormatter.format( response.toString(), new FilterHandler() {						
					@Override
					public boolean accept(String message, String type) {							
						return "002".equals(  message ) || "001".equals( message );
					}
					
					@Override
					public MessageHandler getHandler() {
						return new MessageHandler() {								
							@Override
							public Message getMessage(Map<String, String> keyValues) {
								if( "S".equals( keyValues.get( "type" ) ) ){
									String doc = String.format( getString( R.string.S_001 ), keyValues.get("doc") );
									
									return new Message( keyValues.get( "message" ), keyValues.get( "type" ),doc );
								}else{
									return new Message( keyValues.get( "message" ), keyValues.get( "type" ),keyValues.get("response"));
								}
							}
						};
					}
				} );
				return  msg;
			}
		} 
		catch(Exception e)
		{
			e.printStackTrace();
		}	
		return null;
	}
}

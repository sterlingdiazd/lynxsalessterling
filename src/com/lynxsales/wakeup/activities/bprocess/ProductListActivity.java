package com.lynxsales.wakeup.activities.bprocess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.dto.model.ProductDataModel;
import com.lynx.sales.bundle.dto.model.UnitsDataModel;
import com.lynx.sales.bundle.entities.CenterEntity;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.entities.ConditionEntity;
import com.lynx.sales.bundle.entities.ProductEntity;
import com.lynx.sales.bundle.entities.UnitEntity;
import com.lynxsales.printer.Configuration;
import com.lynxsales.wakeup.activities.BaseActivity;
import com.lynxsales.wakeup.activities.adapter.ProductListAdapter;
import com.lynxsales.wakeup.activities.adapter.ProductListAdapter.ProductEntry;
import com.lynxsales.wakeup.noapi.entities.ListWrapper;
import com.lynxsales.wakeup.utils.AlertDialogManager;
import com.lynxsales.wakeup.R;

public class ProductListActivity extends BaseActivity implements OnScrollListener
{

    public static final String EXTRA_BUSSINESS_CLIENT = "com.lynx.sales.activities.bprocess.ProductListActivity.EXTRA_BUSSINESS_CLIENT";
    public static final String EXTRA_PRODUCTS_SELECTED = "com.lynx.sales.activities.bprocess.ProductListActivity.EXTRA_PRODUCTS_SELECTED";
    public static final String EXTRA_ENTRIES_SELECTED = "com.lynx.sales.activities.bprocess.ProductListActivity.EXTRA_ENTRIES_SELECTED";

    private static final int FLIP_LOADING = 0;
    private static final int FLIP_NOT_FOUND = 1;
    private static final int FLIP_CONTENT = 2;
    private Configuration configuracion;
    private Activity activity;
    private ProductListAdapter adapter;
    private CenterEntity center;
    private ClientEntity client;
    private int indexSelected;
    private int start = 0;
    private int limit = 10;
    private SharedPreferences prefs;
    private ListView listProducts;
    private List<ProductEntity> products;
    private ArrayList<Integer> sequences = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_product_list);
	init();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
	super.onActivityResult(requestCode, resultCode, data);
	if (resultCode == RESULT_OK && data != null)
	{
	    ProductEntity backed = (ProductEntity) data.getExtras()
		    .getSerializable(ProductDetailActivity.EXTRA_BACKED_PRODUCT);
	    updateProduct(indexSelected, backed);
	    Log.e("*****", "amount " + backed.getAmount() + ", center: " + backed.getCenter());
	}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
	getMenuInflater().inflate(R.menu.product_list, menu);

	SearchView search = (SearchView) menu.findItem(R.id.action_search).getActionView();
	search.setQueryHint(getResources().getString(R.string.search_product));

	search.setOnQueryTextListener(queryListener);

	int id = search.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
	TextView text = (TextView) search.findViewById(id);
	text.setHintTextColor(Color.WHITE);

	return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy()
    {
	super.onDestroy();

	/*
	 * if(products != null) products.clear(); products = null;
	 */
    }

    /*
     * @Override protected void onResume() { super.onResume(); products =
     * resolveProduct(client); }
     */
    private void init()
    {
	initActionBar();
	initComponents();
	fetchClientProduct();
    }

    private void initActionBar()
    {
	getActionBar().setTitle(getResources().getString(R.string.product_list));
	getActionBar().setDisplayHomeAsUpEnabled(true);
	getActionBar().setHomeButtonEnabled(true);
    }

    private void initComponents()
    {
	this.activity = ProductListActivity.this;
	configuracion = Configuration.getInstance();
	configuracion.configureSharePreference(activity);
	listProducts = (ListView) findViewById(android.R.id.list);
	listProducts.setOnItemClickListener(listClickListener);
	listProducts.setOnScrollListener(this);
	prefs = PreferenceManager.getDefaultSharedPreferences(this);
	if (getIntent().getExtras() != null)
	{
	    client = (ClientEntity) getIntent().getExtras().getSerializable(EXTRA_BUSSINESS_CLIENT);
	    center = (CenterEntity) getIntent().getExtras().getSerializable(BillActivity.CENTER);
	}
    }

    private void fetchClientProduct()
    {
	try
	{
	    products = resolveProduct(client);
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
    }

    private List<ProductEntity> resolveProduct(final ClientEntity client2)
    {
	new AsyncTask<Void, List<ProductEntity>, List<ProductEntity>>()
	{
	    @Override
	    protected void onPreExecute()
	    {
		fadeFlipView(R.id.flipper_view, FLIP_LOADING);
	    }

	    @Override

	    protected List<ProductEntity> doInBackground(Void... params)
	    {
		boolean centerReady = false;

		if (center != null)
		{
		    List<ProductEntity> pro = ProductDataModel.getProductsByCenterAndStore(ProductListActivity.this, center, client2, limit, start);

		    for (int x = 0; x < pro.size(); x++)
		    {
			try
			{
			    ProductEntity productFull = calculatePrice(pro.get(x));
			    pro.get(x).setPrice(productFull.getPrice());
			    pro.get(x).setOffer(productFull.getOffer());
			    List<UnitEntity> salesUnits = UnitsDataModel.getUnitsByProduct(ProductListActivity.this,
				    productFull.getMaterial());
			    pro.get(x).setSalesUnits(salesUnits);
			    if (salesUnits.size() > 0)
				pro.get(x).setSelectedSalesUnit(salesUnits.get(0));
			    pro.get(x).setDiscount(productFull.getDiscount());
			    
			} 
			catch (Exception e)
			{
			    e.printStackTrace();
			}

		    }
		    start += pro.size();
		    products = pro;
		    // ProductDataModel.bulkSaveProductsByCenter(context, pro);
		    return products;

		}
		return products;
	    }

	    

	    protected void onPostExecute(List<ProductEntity> result)
	    {
		onDone(result);
	    }
	}.execute();

	return products;
    }

    private ProductEntity calculatePrice(ProductEntity product)
    {
	SQLiteDatabase db = Database.getReadableDatabase(activity);
	Cursor cursorConditionType = null;

	try
	{
	    cursorConditionType = db.rawQuery(
		    "select kschl from conditions where vkorg = '" + client.getOrganizacion() + "' "
			    + "and vtweg = '" + client.getCanal() + "' group by kschl order by kschl desc",
		    null);

	    if (cursorConditionType != null)
	    {
		for (cursorConditionType.moveToFirst(); !cursorConditionType.isAfterLast(); cursorConditionType
			.moveToNext())
		{
		    String conditionType = cursorConditionType
			    .getString(cursorConditionType.getColumnIndexOrThrow("kschl"));
		    if (conditionType.equalsIgnoreCase("PR00"))
		    {
			Cursor cursorSequence = db.rawQuery(
				"select sequence from conditions where vkorg = '" + client.getOrganizacion()
					+ "' " + "and vtweg = '" + client.getCanal() + "' and kschl = '"
					+ conditionType + "' group by sequence order by sequence asc",
				null);

			try
			{
			    if (cursorSequence != null)
			    {
				sequences.clear();
				for (cursorSequence.moveToFirst(); !cursorSequence.isAfterLast(); cursorSequence
					.moveToNext())
				{
				    String sequence = cursorSequence
					    .getString(cursorSequence.getColumnIndexOrThrow("sequence"));
				    sequences.add(Integer.valueOf(sequence));
				}
			    }
			} catch (Exception e)
			{
			    e.printStackTrace();
			} finally
			{
			    if (cursorSequence != null)
			    {
				cursorSequence.close();
			    }
			}

			for (Integer currentPriceSequence : sequences)
			{
			    if (currentPriceSequence == 1)
			    {
				Cursor cursorClienteMaterial = db
					.rawQuery("select kbetr from conditions where vkorg = '"
						+ client.getOrganizacion() + "' " + "and vtweg = '"
						+ client.getCanal() + "' and kschl = '" + conditionType
						+ "' and sequence = '" + currentPriceSequence + "' "
						+ "and kunnr like '%" + client.getKunnr()
						+ "%' and matnr like '%" + product.getMaterial() + "%'"
						+ " order by sequence asc", null);

				try
				{
				    if (cursorClienteMaterial != null)
				    {
					for (cursorClienteMaterial.moveToFirst(); !cursorClienteMaterial
						.isAfterLast(); cursorClienteMaterial.moveToNext())
					{
					    String kbetr = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("kbetr"));
					    if (product.getPrice() == null)
					    {
						product.setPrice(kbetr);
						break;
					    } else
					    {
						break;
					    }
					}
				    }
				} catch (Exception e)
				{
				    e.printStackTrace();
				} finally
				{
				    if (cursorClienteMaterial != null)
				    {
					cursorClienteMaterial.close();
				    }
				}

			    } else if (currentPriceSequence == 2)
			    {
				Cursor cursorSectorRamoMaterial = db
					.rawQuery("select kbetr from conditions where vkorg = '"
						+ client.getOrganizacion() + "' " + "and vtweg = '"
						+ client.getCanal() + "' and kschl = '" + conditionType
						+ "' and sequence = '" + currentPriceSequence + "' "
						+ "and spart like '%" + client.getSector()
						+ "%' and brsch like '%" + client.getRamo()
						+ "%'  and matnr like '%" + product.getMaterial() + "%'"
						+ " order by sequence asc", null);

				try
				{
				    if (cursorSectorRamoMaterial != null)
				    {
					for (cursorSectorRamoMaterial.moveToFirst(); !cursorSectorRamoMaterial
						.isAfterLast(); cursorSectorRamoMaterial.moveToNext())
					{
					    String kbetr = cursorSectorRamoMaterial.getString(
						    cursorSectorRamoMaterial.getColumnIndexOrThrow("kbetr"));
					    cursorSectorRamoMaterial.close();
					    if (product.getPrice() == null)
					    {
						product.setPrice(kbetr);
						break;
					    } else
					    {
						break;
					    }
					}
				    }
				} catch (Exception e)
				{
				    e.printStackTrace();
				} finally
				{
				    if (cursorSectorRamoMaterial != null)
				    {
					cursorSectorRamoMaterial.close();
				    }
				}
			    } else if (currentPriceSequence == 3)
			    {
				Cursor cursorMaterial = db
					.rawQuery("select kbetr from conditions where vkorg = '"
						+ client.getOrganizacion() + "' " + "and vtweg = '"
						+ client.getCanal() + "' and kschl = '" + conditionType
						+ "' and sequence = '" + currentPriceSequence + "' "
						+ "and matnr like '%" + product.getMaterial() + "%'"
						+ " order by sequence asc", null);

				try
				{
				    if (cursorMaterial != null)
				    {
					for (cursorMaterial
						.moveToFirst(); !cursorMaterial.isAfterLast(); cursorMaterial
							.moveToNext())
					{
					    String kbetr = cursorMaterial
						    .getString(cursorMaterial.getColumnIndexOrThrow("kbetr"));
					    cursorMaterial.close();
					    if (product.getPrice() == null)
					    {
						product.setPrice(kbetr);
						break;
					    } else
					    {
						break;
					    }
					}
				    }
				} catch (Exception e)
				{
				    e.printStackTrace();
				} finally
				{
				    if (cursorMaterial != null)
				    {
					cursorMaterial.close();
				    }
				}

			    }

			}

		    } else if (conditionType.equalsIgnoreCase("NA00"))
		    {

			Cursor cursorSequence = db.rawQuery(
				"select sequence from conditions where vkorg = '" + client.getOrganizacion()
					+ "' " + "and vtweg = '" + client.getCanal() + "' and kschl = '"
					+ conditionType + "' group by sequence order by sequence asc",
				null);

			try
			{
			    if (cursorSequence != null)
			    {
				sequences.clear();
				for (cursorSequence.moveToFirst(); !cursorSequence.isAfterLast(); cursorSequence
					.moveToNext())
				{
				    String sequence = cursorSequence
					    .getString(cursorSequence.getColumnIndexOrThrow("sequence"));
				    sequences.add(Integer.valueOf(sequence));
				}
			    }
			} catch (Exception e)
			{
			    e.printStackTrace();
			} finally
			{
			    if (cursorSequence != null)
			    {
				cursorSequence.close();
			    }
			}

			for (Integer currentBonusSequence : sequences)
			{

			    if (currentBonusSequence == 1)
			    {
				Cursor cursorClienteMaterial = db
					.rawQuery(
						"select knrmm, knrnm, knrme, knrzm, knrez, knrmat from conditions"
							+ " where kschl = '" + conditionType + "'"
							+ " and vkorg = '" + client.getOrganizacion() + "'"
							+ " and vtweg = '" + client.getCanal() + "'"
							+ " and sequence = '" + currentBonusSequence + "'"
							+ " and kunnr like '%" + client.getKunnr() + "%'"
							+ " and matnr like '%" + product.getMaterial() + "%'",
						null);

				try
				{
				    if (cursorClienteMaterial != null)
				    {
					for (cursorClienteMaterial.moveToFirst(); !cursorClienteMaterial
						.isAfterLast(); cursorClienteMaterial.moveToNext())
					{
					    String minimunQuantity = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrmm"));
					    String bonificableQuantity = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrnm"));
					    String unitMeasureQuantity = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrme"));
					    String bonificatedQuantity = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrzm"));
					    String unitMeasureBonificated = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrez"));
					    String matAdcBonificable = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrmat"));
					    cursorClienteMaterial.close();

					    String offer = "De: " + minimunQuantity + ": " + bonificableQuantity
						    + " " + unitMeasureQuantity + " + " + bonificatedQuantity
						    + " " + unitMeasureBonificated;

					    if (matAdcBonificable.length() > 0)
						product.setKnrmat(matAdcBonificable);

					    if (product.getOffer() == null)
					    {
						product.setOffer(offer.trim());
						break;
					    } else
					    {
						break;
					    }
					}
				    }
				} catch (Exception e)
				{
				    e.printStackTrace();
				} finally
				{
				    if (cursorClienteMaterial != null)
				    {
					cursorClienteMaterial.close();
				    }
				}

			    } else if (currentBonusSequence == 2)
			    {
				Cursor cursorSectorGrupoClienteMaterial = db
					.rawQuery(
						"select knrmm, knrnm, knrme, knrzm, knrez, knrmat from conditions"
							+ " where kschl = '" + conditionType + "'"
							+ " and vkorg = '" + client.getOrganizacion() + "'"
							+ " and vtweg = '" + client.getCanal() + "'"
							+ " and sequence = '" + currentBonusSequence + "'"
							+ " and spart like '%" + client.getSector() + "%'"
							+ " and kdgrp like '%" + client.getKdgrp() + "%'"
							+ " and matnr like '%" + product.getMaterial() + "%'",
						null);

				try
				{
				    if (cursorSectorGrupoClienteMaterial != null)
				    {
					for (cursorSectorGrupoClienteMaterial
						.moveToFirst(); !cursorSectorGrupoClienteMaterial
							.isAfterLast(); cursorSectorGrupoClienteMaterial
								.moveToNext())
					{
					    String minimunQuantity = cursorSectorGrupoClienteMaterial
						    .getString(cursorSectorGrupoClienteMaterial
							    .getColumnIndexOrThrow("knrmm"));
					    String bonificableQuantity = cursorSectorGrupoClienteMaterial
						    .getString(cursorSectorGrupoClienteMaterial
							    .getColumnIndexOrThrow("knrnm"));
					    String unitMeasureQuantity = cursorSectorGrupoClienteMaterial
						    .getString(cursorSectorGrupoClienteMaterial
							    .getColumnIndexOrThrow("knrme"));
					    String bonificatedQuantity = cursorSectorGrupoClienteMaterial
						    .getString(cursorSectorGrupoClienteMaterial
							    .getColumnIndexOrThrow("knrzm"));
					    String unitMeasureBonificated = cursorSectorGrupoClienteMaterial
						    .getString(cursorSectorGrupoClienteMaterial
							    .getColumnIndexOrThrow("knrez"));
					    String matAdcBonificable = cursorSectorGrupoClienteMaterial
						    .getString(cursorSectorGrupoClienteMaterial
							    .getColumnIndexOrThrow("knrmat"));
					    cursorSectorGrupoClienteMaterial.close();

					    String offer = "De: " + minimunQuantity + ": " + bonificableQuantity
						    + " " + unitMeasureQuantity + " + " + bonificatedQuantity
						    + " " + unitMeasureBonificated;

					    if (matAdcBonificable.length() > 0)
						product.setKnrmat(matAdcBonificable);

					    if (product.getOffer() == null)
					    {
						product.setOffer(offer.trim());
						break;
					    } else
					    {
						break;
					    }
					}
				    }
				} catch (Exception e)
				{
				    e.printStackTrace();
				} finally
				{
				    if (cursorSectorGrupoClienteMaterial != null)
				    {
					cursorSectorGrupoClienteMaterial.close();
				    }
				}

			    } else if (currentBonusSequence == 3)
			    {
				Cursor cursorJerarquiaProductoMaterial = db
					.rawQuery(
						"select knrmm, knrnm, knrme, knrzm, knrez, knrmat from conditions"
							+ " where kschl = '" + conditionType + "'"
							+ " and vkorg = '" + client.getOrganizacion() + "'"
							+ " and vtweg = '" + client.getCanal() + "'"
							+ " and sequence = '" + currentBonusSequence + "'"
							+ " and spart like '%" + client.getSector() + "%'"
							+ " and PRODH like '%" + product.getProdh() + "%'"
							+ " and KUNNR like '%" + product.getMaterial() + "%'",
						null);

				try
				{
				    if (cursorJerarquiaProductoMaterial != null)
				    {
					for (cursorJerarquiaProductoMaterial
						.moveToFirst(); !cursorJerarquiaProductoMaterial
							.isAfterLast(); cursorJerarquiaProductoMaterial
								.moveToNext())
					{
					    String minimunQuantity = cursorJerarquiaProductoMaterial
						    .getString(cursorJerarquiaProductoMaterial
							    .getColumnIndexOrThrow("knrmm"));
					    String bonificableQuantity = cursorJerarquiaProductoMaterial
						    .getString(cursorJerarquiaProductoMaterial
							    .getColumnIndexOrThrow("knrnm"));
					    String unitMeasureQuantity = cursorJerarquiaProductoMaterial
						    .getString(cursorJerarquiaProductoMaterial
							    .getColumnIndexOrThrow("knrme"));
					    String bonificatedQuantity = cursorJerarquiaProductoMaterial
						    .getString(cursorJerarquiaProductoMaterial
							    .getColumnIndexOrThrow("knrzm"));
					    String unitMeasureBonificated = cursorJerarquiaProductoMaterial
						    .getString(cursorJerarquiaProductoMaterial
							    .getColumnIndexOrThrow("knrez"));
					    String matAdcBonificable = cursorJerarquiaProductoMaterial
						    .getString(cursorJerarquiaProductoMaterial
							    .getColumnIndexOrThrow("knrmat"));

					    String offer = "De: " + minimunQuantity + ": " + bonificableQuantity
						    + " " + unitMeasureQuantity + " + " + bonificatedQuantity
						    + " " + unitMeasureBonificated;

					    if (matAdcBonificable.length() > 0)
						product.setKnrmat(matAdcBonificable);

					    if (product.getOffer() == null)
					    {
						product.setOffer(offer.trim());
						break;
					    } else
					    {
						break;
					    }
					}
				    }
				} catch (Exception e)
				{
				    e.printStackTrace();
				} finally
				{
				    if (cursorJerarquiaProductoMaterial != null)
				    {
					cursorJerarquiaProductoMaterial.close();
				    }
				}

			    } else if (Integer.valueOf(currentBonusSequence) == 4)
			    {
				Cursor cursorClienteMaterial = db
					.rawQuery(
						"select knrmm, knrnm, knrme, knrzm, knrez, knrmat from conditions"
							+ " where kschl = '" + conditionType + "'"
							+ " and vkorg = '" + client.getOrganizacion() + "'"
							+ " and vtweg = '" + client.getCanal() + "'"
							+ " and sequence = '" + currentBonusSequence + "'"
							+ " and spart like '%" + client.getSector() + "%'"
							+ " and BRSCH like '%" + client.getRamo() + "%'"
							+ " and matnr like '%" + product.getMaterial() + "%'",
						null);

				try
				{
				    if (cursorClienteMaterial != null)
				    {
					for (cursorClienteMaterial.moveToFirst(); !cursorClienteMaterial
						.isAfterLast(); cursorClienteMaterial.moveToNext())
					{
					    String minimunQuantity = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrmm"));
					    String bonificableQuantity = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrnm"));
					    String unitMeasureQuantity = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrme"));
					    String bonificatedQuantity = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrzm"));
					    String unitMeasureBonificated = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrez"));
					    String matAdcBonificable = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrmat"));
					    cursorClienteMaterial.close();

					    String offer = "De: " + minimunQuantity + ": " + bonificableQuantity
						    + " " + unitMeasureQuantity + " + " + bonificatedQuantity
						    + " " + unitMeasureBonificated;

					    if (matAdcBonificable.length() > 0)
						product.setKnrmat(matAdcBonificable);

					    if (product.getOffer() == null)
					    {
						product.setOffer(offer.trim());
						break;
					    } else
					    {
						break;
					    }
					}
				    }
				} catch (Exception e)
				{
				    e.printStackTrace();
				} finally
				{
				    if (cursorClienteMaterial != null)
				    {
					cursorClienteMaterial.close();
				    }
				}

			    } else if (currentBonusSequence == 5)
			    {
				Cursor cursorClienteMaterial = db
					.rawQuery(
						"select knrmm, knrnm, knrme, knrzm, knrez, knrmat from conditions"
							+ " where kschl = '" + conditionType + "'"
							+ " and vkorg = '" + client.getOrganizacion() + "'"
							+ " and vtweg = '" + client.getCanal() + "'"
							+ " and sequence = '" + currentBonusSequence + "'"
							+ " and spart like '%" + client.getSector() + "%'"
							+ " and BRSCH like '%" + client.getRamo() + "%'"
							+ " and PRODH like '%" + product.getProdh() + "%'",
						null);

				try
				{
				    if (cursorClienteMaterial != null)
				    {
					for (cursorClienteMaterial.moveToFirst(); !cursorClienteMaterial
						.isAfterLast(); cursorClienteMaterial.moveToNext())
					{
					    String minimunQuantity = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrmm"));
					    String bonificableQuantity = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrnm"));
					    String unitMeasureQuantity = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrme"));
					    String bonificatedQuantity = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrzm"));
					    String unitMeasureBonificated = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrez"));
					    String matAdcBonificable = cursorClienteMaterial.getString(
						    cursorClienteMaterial.getColumnIndexOrThrow("knrmat"));
					    cursorClienteMaterial.close();

					    String offer = "De: " + minimunQuantity + ": " + bonificableQuantity
						    + " " + unitMeasureQuantity + " + " + bonificatedQuantity
						    + " " + unitMeasureBonificated;

					    if (matAdcBonificable.length() > 0)
						product.setKnrmat(matAdcBonificable);

					    if (product.getOffer() == null)
					    {
						product.setOffer(offer.trim());
						break;
					    } else
					    {
						break;
					    }
					}
				    }
				} catch (Exception e)
				{
				    e.printStackTrace();
				} finally
				{
				    if (cursorClienteMaterial != null)
				    {
					cursorClienteMaterial.close();
				    }
				}

			    }
			}

		    } else if (conditionType.equalsIgnoreCase("KA00"))
		    {
			Cursor cursorSequence = db.rawQuery(
				"select sequence from conditions where vkorg = '" + client.getOrganizacion()
					+ "' " + "and vtweg = '" + client.getCanal() + "' and kschl = '"
					+ conditionType + "' group by sequence order by sequence asc",
				null);

			try
			{
			    if (cursorSequence != null)
			    {
				sequences.clear();
				for (cursorSequence.moveToFirst(); !cursorSequence.isAfterLast(); cursorSequence
					.moveToNext())
				{
				    String sequence = cursorSequence
					    .getString(cursorSequence.getColumnIndexOrThrow("sequence"));
				    sequences.add(Integer.valueOf(sequence));
				}
			    }
			} catch (Exception e)
			{
			    e.printStackTrace();
			} finally
			{
			    if (cursorSequence != null)
			    {
				cursorSequence.close();
			    }
			}

			for (Integer currentDiscountSequence : sequences)
			{
			    if (currentDiscountSequence == 1)
			    {
				Cursor cursorClienteMaterial = db
					.rawQuery("select kbetr from conditions where vkorg = '"
						+ client.getOrganizacion() + "' " + "and vtweg = '"
						+ client.getCanal() + "' and kschl = '" + conditionType
						+ "' and sequence = '" + currentDiscountSequence + "' "
						+ "and kunnr like '%" + client.getKunnr()
						+ "%' and matnr like '%" + product.getMaterial() + "%'"
						+ " order by sequence asc", null);

				while (cursorClienteMaterial.moveToNext())
				{
				    String kbetr = cursorClienteMaterial
					    .getString(cursorClienteMaterial.getColumnIndexOrThrow("kbetr"));
				    cursorClienteMaterial.close();

				    if (product.getDiscount() == null)
				    {
					product.setDiscount(kbetr);
					break;
				    } else
				    {
					break;
				    }
				}
			    } else if (currentDiscountSequence == 2)
			    {
				Cursor cursorSectorRamoMaterial = db
					.rawQuery("select kbetr from conditions where vkorg = '"
						+ client.getOrganizacion() + "' " + "and vtweg = '"
						+ client.getCanal() + "' and kschl = '" + conditionType
						+ "' and sequence = '" + currentDiscountSequence + "' "
						+ "and spart like '%" + client.getSector()
						+ "%' and brsch like '%" + client.getRamo()
						+ "%'  and matnr like '%" + product.getMaterial() + "%'"
						+ " order by sequence asc", null);

				while (cursorSectorRamoMaterial.moveToNext())
				{
				    String kbetr = cursorSectorRamoMaterial
					    .getString(cursorSectorRamoMaterial.getColumnIndexOrThrow("kbetr"));
				    cursorSectorRamoMaterial.close();

				    if (product.getDiscount() == null)
				    {
					product.setDiscount(kbetr);
					break;
				    } else
				    {
					break;
				    }
				}
			    } else if (currentDiscountSequence == 3)
			    {
				Cursor cursorMaterial = db
					.rawQuery("select kbetr from conditions where vkorg = '"
						+ client.getOrganizacion() + "' " + "and vtweg = '"
						+ client.getCanal() + "' and kschl = '" + conditionType
						+ "' and sequence = '" + currentDiscountSequence + "' "
						+ "and matnr like '%" + product.getMaterial() + "%'"
						+ " order by sequence asc", null);

				while (cursorMaterial.moveToNext())
				{
				    String kbetr = cursorMaterial
					    .getString(cursorMaterial.getColumnIndexOrThrow("kbetr"));
				    cursorMaterial.close();

				    if (product.getDiscount() == null)
				    {
					product.setDiscount(kbetr);
					break;
				    } else
				    {
					break;
				    }
				}
			    }
			}

		    }
		}
	    }
	} catch (Exception e)
	{
	    e.printStackTrace();
	} finally
	{
	    if (cursorConditionType != null)
	    {
		cursorConditionType.close();
	    }

	}

	return product;
    }
    
    private void onDone(List<ProductEntity> products)
    {
	if (products != null && !products.isEmpty())
	{
	    adapter = new ProductListAdapter(this, R.layout.entry_product, products, client, center);
	    listProducts.setAdapter(adapter);
	    fadeFlipView(R.id.flipper_view, FLIP_CONTENT);
	} else
	{
	    fadeFlipView(R.id.flipper_view, FLIP_NOT_FOUND);
	}
    }

    public void onAddSelections(MenuItem item)
    {
	ListWrapper<List<ProductEntity>> entityWrapper = new ListWrapper<List<ProductEntity>>();
	ListWrapper<List<ProductEntry>> entryWrapper = new ListWrapper<List<ProductEntry>>();

	if (null != products && 0 != products.size())
	{
	    Log.e("*********", "onAddSelections:products.size(): " + products.size());

	    List<ProductEntity> selections = new ArrayList<ProductEntity>();
	    List<ProductEntry> entries = new ArrayList<ProductEntry>();

	    HashMap<String, ProductEntry> checkeds = adapter.getSelections();
	    for (Map.Entry<String, ProductEntry> entry : checkeds.entrySet())
	    {
		if (entry.getValue().selected)
		{
		    ProductEntity product = null;
		    for (int x = 0; x < products.size(); x++)
		    {
			if (products.get(x).getMaterial().equalsIgnoreCase(entry.getKey()))
			{
			    product = products.get(x);
			    break;
			}
		    }

		    if (product != null)
		    {
			if (center.getName() != null)
			{
			    product.setCenter(center.getName());
			}
			selections.add(product);
			entries.add(entry.getValue());
		    }

		}
	    }
	    entityWrapper.setList(selections);
	    entryWrapper.setList(entries);
	} else
	{
	    new AlertDialogManager().showAlertDialog(this, "No hay Productos", "No hay productos en la lista", true);
	}

	try
	{
	    Intent result = new Intent();
	    result.putExtra(EXTRA_PRODUCTS_SELECTED, entityWrapper);
	    result.putExtra(EXTRA_ENTRIES_SELECTED, entryWrapper);

	    setResult(RESULT_OK, result);
	    finish();
	} catch (Exception ex)
	{
	    ex.printStackTrace();
	}

    }

    private void updateProduct(int index, ProductEntity backed)
    {
	ProductEntity selected = products.get(index);
	selected.setAmount(backed.getAmount());
	selected.setCenter(backed.getCenter());
	selected.setSelectedSalesUnit(backed.getSelectedSalesUnit());
	adapter.notifyDataSetChanged();
    }

    private final OnItemClickListener listClickListener = new OnItemClickListener()
    {
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id)
	{
	    indexSelected = position;
	    ProductEntity selected = products.get(position);
	    Intent detail = new Intent(ProductListActivity.this, ProductDetailActivity.class);
	    detail.putExtra(ProductDetailActivity.EXTRA_SELECTED_PRODUCT, selected);
	    (activity).startActivityForResult(detail, 0);
	}
    };

    private final OnQueryTextListener queryListener = new OnQueryTextListener()
    {
	@Override
	public boolean onQueryTextSubmit(String query)
	{
	    if (query.length() > 0)
	    {
		if (adapter != null)
		{
		    adapter.getFilter().filter(query);
		    return true;
		} else {
		    return false;
		}
	    } else {
		return false;
	    }
	}

	@Override
	public boolean onQueryTextChange(String query)
	{
	    /*
	    if (query.length() > 0)
	    {
		if (adapter != null)
		{
		    adapter.getFilter().filter(query);
		    return true;
		} else {
		    return false;
		}
	    } else {
		return false;
	    }
	    */
	    return false;
	}
    };

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
    {
	int itemsVisibles = firstVisibleItem + visibleItemCount;
        if (itemsVisibles > 5 && itemsVisibles == totalItemCount) {
            loadMoreItems();
        }
	
    }

    private void loadMoreItems()
    {
	List<ProductEntity> pro = ProductDataModel.getProductsByCenterAndStore(this, center, client, limit, start);

	if (pro != null && pro.size() > 0)
	{
	    try
		{
		for(int x = 0; x < pro.size(); x++)
		{
		    ProductEntity productFull = calculatePrice(pro.get(x));
		    pro.get(x).setPrice(productFull.getPrice());
		    pro.get(x).setOffer(productFull.getOffer());
		    List<UnitEntity> salesUnits = UnitsDataModel.getUnitsByProduct(ProductListActivity.this, productFull.getMaterial());
		    pro.get(x).setSalesUnits(salesUnits);
		    if (salesUnits.size() > 0)
			pro.get(x).setSelectedSalesUnit(salesUnits.get(0));
		    pro.get(x).setDiscount(productFull.getDiscount());
		    
		}
		start += pro.size();
		    
		} 
		catch (Exception e)
		{
		    e.printStackTrace();
		}
	    
	    products.addAll(pro);
	    adapter.addAll(pro);
	    adapter.setCachedProducts(products);
	    adapter.notifyDataSetChanged();
	}

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState)
    {
	// TODO Auto-generated method stub
	
    }
}

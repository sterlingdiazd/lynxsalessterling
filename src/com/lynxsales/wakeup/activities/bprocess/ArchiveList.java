package com.lynxsales.wakeup.activities.bprocess;


import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.lynx.sales.bundle.dto.model.ArchiveDataModel;
import com.lynx.sales.bundle.dto.model.ClientDataModel;
import com.lynx.sales.bundle.entities.Archive;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynxsales.wakeup.activities.adapter.ArchiveAdapter;
import com.lynxsales.wakeup.R;

public class ArchiveList extends ListActivity implements OnItemClickListener {	
	
	private ListView list;
	private Context context;
	private Activity activity;
	private ClientEntity client;
	public static final String EXTRA_CLIENT_ENTITY = "com.lynx.sales.activities.bprocess.BillActivity.EXTRA_CLIENT_ENTITY";
	public static final String DONT_SHOW_ARCHIVED_ORDERS = "dont_show_archived_orders";
	public static final String ID_ARCHIVE = "ID_ARCHIVE";
	private ArchiveAdapter adapter;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView( R.layout.list_activity_content );
		initActionBar();	
		
		this.context = getBaseContext();
		this.activity = ArchiveList.this;
		getBussinessClient();
		adapter = new ArchiveAdapter(context, getArchivesOfClient(), false, null);		
		
		list = (ListView) findViewById(android.R.id.list);
		list.setOnItemClickListener(this);
		list.setAdapter(adapter);
		list.refreshDrawableState();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		adapter = new ArchiveAdapter(context, getArchivesOfClient(), false, null);		
		list.setAdapter(adapter);
		list.refreshDrawableState();
	}
	
	
	private List<Archive> getArchivesOfClient()
	{
		List<Archive> archives = ArchiveDataModel.getArchives(this);
		List<Archive> clientArchives = new ArrayList<Archive>();
		
		if(client != null)
		{
			for(Archive archive : archives)
			{
				if(client.getId().equalsIgnoreCase(archive.getOwner()))
				{
					clientArchives.add(archive);
				}				
			} 
			return clientArchives;
			
		}
		return archives;
	}


	private void getBussinessClient() 
	{
		if (getIntent().getExtras() != null) 
		{
			client = (ClientEntity) getIntent().getExtras().get(EXTRA_CLIENT_ENTITY);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.archive_menu, menu);
		return super.onCreateOptionsMenu(menu);

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return false;
		}
	}

	private void initActionBar(){
		getActionBar().setDisplayHomeAsUpEnabled( true );
		getActionBar().setTitle( getResources().getString(R.string.archived) );
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
	{
		Archive archive  = (Archive) parent.getItemAtPosition(position);
		
		if( archive != null &&  archive.getOwner() != null)
		{	
			ClientEntity client = ClientDataModel.getClientByExternalID(context, archive.getOwner());
			if(client.getAddress1() == null)
				client = ClientDataModel.getClient(context, archive.getOwner());
			dispatchActivity( BillActivity.class, archive.getId(), client,  false );
		}	
	}
	
	public void goToBillActivity(MenuItem item)
	{
		dispatchActivity( BillActivity.class, -1, null, true);
		
	}
	private void dispatchActivity(  Class<?> className, int idArchive, ClientEntity client, boolean showArchive )
	{
		if( className != null ){
			Intent intent = new Intent( activity, className );
			intent.putExtra(BillActivity.EXTRA_CLIENT_ENTITY, client );
			intent.putExtra(ID_ARCHIVE, idArchive);
			intent.putExtra(DONT_SHOW_ARCHIVED_ORDERS, showArchive);
			activity.startActivity( intent );
		}		
	}

		
	
	
	
	
}

package com.lynxsales.wakeup.activities.bprocess;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lynx.sales.bundle.dto.model.ArchiveDataModel;
import com.lynx.sales.bundle.dto.model.ClientDataModel;
import com.lynx.sales.bundle.entities.Archive;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.entities.ClientInfo;
import com.lynx.sales.bundle.entities.RouteEntity;
import com.lynxsales.printer.Configuration;
import com.lynxsales.wakeup.activities.BaseActivity;
import com.lynxsales.wakeup.activities.adapter.ArchiveAdapter;
import com.lynxsales.wakeup.noapi.request.Utils;
import com.lynxsales.wakeup.utils.LocationUtility;
import com.lynxsales.wakeup.R;

public class RouteActivity extends BaseActivity implements OnItemClickListener {

	public static final String EXTRA_ROUTE_DATA = "com.lynx.sales.activities.bprocess.RouteActivity.EXTRA_ROUTE_DATA";

	private RouteEntity route;
	private ClientEntity client;
	private ListView listArchive;
	private Activity activity;
	public static final String DONT_SHOW_ARCHIVED_ORDERS = "dont_show_archived_orders";
	public static final String ID_ARCHIVE = "ID_ARCHIVE";
	private LatLng position;
	private boolean isClientCreationEnabled;
	private Configuration configuracion;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_route);
		init();
	}

	
	@Override
	protected void onRestart() {
		super.onRestart();
		init();
	}

	private void init() {
		attachEntityToBussiness();
		initActionBar();
		setupFields();
		configureMap();
	}

	private void attachEntityToBussiness() {
		this.activity = RouteActivity.this;

		if (getIntent().getExtras() != null) {
			route = (RouteEntity) getIntent().getExtras().getSerializable(
					EXTRA_ROUTE_DATA);
			client = ClientDataModel.getClientByExternalID(this, route.getLazzyClientId());
		}
	}

	private void initActionBar() {
		if (client != null) {
			getActionBar().setTitle(client.getName());
		}

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
	}

	private void setupFields() 
	{
		if (route == null || client == null)
			return;
		String clientName = (client.getName() != null) ? client.getName() : "";
		String address = (client.getAddress1() != null) ? client.getAddress1() : "";
		String date =  (route.getCreatedDate() != null) ? Utils.formatDate( route.getCreatedDate())  : "";

		((TextView) findViewById(R.id.idClient)).setText(clientName.toLowerCase());
		((TextView) findViewById(R.id.nameClient)).setText(address.toLowerCase());
		((TextView) findViewById(R.id.created_at)).setText( date );
		((TextView) findViewById(R.id.created_by)).setText( ( route.getCreatedBy() != null) ? route.getCreatedBy() : "");
	}

	private void configureMap() 
	{
		if (route == null)
			return;

		Double location[] = extractRouteLocation();

		GoogleMap map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

		Boolean createNewClient = getIntent().getBooleanExtra("createNewClient", false);

		configuracion = Configuration.getInstance();
		configuracion.configureSharePreference(getBaseContext());

		if (createNewClient) 
		{
			dialogRegisterNewClient(activity).show();
			location[0] = null;
		}

		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) 
		{
			showGPSDisabledAlertToUser();
		} 
		else 
		{
			try 
			{
				if (location.length == 2 && location[0] == null) {
					LocationUtility locationUtility = LocationUtility
							.getInstance(activity);
					Location loc = null;

					if (locationUtility.verifyGooglePlayStatus() && map != null) {
						loc = locationUtility.getUserLocation();
						location = new Double[2];
						if(loc != null)
						{
							location[0] = loc.getLatitude();
							location[1] = loc.getLongitude();
						}
					}
				}
				if(location[0] != null)
				{
					position = new LatLng(location[0], location[1]);

					Marker marker = map.addMarker(new MarkerOptions()
							.position(position));
					marker.setIcon(BitmapDescriptorFactory
							.fromResource(R.drawable.ic_pointer));
					marker.setTitle(client.getName());
					client.setLocation(position.latitude + ", " + position.longitude);

					map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
					map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);	
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private void showGPSDisabledAlertToUser() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder
				.setMessage("GPS esta desactivado. Click para activarlo")
				.setCancelable(false)
				.setPositiveButton("Activar GPS",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Intent callGPSSettingIntent = new Intent(
										android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								startActivity(callGPSSettingIntent);
							}
						});
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();
	}

	private Dialog dialogRegisterNewClient(final Context context) 
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("Registrar Nuevo Usuario");
		// alertDialogBuilder.setIcon( );

		LayoutInflater inflater = LayoutInflater.from(context);
		View rootView = inflater.inflate(R.layout.dialog_register_user, null);

		final EditText editTextContactName, editTextContactLastName, editTextAddress, editTextPhone, editTextInterlocutor, editTextRNC, editTextSociety;

		final ClientInfo clientInfo;

		editTextSociety = (EditText) rootView
				.findViewById(R.id.editTextSociety);
		editTextRNC = (EditText) rootView.findViewById(R.id.editTextRNC);
		editTextInterlocutor = (EditText) rootView
				.findViewById(R.id.editTextInterlocutor);
		editTextContactName = (EditText) rootView
				.findViewById(R.id.editTextContactName);
		editTextContactLastName = (EditText) rootView
				.findViewById(R.id.editTextContactLastName);
		editTextAddress = (EditText) rootView
				.findViewById(R.id.editTextAddress);
		editTextPhone = (EditText) rootView.findViewById(R.id.editTextPhone);

		alertDialogBuilder.setView(rootView);

		alertDialogBuilder.setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						ClientEntity clientEntity = new ClientEntity();
						clientEntity.setKunnr(client.getKunnr());
						clientEntity.setName(editTextSociety.getText()
								.toString());
						clientEntity.setRNC(editTextRNC.getText()
								.toString());
						clientEntity.setInterlocutor(editTextInterlocutor
								.getText().toString());
						clientEntity.setContactName(editTextContactName
								.getText().toString());
						clientEntity.setContactLastName(editTextContactLastName
								.getText().toString());
						clientEntity.setLocation(client.getLocation());
						clientEntity.setAddress1(editTextAddress.getText()
								.toString());
						clientEntity.setPhone(editTextPhone.getText()
								.toString());
						clientEntity.setStatus("CREADO");
						clientEntity.setAvailable("10000.00");
						clientEntity.setOpen("0.00");
						clientEntity.setCredit("10000.00");

						int idRegistered = ClientDataModel.saveClient(context,
								clientEntity);
						if (idRegistered != -1) {
							Toast.makeText(
									context,
									getResources().getString(
											R.string.registered_successfully),
									Toast.LENGTH_LONG).show();
						}
						dialog.cancel();
					}
				});

		return alertDialogBuilder.create();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.route, menu);
		return super.onCreateOptionsMenu(menu);
	}

	private void dispatchMenuActivity(Class<?> activity) {
		if (activity != null) {
			Intent intent = new Intent(this, activity);
			intent.putExtra(BillActivity.EXTRA_CLIENT_ENTITY, client);
			startActivity(intent);
		}
	}

	private void dispatchActivityByID(Class<?> activity) {
		if (activity != null) {
			Intent intent = new Intent(this, activity);
			intent.putExtra(BillActivity.EXTRA_CLIENT_ENTITY, client.getId());
			startActivity(intent);
		}
	}

	private Double[] extractRouteLocation() {
		Double params[] = new Double[2];
		if (route != null) {
			if (!TextUtils.isEmpty(client.getLocation())) {
				try {

					ArrayList<String> latLong = new ArrayList<String>();

					Pattern p = Pattern.compile("\\-?\\d*\\.?\\d+");
					Matcher m = p.matcher(client.getLocation());
					while (m.find()) {
						latLong.add(m.group());
					}
					if (!latLong.isEmpty()) {
						params[0] = Double.valueOf(latLong.get(0));
						params[1] = Double.valueOf(latLong.get(1));
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return params;
	}

	public void onCheckout(MenuItem item) {
		//dispatchActivityByID(ChargeActivity.class);
	}

	public void onBill(MenuItem item) {
		
		List<Archive> archives = ArchiveDataModel.getArchives(RouteActivity.this);
		boolean hasArchives = false;
		for (Archive archive : archives) {
			String id = (client.getStatus().equalsIgnoreCase("CREADO")) ? client
					.getId() : client.getKunnr();
			if (id.equalsIgnoreCase(archive.getOwner())) {
				hasArchives = true;
				break;
			}
		}

		Configuration.getInstance().configureSharePreference(RouteActivity.this);
		Configuration.getInstance().getPrefsEditor().putBoolean("show_assotiation", false);
		Configuration.getInstance().getPrefsEditor().commit();

		if (hasArchives) {
			showArchives(RouteActivity.this, archives.get(0), client).show();
			// dispatchMenuActivity( ArchiveList.class );
		} else {
			dispatchMenuActivity(BillActivity.class);
		}
	}

	private Dialog showArchives(Activity activity, Archive archive, final ClientEntity client) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				activity);

		alertDialogBuilder.setTitle(activity.getResources().getString(
				R.string.archive_list));
		alertDialogBuilder.setIcon(activity.getResources().getDrawable(
				R.drawable.ic_account));
		String message = "The client:  " + archive.getName() + " with id: ( "
				+ archive.getId() + " ) has the following archives.";
		// alertDialogBuilder.setMessage(message);

		LayoutInflater inflater = LayoutInflater.from(activity);
		View view = inflater.inflate(R.layout.archive_dialog, null);

		TextView empty = (TextView) view.findViewById(android.R.id.empty);
		TextView idClient = (TextView) view.findViewById(R.id.idClient);
		TextView nameClient = (TextView) view.findViewById(R.id.nameClient);

		String id = (client.getStatus().equalsIgnoreCase("CREADO")) ? client
				.getId() : client.getKunnr();
		idClient.setText(id);
		nameClient.setText(client.getName());
		empty.setVisibility(TextView.GONE);

		listArchive = (ListView) view.findViewById(R.id.listArchive);
		listArchive.setOnItemClickListener(this);
		ArchiveAdapter adapter = new ArchiveAdapter(activity, getArchivesOfClient(client), true, null);
		listArchive.setAdapter(adapter);
		listArchive.refreshDrawableState();

		alertDialogBuilder.setView(view);
		alertDialogBuilder.setPositiveButton(
				activity.getResources().getString(R.string.create_new),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dispatchActivity(BillActivity.class, -1, client, true);
						dialog.cancel();
					}
				});

		return alertDialogBuilder.create();
	}


	private void dispatchActivity(Class<?> className, int idArchive, ClientEntity client, boolean showArchive) 
	{
		if (className != null) {
			Intent intent = new Intent(activity, className);
			intent.putExtra(BillActivity.EXTRA_CLIENT_ENTITY, client);
			intent.putExtra(ID_ARCHIVE, idArchive);
			intent.putExtra(DONT_SHOW_ARCHIVED_ORDERS, showArchive);
			activity.startActivity(intent);
		}
	}

	private List<Archive> getArchivesOfClient(ClientEntity client) {
		List<Archive> archives = ArchiveDataModel.getArchives(this);
		List<Archive> clientArchives = new ArrayList<Archive>();

		if (client != null) {
			for (Archive archive : archives) {
				String id = (client.getStatus().equalsIgnoreCase("CREADO")) ? client
						.getId() : client.getKunnr();
				if (id.equalsIgnoreCase(archive.getOwner())) {
					clientArchives.add(archive);
				}
			}
			return clientArchives;

		}
		return archives;
	}

	public void onFinancial(MenuItem item) {
	}

	public void onCredits(MenuItem item) {
	}

	public void onDebits(MenuItem item) {
	}

	public void onClientProfile(View v) {

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
	{		
		switch (parent.getId()) 
		{
			case R.id.listArchive:
				
				Archive archive = (Archive) parent.getItemAtPosition(position);
	
				if (archive != null) 
				{
					SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
					ClientEntity client = ClientDataModel.getClientByExternalID(activity, archive.getOwner());
					if (client == null)
						client = ClientDataModel.getClient(activity, archive.getOwner());
					dispatchActivity(BillActivity.class, archive.getId(), client, false);
				}		
				
				break;
	
			default:
				break;
			}
	}
}

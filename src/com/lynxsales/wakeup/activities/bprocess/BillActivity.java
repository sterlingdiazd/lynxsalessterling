package com.lynxsales.wakeup.activities.bprocess;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jvra.signature.SignatureActivity;
import com.lynx.sales.bundle.app.LynxApplication;
import com.lynx.sales.bundle.dto.model.CompanyDataModel;
import com.lynx.sales.bundle.dto.model.ProductDataModel;
import com.lynx.sales.bundle.dto.model.ScheduleDataModel;
import com.lynx.sales.bundle.dto.model.UnitsDataModel;
import com.lynx.sales.bundle.dto.model.UserRetrieval;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob;
import com.lynx.sales.bundle.entities.CenterEntity;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.entities.ClientInfo;
import com.lynx.sales.bundle.entities.Company;
import com.lynx.sales.bundle.entities.DocTypeEntity;
import com.lynx.sales.bundle.entities.ProductEntity;
import com.lynx.sales.bundle.entities.UnitEntity;
import com.lynx.sales.bundle.entities.UserEntity;
import com.lynx.sales.bundle.http.request.ErrorStub;
import com.lynx.sales.bundle.http.request.PhaseHandler;
import com.lynx.sales.bundle.http.request.Requestor;
import com.lynx.sales.bundle.http.request.ResponseStub;
import com.lynx.sales.bundle.http.request.ScheduleLocalTaskRequest;
import com.lynx.sales.bundle.http.request.ServiceLocator;
import com.lynx.sales.bundle.http.request.ServiceName;
import com.lynx.sales.bundle.http.request.ServiceStub;
import com.lynx.sales.bundle.mn.connection.NetworkHelper;
import com.lynx.sales.bundle.msg.MessageFormatter;
import com.lynx.sales.bundle.msg.MessageFormatter.FilterHandler;
import com.lynx.sales.bundle.msg.MessageFormatter.Message;
import com.lynx.sales.bundle.msg.MessageFormatter.MessageHandler;
import com.lynx.sales.bundle.prefs.utils.PreferenceHelper;
import com.lynxsales.printer.Configuration;
import com.lynxsales.printer.LynxPrinter;
import com.lynxsales.util.Utility;
import com.lynxsales.wakeup.ProcessMemento;
import com.lynxsales.wakeup.activities.BaseActivity;
import com.lynxsales.wakeup.activities.SynchronizeActivity;
import com.lynxsales.wakeup.activities.adapter.CentersAdapter;
import com.lynxsales.wakeup.activities.adapter.DocTypeAdapter;
import com.lynxsales.wakeup.activities.adapter.ProductBillAdapter;
import com.lynxsales.wakeup.activities.adapter.ProductListAdapter.ProductEntry;
import com.lynxsales.wakeup.noapi.entities.ListWrapper;
import com.lynxsales.wakeup.noapi.request.DefaultRequestor;
import com.lynxsales.wakeup.noapi.request.Utils;
import com.lynxsales.wakeup.utils.AlertDialogManager;
import com.lynxsales.wakeup.R;

@SuppressLint("NewApi")
public class BillActivity extends BaseActivity implements OnItemClickListener, OnItemLongClickListener {

	public static final String EXTRA_CLIENT_ENTITY = "com.lynx.sales.activities.bprocess.BillActivity.EXTRA_CLIENT_ENTITY";
	private static final int FLIP_NO_CONTENT = 0;
	private static final int FLIP_CONTENT = 1;
	private static final int REQUEST_PRODUCT_LIST = 1 << 5;
	private static final int REQUEST_PRODUCT_DETAIL = 1 << 7;
	private static final int SENDER_NO_NAVIGATE = 1 << 1;
	private static final int SENDER_NAVIGATE = 1 << 2;
	private ClientEntity client;
	private List<ProductEntity> billProducts;
	private ActionMode actionMode;
	private ProductBillAdapter adapter;
	private ListView listProducts;
	private boolean signed;
	private Configuration configuracion;
	private boolean firstLoad;
	private boolean restored;
	private boolean guardLimit = true;
	private int indexSelected;
	private int idArchive;
	private SharedPreferences prefs;
	private CenterEntity center;
	public static final String CENTER = "center";
	private ListView listCenter;
	private ListView listDocType;
	private List<CenterEntity> centers;
	private List<DocTypeEntity> docTypes;
	private ProgressDialog progress;
	private DocTypeEntity docType;
	private UserEntity user;
	private View currentSelectedView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bill);

		init();
	}

	private void init() 
	{
		initComponents();
		getBussinessClient();
		idArchive = getIntent().getExtras().getInt(ArchiveList.ID_ARCHIVE);
		initClass(idArchive);
		initActionBar();
		setupComponentsValues();
		setupHeaderValues();
	}

	private Dialog showCenters(Activity activity, final ClientEntity client, List<CenterEntity> centers) 
	{
		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

		alertDialogBuilder.setTitle("Seleccionar Centro");
		// alertDialogBuilder.setIcon(activity.getResources().getDrawable(R.drawable.ic_account));
		String message = "Seleccione el centro";
		// alertDialogBuilder.setMessage(message);

		LayoutInflater inflater = LayoutInflater.from(activity);
		View view = inflater.inflate(R.layout.center_selection, null);

		TextView empty = (TextView) view.findViewById(android.R.id.empty);
		empty.setVisibility(TextView.GONE);

		listCenter = (ListView) view.findViewById(R.id.listCenter);
		listCenter.setOnItemClickListener(this);
		
		CentersAdapter adapter = new CentersAdapter(activity, centers);
		listCenter.setAdapter(adapter);
		listCenter.refreshDrawableState();

		alertDialogBuilder.setView(view);

		alertDialogBuilder.setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});

		final AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();

		Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
		b.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if (center == null) {
					alertDialog.setMessage("Debe selecionar un centro.");
					alertDialog.setCancelable(false);
				} else {
					alertDialog.cancel();
					
					/*if(billProducts.size() > 0)
					{*/
						if(docType.getDocType().equalsIgnoreCase(center.getDocType()))
						{
							Intent intent = new Intent(BillActivity.this, ProductListActivity.class);
							intent.putExtra(ProductListActivity.EXTRA_BUSSINESS_CLIENT, client);
							intent.putExtra(CENTER, center);
							startActivityForResult(intent, REQUEST_PRODUCT_LIST);
						} else {
							new AlertDialogManager().showAlertDialog(BillActivity.this, "Error de Seleccion", "No puede elegir diferentes tipos de pedidos.", false);
						}
					/*}*/
					
				}
			}
		});

		return alertDialog;
	}

	private final Dialog chooseDocType(Activity activity, final ClientEntity client, List<DocTypeEntity> docTypes, final boolean hide_bp04) 
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
		alertDialogBuilder.setTitle("Tipo de Documento");
		// alertDialogBuilder.setIcon(activity.getResources().getDrawable(R.drawable.ic_account));
		String message = "Seleccione el tipo";
		// alertDialogBuilder.setMessage(message);

		LayoutInflater inflater = LayoutInflater.from(activity);
		View view = inflater.inflate(R.layout.selection_doc_type, null);

		TextView empty = (TextView) view.findViewById(android.R.id.empty);
		empty.setVisibility(TextView.GONE);

		listDocType = (ListView) view.findViewById(R.id.listDocType);
		listDocType.setOnItemClickListener(this);

		DocTypeAdapter docAdapter = new DocTypeAdapter(activity, docTypes);
		listDocType.setAdapter(docAdapter);
		listDocType.refreshDrawableState();

		alertDialogBuilder.setView(view);

		alertDialogBuilder.setPositiveButton("Seleccionar", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

			}
		});

		final AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();

		Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
		b.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if (docType == null) {
					alertDialog.setMessage("Debe selecionar un tipo de documento.");
					alertDialog.setCancelable(false);
				} 
				else 
				{
					alertDialog.cancel();
					
					centers = new ArrayList<CenterEntity>();

					for (CenterEntity center : user.getCenters()) 
					{
							if ( center.getDocType().equalsIgnoreCase(docType.getDocType()))
							{
								if(center.getName().equalsIgnoreCase("BP04") && hide_bp04 == true )
								{
									continue;
								}
								centers.add(center);
							}
							
					}
					
					boolean key_center_filter_enabled = prefs.getBoolean("key_center_filter_enabled", false);
					
					if(centers.size() == 0 )
					{
						new AlertDialogManager().showAlertDialog(BillActivity.this, "No hay centros", "Para la clase de pedidos, no hay centros asignados", false);
					}
					else if (key_center_filter_enabled && centers.size() > 1) {
						showCenters(BillActivity.this, client, centers).show();
					} else {

						Intent intent = new Intent(BillActivity.this, ProductListActivity.class);
						intent.putExtra(ProductListActivity.EXTRA_BUSSINESS_CLIENT, client);
						
						center = centers.get(0);
						intent.putExtra(CENTER, center);
						startActivityForResult(intent, REQUEST_PRODUCT_LIST);
					}
					
				}
			}
		});

		return alertDialog;
	}

	private Dialog showPrintCopyMessage(Activity activity) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

		alertDialogBuilder.setTitle("Imprimir Copia	");
		// alertDialogBuilder.setIcon(activity.getResources().getDrawable(R.drawable.ic_account));
		String message = "Continuar con la impresi�n";
		alertDialogBuilder.setMessage(message);

		alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

				dialog.cancel();
				new AsyncTask<Void, Void, Void>() {

					@Override
					protected void onPreExecute() {

					};

					@Override
					protected Void doInBackground(Void... params) {
						boolean isPrintOrderEnabled = prefs.getBoolean("key_print_order_enabled", false);
						boolean isPriceEnabled = prefs.getBoolean("key_price_enabled", false);
						boolean isTheCopy = true;

						if (isPrintOrderEnabled) {
							printDocument(isPriceEnabled, isTheCopy);
						}
						return null;
					};

					@Override
					protected void onPostExecute(Void result) {
						super.onPostExecute(result);

						navigateUpTo(new Intent(BillActivity.this, ItineraryActivity.class));

					}
				}.execute();

			}
		});

		return alertDialogBuilder.create();
	}

	private void setupHeaderValues() {
		setupClientInfo();
	}

	private void setupClientInfo() {

		final String[] interlocutor;
		ClientInfo clientInfo = null;

		if (client != null) {
			interlocutor = client.getInterlocutor().split("-");
			client.setInterlocutor(interlocutor[0]);

			/*
			 * clientInfo = client.getClientInfo(); if (clientInfo == null) {
			 * clientInfo = ClientDataModel.getClientInfo(this, client.getId());
			 * if (clientInfo.getSocieties().length == 0) { clientInfo =
			 * ClientDataModel.getClientInfo(this, client.getKunnr()); }
			 * 
			 * if (client.getClientInfo().getSalesArea() != null) { if
			 * (client.getClientInfo().getSalesArea().get(0) != null) {
			 * //interlocutor =
			 * client.getClientInfo().getSalesArea().get(0).getInterlocutors();
			 * 
			 * if (interlocutor.length > 0) {
			 * client.setInterlocutor(interlocutor[0]); } } }
			 * 
			 * } client.setClientInfo(clientInfo);
			 */

			user.setDefaultCenter(center);

			docTypes = UserRetrieval.getUser(BillActivity.this).getDocTypes();

			// client.setDocType(user.getDocType());

			/*
			 * final String[] societies = client.getClientInfo().getSocieties();
			 * if(societies.length > 0) { client.setSociety(societies[0]); }
			 * 
			 * final List<String> sales =
			 * client.getClientInfo().getSalesResume(); if(sales.size() > 0) {
			 * client.setSalesOrg(sales.get(0)); }
			 * 
			 * final List<String> channels =
			 * client.getClientInfo().getChannelAsResume(); if(channels.size() >
			 * 0) { client.setChannel(channels.get(0)); }
			 * 
			 * 
			 * client.setSector("01");
			 */
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.bill, menu);
		return super.onCreateOptionsMenu(menu);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			saveOrUpdate();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> adapter, View view, int position, long id) {
		onListItemSelect(position);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) 
	{
		if (currentSelectedView != null && currentSelectedView != view) {
			unhighlightCurrentRow(currentSelectedView);
		}
		currentSelectedView = view;
		highlightCurrentRow(currentSelectedView);

		if ((listCenter != null)) {
			if (adapter.getId() == listCenter.getId()) {
				center = (CenterEntity) adapter.getItemAtPosition(position);
			}

		}
		if (listDocType != null) 
		{
			if (adapter.getId() == listDocType.getId()) 
			{
				if (view.isSelected())
					view.setBackgroundColor(getResources().getColor(R.color.light_blue));
				docType = (DocTypeEntity) adapter.getItemAtPosition(position);
				if(docTypes != null)
				{
					docTypes.clear();
				} else {
					docTypes = new ArrayList<DocTypeEntity>();
				}
				docTypes.add(docType);
				listDocType.refreshDrawableState();
				
			}

		} else {
			if (null == actionMode) {
				indexSelected = position;
				if (billProducts.size() > 0) {
					ProductEntity selected = billProducts.get(position);
					Intent detail = new Intent(BillActivity.this, ProductDetailActivity.class);
					detail.putExtra(ProductDetailActivity.EXTRA_SELECTED_PRODUCT, selected);
					detail.putExtra(ProductDetailActivity.EXTRA_PRODUCT_AMOUNT_EDITABLE, true);
					startActivityForResult(detail, REQUEST_PRODUCT_DETAIL);
				}

			} else {
				onListItemSelect(position);
			}
		}

	}

	private void unhighlightCurrentRow(View rowView) 
	{	
		if(rowView != null)
			rowView.setBackgroundColor(Color.TRANSPARENT);
		// TextView textView = (TextView) rowView.findViewById(R.id.menuTitle);
		// textView.setTextColor(getResources().getColor(R.color.white));
	}

	private void highlightCurrentRow(View rowView) 
	{
		if(rowView != null)
			rowView.setBackgroundColor(getResources().getColor(R.color.light_blue));
		// TextView textView = (TextView) rowView.findViewById(R.id.menuTitle);
		// textView.setTextColor(getResources().getColor(R.color.yellow));

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		saveOrUpdate();
	}

	public void saveOrUpdate() {
		if (restored) {
			updateMemento(idArchive);
		} else {
			saveMemento();
		}

	}

	private void updateMemento(int idArchive) {
		if (null == billProducts || billProducts.isEmpty()) {
			deleteMemento(idArchive);
			return;
		}

		ContentValues values = new ContentValues();
		String articles = getArticlesQueryString(billProducts);

		// values.put("archive_type", "order");
		// values.put("owner", client.getId());
		values.put("state", (articles != null) ? articles : "");
		values.put("date", new Utility().getDateTime());
		// values.put("owner_name", client.getName());

		ProcessMemento.update(this, values, idArchive);

		Toast.makeText(this, getResources().getString(R.string.order_archived), Toast.LENGTH_LONG).show();

	}

	private void saveMemento() {
		if (null == billProducts || billProducts.isEmpty())
			return;

		ContentValues values = new ContentValues();
		String articles = getArticlesQueryString(billProducts);

		values.put("archive_type", "Order");
		values.put("owner", (client.getStatus().equalsIgnoreCase("CREADO")) ? client.getId() : client.getKunnr());
		values.put("state", (articles != null) ? articles : "");
		values.put("date", new Utility().getDateTime());
		values.put("owner_name", client.getName());

		ProcessMemento.save(this, values);

		Toast.makeText(this, getResources().getString(R.string.order_archived), Toast.LENGTH_LONG).show();
	}

	private void deleteMemento(int idArchive) {
		ProcessMemento.delete(this, idArchive);
		restored = false;
	}
	
	private boolean restoreFromMemento(int idArchive) 
	{
		
		String state = ProcessMemento.restore(this, idArchive);

		if (null != state) {
			String[] products = state.split("\\;");
			for (String keyPair : products) {
				if (!TextUtils.isEmpty(keyPair)) {
					String[] keyValue = keyPair.split("\\:");

					String productMaterial = null;
					String productCenter = null;
					String productStore = null;
					String productAmount = null;
					String productUnit = null;
					String productPrice = null;
					String productOffer = null;
					String productDiscount = null;
					String productAditionalMaterial = null;

					try {
						productMaterial = keyValue[0];
						productCenter = keyValue[1];
						productStore = keyValue[2];
						productAmount = keyValue[3];
						productUnit = keyValue[4];
						productPrice = keyValue[5];
						productAditionalMaterial = keyValue[6];
						
						if(keyValue[7] != null)
						{
							if (!keyValue[7].equalsIgnoreCase("null") ) 
							{
								productOffer = (keyValue[7] + ":" + keyValue[8] + ":" + keyValue[9]).trim();
								productDiscount = (keyValue[10] != null && !keyValue[10].equalsIgnoreCase("null")) ? keyValue[10] : null;
							} else {
								productOffer = null;
								productDiscount = (keyValue[8] != null && !keyValue[8].equalsIgnoreCase("null")) ? keyValue[8] : null;
							}	
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
					CenterEntity centerArchive = new CenterEntity();
					centerArchive.setStore(productStore);
					centerArchive.setName(productCenter);
					ProductEntity product = ProductDataModel.getProductsByCenterAndMaterial(this, centerArchive, productMaterial);
					
					product.setAmount(productAmount);
					product.setPrice(productPrice);
					product.setDiscount(productDiscount);
					product.setOffer(productOffer);
					
					if(productAditionalMaterial != null)
					{
						product.setKnrmat(productAditionalMaterial);
					}
						
					
					UnitEntity unit = new UnitEntity();
					unit.setMatnr(productMaterial);
					unit.setMeinh(productUnit);
					product.setSelectedSalesUnit(unit);
					
					List<CenterEntity> centers = user.getCenters();
					for (CenterEntity center : centers) 
					{
						if (productCenter.equalsIgnoreCase(center.getName())) 
						{
							List<DocTypeEntity> docs = UserRetrieval.getUser(getBaseContext()).getDocTypes();
							for (DocTypeEntity doc : docs) 
							{
								if (doc.getDocType().equalsIgnoreCase(center.getDocType())) 
								{
									docType = doc;
									if(docTypes != null)
									{
										docTypes.clear();
									} else {
										docTypes = new ArrayList<DocTypeEntity>();
									}
									docTypes.add(docType);
									//listDocType.refreshDrawableState();
									break;
								}
							}
						}

					}

					billProducts.add(product);
				}
			}
		}
		return !billProducts.isEmpty();
		
	}

	private void initClass(int idArchive) {
		billProducts = new ArrayList<ProductEntity>();

		boolean dont_show_archived = getIntent().getExtras().getBoolean(ArchiveList.DONT_SHOW_ARCHIVED_ORDERS);

		if (dont_show_archived == true) {

		} else {
			
			if ((restored = restoreFromMemento(idArchive))) {
				Toast.makeText(this, getResources().getString(R.string.restoring_archived), Toast.LENGTH_LONG).show();
				calculateTotal(billProducts);
			}
			
		}

		adapter = new ProductBillAdapter(this, R.layout.entry_product, billProducts);

		if (listProducts != null) {
			listProducts.setAdapter(adapter);
			listProducts.setOnItemClickListener(this);
			listProducts.setOnItemLongClickListener(this);
		}

		if (!billProducts.isEmpty()) {
			fadeFlipView(R.id.flipper_view, FLIP_CONTENT);
		}
	}

	private void initActionBar() {
		if (client != null)
			getActionBar().setTitle(getResources().getString(R.string.order));

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
	}

	private void initComponents() {
		configuracion = Configuration.getInstance();
		configuracion.configureSharePreference(getBaseContext());
		listProducts = (ListView) findViewById(R.id.bill_list);
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		LynxApplication app = (LynxApplication) getApplication();
		user = app.getUser();
	}

	private void getBussinessClient() {
		if (getIntent().getExtras() != null) {
			client = (ClientEntity) getIntent().getExtras().get(EXTRA_CLIENT_ENTITY);
		}
	}

	private void setupComponentsValues() {
		if (client == null)
			return;

		((TextView) findViewById(R.id.available_credit)).setText(client.getAvailable());
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.e("######", "" + billProducts.size());
		if (requestCode == REQUEST_PRODUCT_LIST)

			productListBack(resultCode, data);
		else if (requestCode == REQUEST_PRODUCT_DETAIL)
			productEditionBack(resultCode, data);

		Boolean isClientCreationEnabled = prefs.getBoolean("key_client_creation", false);

		Configuration.getInstance().configureSharePreference(this);
		boolean clientAttached = Configuration.getInstance().getPrefs().getBoolean("show_assotiation", false);

		if (isClientCreationEnabled && clientAttached == false) {
			Configuration.getInstance().configureSharePreference(this);
			Configuration.getInstance().getPrefsEditor().putBoolean("show_assotiation", true);
			Configuration.getInstance().getPrefsEditor().commit();
			new AlertDialogManager().dialogAttachOrderToClient(this, "Asociar Cliente",
					"Se han creado nuevos clientes recientemente, �Desea asociar la orden a un nuevo cliente");
		}
	}

	public void changeClient() {
		boolean isSetAttachOrderToClient = configuracion.getPrefs().getBoolean("AttachOrderToClient", false);
		if (isSetAttachOrderToClient) {
			new AlertDialogManager().clientListDialog(this);
		}
	}

	public void changeClientID() {
		client.setId(configuracion.getPrefs().getString("client_id", "-1"));
		client.setStatus("CREADO");
	}

	private void productListBack(int resultCode, Intent data) {
		if (data != null) {

			@SuppressWarnings("unchecked")
			ListWrapper<List<ProductEntity>> wrapper = (ListWrapper<List<ProductEntity>>) data.getExtras()
					.getSerializable(ProductListActivity.EXTRA_PRODUCTS_SELECTED);

			@SuppressWarnings("unchecked")
			ListWrapper<List<ProductEntry>> entries = (ListWrapper<List<ProductEntry>>) data.getExtras()
					.getSerializable(ProductListActivity.EXTRA_ENTRIES_SELECTED);

			// Log.e("**********", "******pre to bill " +
			// wrapper.getList().size());
			if (wrapper != null && wrapper.getList() != null) {
				addProductsToBill(wrapper.getList(), entries.getList());
			}
		}
	}

	private void productEditionBack(int resultCode, Intent data) {
		if (data != null && data.getExtras() != null) {
			ProductEntity backed = (ProductEntity) data
					.getSerializableExtra(ProductDetailActivity.EXTRA_BACKED_PRODUCT);
			updateProduct(indexSelected, backed);
		}
	}

	private void updateProduct(int index, ProductEntity backed) 
	{
		ProductEntity selected = billProducts.get(index);
		selected.setAmount(backed.getAmount());
		selected.setCenter(backed.getCenter());
		selected.setSelectedSalesUnit(backed.getSelectedSalesUnit());
		adapter.notifyDataSetChanged();
		calculateTotal(billProducts);
	}

	private void addProductsToBill(List<ProductEntity> products, List<ProductEntry> entries) {

		if (products == null || products.isEmpty())
			return;

		final List<ProductEntity> temps = new ArrayList<ProductEntity>();

		for (int i = 0; i < entries.size(); ++i) {
			ProductEntity product = products.get(i);

			product.setAmount(entries.get(i).amount); // Poner los precios y bonos

			temps.add(product);
		}
		setBillAdapter(temps);

		/**
		 * Exclusion temporary disabled
		 */
		// excludeSelectionFromModel(model);
	}

	private void setBillAdapter(List<ProductEntity> products) 
	{
		if (products != null && !products.isEmpty()) 
		{

			for (ProductEntity product : products) 
			{
				if (billProducts.contains(product)) 
				{
					int index = billProducts.indexOf(product);
					ProductEntity updated = billProducts.get(index);
					Double amount = Double.valueOf(updated.getAmount()) + Double.valueOf("0" + product.getAmount());
					updated.setAmount(String.valueOf(amount));
					updated.setSelectedSalesUnit(product.getSelectedSalesUnit());
				} else
					billProducts.add(product);
			}

			// billProducts.addAll(products);

			adapter.notifyDataSetChanged();

			if (!firstLoad && !billProducts.isEmpty()) {
				fadeFlipView(R.id.flipper_view, FLIP_CONTENT);
				firstLoad = true;
			}

			calculateTotal(billProducts);
		}
	}

	//
	// private void excludeSelectionFromModel( ProductDataModel model ){
	// if( model.getProductsCache() == null ||
	// model.getProductsCache().isEmpty() )
	// return;
	// model.getProductsCache().removeAll(billProducts);
	// }
	//

	private void calculateTotal(List<ProductEntity> added) {
		double available = 0;

		NumberFormat nFormat = NumberFormat.getInstance(Locale.US);
		try {
			if (client.getAvailable() != null) {
				available = nFormat.parse(client.getAvailable()).doubleValue();
			} else {
				new AlertDialogManager().showAlertDialog(this, "Sin monto disponible",
						"No tiene monto disponible y no se le puede generar pedidos", false);
				return;
			}

		} catch (ParseException ex) {
			ex.printStackTrace();
		}

		double total = 0;
		for (ProductEntity product : added) {
			double price = 0;
			int quantity = 0;
			double discount = 0;

			try {
				Log.e("************", "Product Name: " + product.getDescription() + " : " + product.getPrice() + " : "
						+ product.getAmount());

				price = (product.getPrice() != null) ? nFormat.parse(product.getPrice()).doubleValue() : price;
				quantity = (product.getAmount() != null) ? nFormat.parse(product.getAmount()).intValue() : quantity;

				String stringDiscount = product.getDiscount();
				if (stringDiscount != null) {
					int start = stringDiscount.indexOf("-") + 1;
					int end = stringDiscount.length();

					String doubleValue = null;
					try {
						doubleValue = stringDiscount.substring(start, end);
						discount = (Double.parseDouble(doubleValue) / 10);
						price = price - (price * (discount / 100));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				

				total += price * (double) quantity;

			} catch (Exception ex) {
				ex.printStackTrace();
				total = 0;
			}

			if (available >= total) {
				guardLimit = true;
			} else {
				createDialog("Constraint", "The total amount is greater than available mount", SENDER_NO_NAVIGATE);
				guardLimit = false;
			}
			DecimalFormat format = new DecimalFormat("$ ###,###,###.00");
			((TextView) findViewById(R.id.total_amount)).setText(format.format(total));
		}
	}

	public void createDialog(String title, Object req, final int sender) {
		Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(req.toString());
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				if (sender != SENDER_NO_NAVIGATE)
					navigateUpTo(new Intent(BillActivity.this, ItineraryActivity.class));
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	public void onProcess(MenuItem item) {

		if (billProducts == null || billProducts.isEmpty()) {
			Toast.makeText(this, getResources().getString(R.string.must_select_product), Toast.LENGTH_LONG).show();
			return;
		}

		boolean isCreditLimitEnabled = prefs.getBoolean("credit_limit_config", false);

		if (isCreditLimitEnabled) {
			if (guardLimit == false) {
				createDialog("Constraint", "The total amount is greater than available mount", SENDER_NO_NAVIGATE);
				return;
			}
		}

		boolean isSignEnabled = prefs.getBoolean("sign_config", false);

		if (!signed && isSignEnabled) {
			Toast.makeText(this, R.string.must_sign, Toast.LENGTH_LONG).show();
			return;
		}

		boolean isHeaderEnabled = prefs.getBoolean("header_config", false);

		if (isHeaderEnabled) {
			/*
			 * if (TextUtils.isEmpty(client.getDocType())) { //Arreglar con la
			 * configuracion createDialog("Server Response",
			 * "It's neccesary edit header, before process",
			 * SENDER_NO_NAVIGATE); return; }
			 */
		}

		/**
		 * Clear archive si existe para el cliente especifico.
		 */
		if (idArchive != -1)
			ProcessMemento.delete(this, idArchive);

			boolean modeOnline = prefs.getBoolean("key_sync_work_mode", false);
			if (modeOnline && NetworkHelper.isDataOperational(BillActivity.this)) {
				// submitBill(guardLimit);
				scheduleBill(guardLimit, modeOnline);
			} else {
				scheduleBill(guardLimit, modeOnline);
			}
		//}

		/*
		 * new AsyncTask<Void, Void, Void>() {
		 * 
		 * @Override protected void onPreExecute() {
		 * 
		 * };
		 * 
		 * @Override protected Void doInBackground(Void... params) {
		 * 
		 * return null; };
		 * 
		 * @Override protected void onPostExecute(Void result) {
		 * super.onPostExecute(result);
		 * 
		 * }
		 * 
		 * }.execute();
		 */

	}

	private void submitBill(boolean guardLimit) {
		final ProgressDialog progress = new ProgressDialog(BillActivity.this);
		progress.setMessage(getResources().getString(R.string.saving));
		progress.setTitle("Progress");

		Requestor req = new DefaultRequestor(this, new PhaseHandler() {

			@Override
			public void onError(ErrorStub stub) {
				Log.e("*******", "Error");
				createDialog("Server Response", stub, SENDER_NO_NAVIGATE);
			}

			@Override
			public void onFeedback(String feed) {
			}

			@Override
			public void onCompleted(final ResponseStub response) {
				progress.dismiss();

				final MessageFormatter msgFormatter = MessageFormatter.getInstance(BillActivity.this);

				if (response.toString().contains("type")) {

					Message msg = msgFormatter.format(response.toString(), new FilterHandler() {
						@Override
						public boolean accept(String message, String type) {
							return "002".equals(message) || "001".equals(message);
						}

						@Override
						public MessageHandler getHandler() {
							return new MessageHandler() {
								@Override
								public Message getMessage(Map<String, String> keyValues) {
									if ("S".equals(keyValues.get("type"))) {
										String doc = String.format(getString(R.string.S_001), keyValues.get("doc"));

										return new Message(keyValues.get("message"), keyValues.get("type"), doc);
									} else {
										return new Message(keyValues.get("message"), keyValues.get("type"), keyValues
												.get("response"));
									}
								}
							};
						}
					});

					createDialog("Server Response", msg.toString(), SENDER_NAVIGATE);
				}
			}

			@Override
			public void onBegan() {
				progress.show();
			}
		});

		ServiceStub service = ServiceLocator.lookup(ServiceName.ORDER_CREATION);
		String[] params = getRequestParams();
		req.request(service, params);
	}

	private void scheduleBill(boolean guardLimit, final boolean modeOnline) {
		progress = new ProgressDialog(BillActivity.this);
		progress.setMessage(getResources().getString(R.string.savint_to_send));
		progress.setTitle("Progress");

		int jobID = 0;
		LynxApplication app = (LynxApplication) getApplication();
		ScheduleLocalTaskRequest request = new ScheduleLocalTaskRequest(this, app.getUser().getName(),
				ScheduleJob.Type.BILL, String.valueOf(guardLimit), String.valueOf(modeOnline), new PhaseHandler() {
					@Override
					public void onFeedback(String feed) {
					}

					@Override
					public void onError(ErrorStub stub) {
						Log.e("*******", "Error");
						createDialog("Scheduling Error", stub, SENDER_NO_NAVIGATE);
					}

					@Override
					public void onCompleted(ResponseStub response) {
						progress.dismiss();
						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.scheduled_successfully), Toast.LENGTH_LONG).show();

						new AsyncTask<Void, Void, Void>() {

							@Override
							protected void onPreExecute() {

							};

							@Override
							protected Void doInBackground(Void... params) {
								boolean isPrintOrderEnabled = prefs.getBoolean("key_print_order_enabled", false);
								boolean isPriceEnabled = prefs.getBoolean("key_price_enabled", false);
								boolean isTheCopy = false;

								if (isPrintOrderEnabled) {
									printDocument(isPriceEnabled, isTheCopy);
								}

								return null;
							};

							@Override
							protected void onPostExecute(Void result) {
								super.onPostExecute(result);
								if (modeOnline) {
									sync();
								} else {

									boolean isCopiesEnabled = prefs.getBoolean("key_copies_enabled", false);

									if (isCopiesEnabled) {
										showPrintCopyMessage(BillActivity.this).show();
									} else {
										navigateUpTo(new Intent(BillActivity.this, ItineraryActivity.class));
									}
								}
							}
						}.execute();
					}

					@Override
					public void onBegan() {
						if (progress != null)
							progress.show();
					}
				});

		ServiceStub service = ServiceLocator.lookup(ServiceName.ORDER_CREATION);

		String[] params = getRequestParams();
		request.request(service, params);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (progress != null) {
			progress.dismiss();
		}
	}

	private void sync() {
		String jobID = PreferenceHelper.getString(BillActivity.this, "preferences", "jobID");

		List<ScheduleJob> jobs = ScheduleDataModel.getShedulesJobs(this);
		for (ScheduleJob job : jobs) {
			if (job.getId() == Integer.valueOf(jobID)) {
				Intent intent = new Intent(this, SynchronizeActivity.class);
				Bundle extras = new Bundle();
				extras.putSerializable("jobs", (Serializable) job);
				intent.putExtras(extras);
				startActivity(intent);
			}
		}

	}

	private void printDocument(boolean isPriceEnabled, boolean isTheCopy) {
		LynxApplication app = (LynxApplication) getApplication();
		UserEntity seller = app.getUser();
		LynxPrinter printer = new LynxPrinter(this, isPriceEnabled);

		ArrayList<ArrayList<String>> allProducts = new ArrayList<ArrayList<String>>();

		for (int x = 0; x < billProducts.size(); x++) {
			ProductEntity product = billProducts.get(x);
			ArrayList<String> singleProduct = new ArrayList<String>();
			singleProduct.add(product.getId());
			singleProduct.add(product.getDescription());
			singleProduct.add(product.getPrice());
			singleProduct.add(product.getAmount() + " " + product.getSalesUnits());
			singleProduct.add(product.getAmount());
			singleProduct.add(product.getCenter());
			allProducts.add(singleProduct);
		}

		Company company = CompanyDataModel.getCompany(this);
		boolean isForSanut = false;

		if (!company.getName().equalsIgnoreCase("Mejia Alcala")) {
			isForSanut = true;
		}

		String address = "";
		if (company != null)
			address = company.getAddress();
		String companyAddress = "";
		if (address != "") {
			companyAddress = (address.length() < 40) ? address : address.substring(0, 40) + "...";
		}
		/*
		 * ARREGLAR CON LA CONFIGURACION printer.printBill(isForSanut,
		 * isTheCopy, company.getName(), client.getDocType(),
		 * client.getSalesOrg(), client.getChannel(), client.getSector(),
		 * client.getInterloctor(), seller.getId(), seller.getName(),
		 * seller.getCel(), companyAddress, company.getRnc(),
		 * company.getPhone(), allProducts);
		 */
	}

	private String[] getRequestParams() {
		LynxApplication app = (LynxApplication) getApplication();
		String code = Utils.getUserHashCode(this, app.getUser().getName());

		String articles = getArticlesQueryString(billProducts);

		return new String[] { "1",
				(client.getStatus().equalsIgnoreCase("CREADO")) ? client.getId() : client.getKunnr(),
				(docType.getDocType() != null) ? docType.getDocType() : "",
				(client.getOrganizacion() != null) ? client.getOrganizacion() : "",
				(client.getCanal() != null) ? client.getCanal() : "",
				(client.getSector() != null) ? client.getSector() : "",
				(client.getInterlocutor() != null) ? client.getInterlocutor() : "", 
				(articles != null) ? articles : "",
				(code != null) ? code : "", 
				(client.getSector() != null) ? client.getSector() : "",
				(app.getUser().getId() != null) ? app.getUser().getId() : "" };
	}

	public void onAddProduct(MenuItem item) {
		
		boolean hide_bp04 = false;
		
		if(docTypes != null)
		{
			
			if (docTypes.size() > 1) 
			{
				hide_bp04 = prefs.getBoolean("key_hide_bp04", false);				
				chooseDocType(BillActivity.this, client, docTypes, hide_bp04).show();
			} 
			else 
			{
				docType = docTypes.get(0);
				
				centers = new ArrayList<CenterEntity>();
				
				List<CenterEntity> centros = user.getCenters();
				if(centros != null)
				{
					for (CenterEntity center : user.getCenters()) 
					{
						if ( center.getDocType().equalsIgnoreCase(docType.getDocType()))
						{
							if(center.getName().equalsIgnoreCase("BP04") && hide_bp04 == true )
							{
								continue;
							}
							centers.add(center);
						}
							
					}
				}
				
				boolean key_center_filter_enabled = prefs.getBoolean("key_center_filter_enabled", false);
				
				if (key_center_filter_enabled && centers.size() > 1) {
					showCenters(BillActivity.this, client, centers).show();
				} else {

					Intent intent = new Intent(BillActivity.this, ProductListActivity.class);
					intent.putExtra(ProductListActivity.EXTRA_BUSSINESS_CLIENT, client);
					center = centers.get(0);
					intent.putExtra(CENTER, center);
					startActivityForResult(intent, REQUEST_PRODUCT_LIST);
				}
			}
		} 
		else {
			new AlertDialogManager().dialogAttachOrderToClient(this, "Canasta Vacia", "No hay tipo de documento seleccionado");
		}
		
	}

	/*
	 * private List<String> getCenter(ClientEntity client) { ProductEntity
	 * product = ProductDataModel.getClientProduct(this, client.getExternalID(),
	 * "000000000000001005"); return product.getCenters(); }
	 */
	public void onSign(MenuItem item) {
		if (billProducts == null || billProducts.isEmpty())
			return;
		signed = true;
		startActivityForResult(new Intent(this, SignatureActivity.class), 0);
	}

	public void onArchive(MenuItem item) {
		saveOrUpdate();
	}

	public void onDelete(MenuItem item) {
		createDialog("Warning", getResources().getString(R.string.delete_archive_message), SENDER_NO_NAVIGATE);

		deleteMemento(idArchive);
		billProducts.clear();
		fadeFlipView(R.id.flipper_view, FLIP_NO_CONTENT);
		firstLoad = false;
		((TextView) findViewById(R.id.total_amount)).setText("0.0");
	}

	public void onCancel(MenuItem item) {
		finish();
	}

	public void onHeaderEdit(MenuItem item) {
		/*
		 * LynxApplication app = (LynxApplication) getApplication(); Bundle args
		 * = new Bundle();
		 * 
		 * setupClientInfo();
		 * args.putSerializable(HeaderDialogFragment.EXTRA_BUSSINESS_CLIENT,
		 * client); args.putSerializable(HeaderDialogFragment.EXTRA_SELLER,
		 * app.getUser());
		 * 
		 * HeaderDialogFragment header = new HeaderDialogFragment();
		 * header.setRetainInstance(true); header.setCancelable(false);
		 * header.setArguments(args);
		 * 
		 * header.show(getFragmentManager(), null);
		 * 
		 * header.setActionListener(new ActionListener() {
		 * 
		 * @Override public void onCancel(HeaderDialogFragment header) { }
		 * 
		 * @Override public void onAccept(HeaderDialogFragment header) { } });
		 */
	}

	/**
	 * 
	 * @param products
	 * @return The query string representing all articles to create this order.
	 *         The format for the query is the following. [Article ID]:[ Article
	 *         Center]:[ Article Amount ]
	 */
	private String getArticlesQueryString(List<ProductEntity> products) 
	{
		StringBuilder builder = new StringBuilder();
		LynxApplication app = (LynxApplication) getApplication();
		CenterEntity center = new CenterEntity();
		center.setName("BP01");
		center.setStore("0101");
		app.getUser().setDefaultCenter(center);

		for (ProductEntity product : products) 
		{
			String amount = TextUtils.isEmpty(product.getAmount()) ? "1" : product.getAmount();
			boolean hasOffer = false;
			String offer = product.getOffer();
			if (offer == null || offer == "null")
			{
				hasOffer = false;
				offer = "";
			} 
			
			String store = product.getLgort().trim();

			builder
			.append(product.getMaterial()).append(":")
			.append(product.getCenter()).append(":")
			.append(store).append(":")
			.append(amount).append(":")
			.append(product.getSelectedSalesUnit().getMeinh()).append(":")
			.append(product.getPrice()).append(":")
			.append(product.getKnrmat()).append(":")
			.append(offer.trim().replaceAll(" ", "")).append(";")
			
			/*.append(product.getDiscount()).append(":")
			.append((hasOffer + "").toUpperCase()).append(";")*/;

		}
		builder.delete(builder.length() - 1, builder.length());
		return builder.toString();
	}

	private void onListItemSelect(int position) {

		adapter.toggleSelection(position);
		boolean hasCheckedItems = adapter.getSelectedCount() > 0;

		if (hasCheckedItems && actionMode == null)
			// there are some selected items, start the actionMode
			actionMode = startActionMode(new ActionModeCallback());
		else if (!hasCheckedItems && actionMode != null)
			// there no selected items, finish the actionMode
			actionMode.finish();

		if (actionMode != null)
			actionMode.setTitle(String.valueOf(adapter.getSelectedCount()) + " selected");
	}

	private class ActionModeCallback implements ActionMode.Callback {
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.getMenuInflater().inflate(R.menu.bill_action_mode, menu);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			if (item.getItemId() != R.id.delete)
				return false;

			SparseBooleanArray selected = adapter.getSelectedIds();
			for (int i = (selected.size() - 1); i >= 0; i--) {
				if (selected.get(selected.keyAt(i))) {
					ProductEntity selectedItem = billProducts.get(selected.keyAt(i));
					billProducts.remove(selectedItem);

					adapter.notifyDataSetChanged();
				}
			}
			mode.finish();
			calculateTotal(billProducts);
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			adapter.removeSelection();
			actionMode = null;
		}

	}

}

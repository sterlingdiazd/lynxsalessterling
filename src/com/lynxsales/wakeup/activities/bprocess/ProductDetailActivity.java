package com.lynxsales.wakeup.activities.bprocess;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.lynx.sales.bundle.dto.model.UnitsDataModel;
import com.lynx.sales.bundle.entities.ProductEntity;
import com.lynx.sales.bundle.entities.UnitEntity;
import com.lynx.sales.bundle.http.request.ServiceLocator;
import com.lynx.sales.bundle.http.request.ServiceName;
import com.lynx.sales.bundle.http.request.ServiceStub;
import com.lynxsales.wakeup.activities.BaseActivity;
import com.lynxsales.wakeup.foreing.cache.ImageDownloader;
import com.lynxsales.wakeup.foreing.cache.ImageUtil;
import com.lynxsales.wakeup.R;
import com.testflightapp.lib.TestFlight;

public class ProductDetailActivity extends BaseActivity{
	
	public static final String EXTRA_SELECTED_PRODUCT			= "com.lynx.sales.activities.bprocess.ProductDetailActivity.EXTRA_SELECTED_PRODUCT";
	public static final String EXTRA_BACKED_PRODUCT				= "com.lynx.sales.activities.bprocess.ProductDetailActivity.EXTRA_BACKED_PRODUCT";
	public static final String EXTRA_PRODUCT_AMOUNT_EDITABLE	= "com.lynx.sales.activities.bprocess.ProductDetailActivity.EXTRA_PRODUCT_AMOUNT_EDITABLE";
	
	private boolean amountEditable;
	private ProductEntity product;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView( R.layout.activity_product_detail );
		
		init();
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate( R.menu.product_detail, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	
	private void init(){
		initActionBar();		
		extractProduct();
		setFields();
	}	
	
	private void initActionBar(){	
		getActionBar().setTitle( getResources().getString(R.string.product_detail) );
		getActionBar().setDisplayHomeAsUpEnabled( true );
		getActionBar().setHomeButtonEnabled( true );
	}
	
	
	private void extractProduct(){
		if( getIntent().getExtras() != null ){
			amountEditable  = getIntent().getExtras().getBoolean( EXTRA_PRODUCT_AMOUNT_EDITABLE );
			product = ( ProductEntity ) getIntent().getExtras().getSerializable( EXTRA_SELECTED_PRODUCT );
		}
	}
	
	
	private void setFields(){
		if( product == null )
			return;
		
		/* DESPUES DE TENER LOS CAMPOS CALCULADOS DE CLIENTE Y PRODUCTO 
			
		(( TextView ) findViewById( R.id.idClient  ) )		.setText( product.getName() );
		(( TextView ) findViewById( R.id.stock ) )			.setText( "Stock: "+product.getStock() );
		(( TextView ) findViewById( R.id.organization ) )	.setText( product.getOrganization() );
		(( TextView ) findViewById( R.id.channel ) )		.setText( product.getDistributionChannel() );
		(( TextView ) findViewById( R.id.price ) )			.setText( product.getPrice() );
		(( TextView ) findViewById( R.id.offer ) )			.setText( TextUtils.isEmpty(product.getOffer()) ? this.getResources().getString(R.string.no_offer)  : product.getOffer() );
		*/
		ImageView pick = ( ImageView ) findViewById( R.id.image1 );


		ServiceStub service = ServiceLocator.lookup( ServiceName.PRODUCT_THUMB );
		String imageName = service.getUrl() + service.resolveParams( product.getId().concat(".jpg") );
		
		/*
		Bitmap cachedImage = ImageUtil.getBitmapFromFile( imageName );
		if (cachedImage != null){	
			pick. setImageBitmap(cachedImage);
		}else {
			ImageDownloader imageDown = new ImageDownloader();
			imageDown.download( imageName, pick, this );
		}		
		*/
		if( amountEditable ){
			(( TextView ) findViewById( R.id.quantity_text ) ).setVisibility( View.VISIBLE );
			final EditText quantity = ( EditText ) findViewById( R.id.quantity );
			
			quantity.setVisibility( View.VISIBLE );			
			quantity.setText( product.getAmount() );
			
			quantity.addTextChangedListener( new TextWatcher() {				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {					
				}				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,int after) {		
				}				
				@Override
				public void afterTextChanged(Editable s) {
					if( quantity.getText().toString().length() >0 ){
						int quant = Integer.valueOf( quantity.getText().toString() );
					    int stock = 0;
					    
					    try{					    						    	
					    	NumberFormat format = NumberFormat.getInstance( Locale.US );					    	
					    	stock = format.parse( product.getStock() ).intValue();					    	
					    }catch( Exception ex ){
					    	ex.printStackTrace();
					    	TestFlight.log( "Error parsing string-to-number: "+ex.getMessage() );
					    }
					    
					    if( stock < quant ){
					    	String mesg = getString( R.string.not_accepted_quantity );
					    	Toast.makeText( ProductDetailActivity.this, mesg+" "+stock, Toast.LENGTH_LONG ).show();
					    	return;
					    }
					}					
					product.setAmount( quantity.getText().toString() );
				}
			});
		}
		
		
		final Spinner salesUnit = ( Spinner ) findViewById( R.id.sales_units );
		
		List<UnitEntity> salesUnits = UnitsDataModel.getUnitsByProduct(ProductDetailActivity.this, product.getMaterial());
		if(product.getSalesUnits() == null)
		{
			product.setSalesUnits(salesUnits);
		}
		final List<UnitEntity> units = product.getSalesUnits();
		final List<String> unitsString = new ArrayList<String>();
		for(UnitEntity unit : units)
		{
			unitsString.add(unit.getMeinh());
		}
		if(units != null && units.size() > 0 )
		{
			ArrayAdapter<String> salesUnitsAdapter = new ArrayAdapter<String>( this, android.R.layout.simple_spinner_item, unitsString);
			salesUnit.setAdapter( salesUnitsAdapter );
			
			salesUnit.setOnItemSelectedListener(new OnItemSelectedListener() 
			{
			    @Override
			    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
			    {
			    	if( units!= null && !units.isEmpty() )		   
			    		product.setSelectedSalesUnit( units.get(position) );
			    }
			    @Override
			    public void onNothingSelected(AdapterView<?> parentView) {
			    }
			    
			});		
		}
		
		
		/*
		final Spinner centers = ( Spinner ) findViewById( R.id.centers );
		ArrayAdapter<String> centersAdapter = new ArrayAdapter<String>( this, android.R.layout.simple_spinner_item,product.getCenters() );
		centers.setAdapter( centersAdapter );
		
		centers.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		    	if( product.getCenters() != null && !product.getCenters().isEmpty() ){
		    		product.setCenter( product.getCenters().get( position ) );
		    	}else
		    		product.setCenter( "BP01" );
		    }
		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		    }
		});	
		
			*/
	}
		
	
	public void onAddProduct( MenuItem item ){
		
		Intent data = new Intent();
		data.putExtra( EXTRA_BACKED_PRODUCT, product );
		setResult( RESULT_OK,data );
		finish();
	}		
	
}

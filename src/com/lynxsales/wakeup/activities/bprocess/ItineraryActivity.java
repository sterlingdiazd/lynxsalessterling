package com.lynxsales.wakeup.activities.bprocess;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.lynx.sales.bundle.app.LynxApplication;
import com.lynx.sales.bundle.dto.model.ArchiveDataModel;
import com.lynx.sales.bundle.dto.model.ClientDataModel;
import com.lynx.sales.bundle.dto.model.ItineraryDataModel;
import com.lynx.sales.bundle.dto.model.ScheduleDataModel;
import com.lynx.sales.bundle.dto.model.SyncDataModel;
import com.lynx.sales.bundle.dto.model.UserRetrieval;
import com.lynx.sales.bundle.dto.prefs.UserPreferences;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob.Status;
import com.lynx.sales.bundle.entities.Archive;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.entities.ItineraryEntity;
import com.lynx.sales.bundle.entities.RouteEntity;
import com.lynx.sales.bundle.entities.RouteEntry;
import com.lynx.sales.bundle.entities.Sync;
import com.lynx.sales.bundle.entities.UserEntity;
import com.lynx.sales.bundle.http.request.SynchronizeRequest;
import com.lynxsales.printer.Configuration;
import com.lynxsales.wakeup.activities.BaseActivity;
import com.lynxsales.wakeup.activities.DownloadActivity;
import com.lynxsales.wakeup.activities.LoginActivity;
import com.lynxsales.wakeup.activities.SynchronizeActivity;
import com.lynxsales.wakeup.activities.adapter.DrawerAdapter;
import com.lynxsales.wakeup.activities.adapter.RouteAdapter;
import com.lynxsales.wakeup.activities.dto.DrawerEntry;
import com.lynxsales.wakeup.activities.prefs.LynxPreferences;
import com.lynxsales.wakeup.features.Feature;
import com.lynxsales.wakeup.features.KnowledgeFeature;
import com.lynxsales.wakeup.noapi.request.Utils;
import com.lynxsales.wakeup.R;

@SuppressLint("ResourceAsColor")
public class ItineraryActivity extends BaseActivity implements OnScrollListener
{

    private static final int FLIP_LOADING = 0;
    private static final int FLIP_NOT_FOUND = 1;
    private static final int FLIP_CONTENT = 2;
    public static Boolean createNewClient = false;
    private Activity activity;
    private boolean allowedMenu;
    private Configuration configuracion;
    private List<DrawerEntry> drawerEntries;
    protected DrawerLayout drawerLayout;
    protected ActionBarDrawerToggle drawerToggle;
    private ListView drawerList;
    private List<ItineraryEntity> itineraries;
    private ItineraryEntity itinerary;
    private List<RouteEntity> routes;
    private RouteAdapter routeAdapter;
    private List<RouteEntity> routeEntities;
    private int start = 0;
    private int limit = 10;
    private View loadMoreView;
    private boolean loadingMore = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_itinerary);
	init();
    }

    @Override
    protected void onResume()
    {
	super.onResume();

    }

    private void init()
    {
	initActionBar();
	initDrawerLayout();
	prepareItinerary();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
	boolean render = allowedMenu && !drawerLayout.isDrawerOpen(drawerList);
	if (drawerLayout != null)
	    menu.setGroupVisible(R.id.group_hiden_1, render);
	return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
	getMenuInflater().inflate(R.menu.itinerary, menu);

	SearchView search = (SearchView) menu.findItem(R.id.action_search).getActionView();
	search.setQueryHint(getString(R.string.search_routes));
	search.setOnQueryTextListener(queryListener);

	int id = search.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);

	TextView text = (TextView) search.findViewById(id);
	text.setHintTextColor(Color.WHITE);

	return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
	if (drawerToggle.onOptionsItemSelected(item))
	{
	    KnowledgeFeature.setRecognizedFeature(this, Feature.DRAWER_VIEW, true);
	    return true;
	}
	return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
	super.onPostCreate(savedInstanceState);
	drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(android.content.res.Configuration newConfig)
    {
	super.onConfigurationChanged(newConfig);
	drawerToggle.onConfigurationChanged(newConfig);
    }

    private void initActionBar()
    {
	this.activity = ItineraryActivity.this;
	LynxApplication app = (LynxApplication) getApplication();
	UserEntity user = UserRetrieval.getUser(getApplicationContext());
	if (user != null)
	{
	    String name = user.getName();
	    getActionBar().setSubtitle(user.getName());
	}
	getActionBar().setDisplayHomeAsUpEnabled(true);
	getActionBar().setHomeButtonEnabled(true);
    }

    private void initDrawerLayout()
    {

	if (drawerLayout == null)
	    drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

	drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);

	drawerEntries = getDrawerEntries();
	DrawerAdapter adapter = new DrawerAdapter(this, R.layout.entry_drawer_layout, drawerEntries);
	drawerList = (ListView) findViewById(R.id.drawer_list);
	drawerList.setAdapter(adapter);

	registerDrawerEntryListener(drawerList);
	setDrawerToggleListener();
	forceDrawerBeRecognized(drawerList);

    }

    private void registerDrawerEntryListener(ListView list)
    {
	list.setOnItemClickListener(new OnItemClickListener()
	{
	    @Override
	    public void onItemClick(AdapterView<?> adapterView, View c, int position, long id)
	    {
		dispatchDrawerSelection(c, position);
	    }
	});
    }

    private void forceDrawerBeRecognized(View v)
    {
	if (!KnowledgeFeature.recogizedFeature(this, Feature.DRAWER_VIEW))
	{
	    drawerLayout.openDrawer(v);
	}
    }

    private void setDrawerToggleListener()
    {
	drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.string.open_mode,
		R.string.closed_mode)
	{
	    @Override
	    public void onDrawerClosed(View drawerView)
	    {
		getActionBar().setTitle(R.string.itinerary);

		invalidateOptionsMenu();
		super.onDrawerClosed(drawerView);
	    }

	    @Override
	    public void onDrawerOpened(View drawerView)
	    {
		getActionBar().setTitle(R.string.lynxsales);

		drawerEntries = getDrawerEntries();

		DrawerAdapter adapter = new DrawerAdapter(drawerView.getContext(), R.layout.entry_drawer_layout,
			drawerEntries);
		drawerList.setAdapter(adapter);
		drawerList.refreshDrawableState();

		invalidateOptionsMenu();
		super.onDrawerOpened(drawerView);
	    }

	};

	drawerLayout.setDrawerListener(drawerToggle);
    }

    protected List<Archive> refreshArchives()
    {
	return ArchiveDataModel.getArchives(this);
    }

   
    private List<DrawerEntry> getDrawerEntries()
    {

	ScheduleDataModel model = new ScheduleDataModel();
	List<ScheduleJob> jobs = model.getShedulesJobs(this);

	int pendingCount = 0;
	int responseCount = 0;

	if (null != jobs)
	{
	    for (ScheduleJob job : jobs)
	    {
		if (Status.PENDING == job.getStatus())
		    pendingCount++;
		else
		    responseCount++;
	    }
	}

	int archiveCount = refreshArchives().size();

	// ProcessMemento.restore( this,client.getId());
	return Arrays.asList(
		// new DrawerEntry(R.drawable.ic_perfil,
		// getString(R.string.perfil), null),
		new DrawerEntry(R.drawable.ic_sync, getString(R.string.data_download), DownloadActivity.class),
		new DrawerEntry(R.drawable.ic_info, getString(R.string.archived), ArchiveList.class, 0 != archiveCount,
			archiveCount),
		// new DrawerEntry(R.drawable.ic_cross,
		// getString(R.string.tracking), null),
		// new DrawerEntry(R.drawable.ic_report,
		// getString(R.string.report), null),
		new DrawerEntry(R.drawable.ic_info, getString(R.string.pending_jobs), PendingList.class,
			0 != pendingCount, pendingCount),
		new DrawerEntry(R.drawable.ic_sync, getString(R.string.send_orders), SynchronizeActivity.class),
		new DrawerEntry(R.drawable.ic_info, getString(R.string.error_log), ResponseLogActivity.class,
			0 != responseCount, responseCount),
		new DrawerEntry(R.drawable.ic_settings, getString(R.string.settings), LynxPreferences.class),
		// new DrawerEntry(R.drawable.ic_help, getString(R.string.help),
		// null),
		new DrawerEntry(R.drawable.ic_logout, getString(R.string.logout), LoginActivity.class));
    }

    private void prepareItinerary()
    {
	new AsyncTask<Void, Void, Boolean>()
	{
	    @Override
	    protected void onPreExecute()
	    {
		fadeFlipView(R.id.flipper_view, FLIP_LOADING);
		checkSync();
	    }

	    private void checkSync()
	    {
		List<Sync> listSyncEntities = SyncDataModel.getAllSync(ItineraryActivity.this.activity);
		ArrayList<Sync> pendingSyncList = new ArrayList<Sync>();

		for (Sync sync : listSyncEntities)
		{

		    if (!sync.getEntity().equalsIgnoreCase("clients") && !sync.getEntity().equalsIgnoreCase("itineraries") && !sync.getEntity().equalsIgnoreCase("products"))
		    {
			String currentDatetime = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
			String savedDatetime = sync.getDate() != null ? sync.getDate() : "";
			String status = sync.getStatus() != null ? sync.getStatus() : "";

			if (status.equalsIgnoreCase("unsync") || !savedDatetime.equalsIgnoreCase(currentDatetime))
			{
			    pendingSyncList.add(sync);
			} 
		    } 
		}

		if (pendingSyncList.size() > 0)
		{
		    ItineraryActivity.this.fadeFlipView(2131623945, 2);
		    Intent localIntent = new Intent(ItineraryActivity.this.activity, DownloadActivity.class);
 		    localIntent.putExtra("unsync", (Serializable) pendingSyncList);
		    localIntent.setFlags(268468224);
		    ItineraryActivity.this.startActivity(localIntent);
		}
	    }

	    @Override
	    protected Boolean doInBackground(Void... params)
	    {
		Boolean result = false;
		LynxApplication app = (LynxApplication) getApplication();
		UserEntity user = app.getUser();

		itineraries = ItineraryDataModel.getSellerItineraries(user.getId(), ItineraryActivity.this, start,
			limit);
		int itinQuantity = itineraries.size();
		if (itinQuantity > 0)
		{
		    itinerary = itineraries.get(0);
		    routes = new ArrayList<RouteEntity>();

		    if (itinerary.getId() != null)
		    {
			List<RouteEntity> newRoutes = itinerary.getRoutes();
			
			routes.addAll(newRoutes);
			if (routes != null && !routes.isEmpty())
			{
			    start += newRoutes.size();
			    result = true;
			    allowedMenu = true;
			    
			} else
			{
			  //fadeFlipView(R.id.flipper_view, FLIP_NOT_FOUND);
			    result = false;

			}
			invalidateOptionsMenu();
		    }
		} else
		{
		    result = false;
		    //fadeFlipView(R.id.flipper_view, FLIP_CONTENT);
		}
		return result;
	    };

	    @Override
	    protected void onPostExecute(Boolean result)
	    {
		super.onPostExecute(result);
		if (result)
		{
		    fadeFlipView(R.id.flipper_view, FLIP_CONTENT);
		    setViewData();
		} else 
		{
		    fadeFlipView(R.id.flipper_view, FLIP_NOT_FOUND);
		}

	    }
	}.execute();

    }

    @Override
    protected void onPause()
    {
	super.onPause();

    }

    private void registerRouteListener(ListView list)
    {
	list.setOnItemClickListener(new OnItemClickListener()
	{
	    @Override
	    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id)
	    {
		RouteEntity route = (RouteEntity) adapterView.getItemAtPosition(position);
		configureClient(activity, route);
	    }
	});
    }

    private void configureClient(Activity activity, RouteEntity route)
    {
	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
	Boolean isClientCreationEnabled = prefs.getBoolean("key_client_creation", false);

	if (isClientCreationEnabled)
	{
	    configuracion = Configuration.getInstance();
	    configuracion.configureSharePreference(getBaseContext());
	    dialogAskCreateClient(activity, route).show();
	} else
	{
	    Intent intent = new Intent(activity, RouteActivity.class);
	    intent.putExtra(RouteActivity.EXTRA_ROUTE_DATA, route);
	    intent.putExtra("createNewClient", createNewClient);
	    startActivity(intent);
	}
    }

    private Dialog dialogAskCreateClient(final Context context, final RouteEntity route)
    {

	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
	alertDialogBuilder.setTitle("Registrar Nuevo Usuario");
	alertDialogBuilder.setMessage("Desea registrar un nuevo cliente?");
	// alertDialogBuilder.setIcon( );

	alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
	{
	    public void onClick(DialogInterface dialog, int which)
	    {
		createNewClient = true;
		dialog.cancel();

		Intent intent = new Intent(context, RouteActivity.class);
		intent.putExtra(RouteActivity.EXTRA_ROUTE_DATA, route);
		intent.putExtra("createNewClient", createNewClient);
		startActivity(intent);
	    }
	});

	alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener()
	{
	    public void onClick(DialogInterface dialog, int which)
	    {
		createNewClient = false;
		dialog.cancel();

		Intent intent = new Intent(context, RouteActivity.class);
		intent.putExtra(RouteActivity.EXTRA_ROUTE_DATA, route);
		intent.putExtra("createNewClient", createNewClient);
		startActivity(intent);
	    }
	});

	return alertDialogBuilder.create();
    }

    private void setViewData()
    {
	setHeaderInfo();
	setViewRoutes();
    }

    private void setHeaderInfo()
    {
	if (itinerary != null)
	{
	    ((TextView) findViewById(R.id.description)).setText(itinerary.getDescription());
	    ((TextView) findViewById(R.id.name)).setText(itinerary.getId());
	    ((TextView) findViewById(R.id.created_by)).setText(itinerary.getCreatedBy());
	    ((TextView) findViewById(R.id.created_at)).setText(Utils.formatDate(itinerary.getCreationDate()));
	}
    }

    private void setViewRoutes()
    {
	routeAdapter = new RouteAdapter(ItineraryActivity.this, R.layout.entry_itinerary_route, routes);

	ListView routesList = (ListView) findViewById(android.R.id.list);

	routesList.setAdapter(routeAdapter);
	routesList.setTextFilterEnabled(true);
	registerRouteListener(routesList);
	routesList.setOnScrollListener(this);
    }
    
    private void dispatchDrawerSelection(View v, int position)
    {

	if (drawerLayout.isDrawerOpen(drawerList))
	    drawerLayout.closeDrawer(drawerList);

	DrawerEntry selected = drawerEntries.get(position);
	Class<?> backedActivity = selected.getBackedActivity();
	Intent intent = new Intent();

	if (null != backedActivity && LoginActivity.class.equals(backedActivity))
	{
	    UserPreferences.setLogged(this, false);
	    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
	}

	if (null != backedActivity && SynchronizeActivity.class.equals(backedActivity))
	{
	    intent.putExtra(SynchronizeActivity.EXTRA_REENTRANT_SYNC, true);
	}

	if (null != backedActivity && Void.class.equals(backedActivity))
	    throw new UnsupportedOperationException("");

	if (backedActivity != null)
	{
	    intent.setClass(this, backedActivity);
	    startActivity(intent);
	}
    }


    private final OnQueryTextListener queryListener = new OnQueryTextListener()
    {
	@Override
	public boolean onQueryTextSubmit(String query)
	{
	    if (routeAdapter != null)
	    {
		routeAdapter.getFilter().filter(query);
		return true;
	    } else
	    {
		return false;
	    }
	}

	@Override
	public boolean onQueryTextChange(String query)
	{
	    if (routeAdapter != null)
	    {
		routeAdapter.getFilter().filter(query);
		return true;
	    } else
	    {
		return false;
	    }
	}
    };

    @Override
    public void onScroll(AbsListView view,  int firstVisibleItem, int visibleItemCount, int totalItemCount)
    {
	int itemsVisibles = firstVisibleItem + visibleItemCount;
        if (itemsVisibles > 5 && itemsVisibles == totalItemCount) {
            ItineraryActivity.this.loadMoreItems(ItineraryActivity.this.itinerary);
            return;
        }
        Log.e((String)"e", (String)"No loading more");
    }

    public void loadMoreItems(final ItineraryEntity itineraryEntity) {
        
	new AsyncTask<Void, Void, Boolean>()
	{
	    List<RouteEntity> newRoutes = null;
	    @Override
	    protected void onPreExecute()
	    {
		//fadeFlipView(R.id.flipper_view, FLIP_LOADING);
		
	    }

	    @Override
	    protected Boolean doInBackground(Void... params)
	    {
		Boolean result = false;
		newRoutes = ItineraryDataModel.getRoutes(activity, itineraryEntity.getId(), start, limit);
		if(newRoutes != null && newRoutes.size() > 0)
		{
		    start += newRoutes.size();
		    return true;
		}
		return false;
	    };

	    @Override
	    protected void onPostExecute(Boolean result)
	    {
		super.onPostExecute(result);
		if (result)
		{
		    //fadeFlipView(R.id.flipper_view, FLIP_CONTENT);
		    //routes.addAll(newRoutes);
		    routeAdapter.addAll(newRoutes);
		    routeAdapter.notifyDataSetChanged();
		    
		} else 
		{
		    
		}

	    }
	}.execute();
	
	
        
    }
    
    @Override
    public void onScrollStateChanged(AbsListView arg0, int arg1)
    {
	
    }
}

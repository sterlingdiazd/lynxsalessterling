package com.lynxsales.wakeup.activities;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.lynxsales.wakeup.R;
import com.lynx.sales.bundle.dto.prefs.UserPreferences;
import com.testflightapp.lib.TestFlight;

public class LicenseActivity extends BaseActivity {

	
	private static final String DUMMIE_KEY = "beta12345";
	
	private static final int FLIP_LOADING  = 0;
	private static final int FLIP_ERROR	   = 1;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_license);
		
		init();
		initLogoAnimation();		
	}

	
	
	private void init(){
		TextView loadingStatus = ( TextView ) findViewById( R.id.feedback_loading_text );
		loadingStatus.setText( getResources().getText( R.string.validating_license ) );
	}
	
	
	
	public void onRegister(View v) {
		/**
		 * Aqui se realizara el registro del dispositivo
		 */

		final EditText key  = ( EditText ) findViewById( R.id.edit1 );
		final TextView loadingStatus = ( TextView ) findViewById( R.id.feedback_loading_text );
		loadingStatus.setText( getResources().getText( R.string.validating_license ) );
		
		fadeFlipView( R.id.feedback_flipper,FLIP_LOADING );
		
		new AsyncTask<Void, Void, Void>() {			
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				flipupFeedbackView();
				fadeFlipView( R.id.feedback_flipper,FLIP_LOADING );
			}

			@Override
			protected Void doInBackground(Void... arg0) {
				try {
					Random random = new Random();
					TimeUnit.MILLISECONDS.sleep( random.nextInt( 500 ) );			
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				if( key.getText().toString().equals( DUMMIE_KEY ) ){
					UserPreferences.setLicensed(LicenseActivity.this, true );
					passActivity();
				}else{					
					TextView failured = ( TextView ) findViewById( R.id.feedback_error_text );
					failured.setText( getResources().getText( R.string.validating_failured ) );					
					fadeFlipView( R.id.feedback_flipper,FLIP_ERROR );
				}				
			}
		}.execute();
	}

	
	private void setRegisteredState(boolean registered) {
		/*
		 * Realizar cualquier trabajo para setear als condiciones de registros
		 */
	}
	
	
	// Needed for loadng_status_view.xml layout
	public void onRetry(View view) {
		onRegister(view);
	}

	
	
	private void flipupFeedbackView() {
		ViewGroup container = (ViewGroup) findViewById(R.id.feedback_content );
		container.animate().translationY(0).setDuration(500).start();
	}
	
	

	private void passActivity() {
		TestFlight.passCheckpoint("License Accepted.!");

		
		Intent intent = new Intent(this, ConditionActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

}

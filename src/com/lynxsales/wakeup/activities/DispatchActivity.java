package com.lynxsales.wakeup.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.lynx.sales.bundle.dto.prefs.UserPreferences;
import com.lynxsales.wakeup.activities.bprocess.ItineraryActivity;
import com.testflightapp.lib.TestFlight;


public class DispatchActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dispatch();
	}
	
	
	private void dispatch(){
		Class<?> activity = null;
	
		if( !UserPreferences.isLicensed(this) )
			activity = LicenseActivity.class;
		else if( !UserPreferences.acceptedConditions(this) )
			activity = ConditionActivity.class;
		else if( !UserPreferences.isLogged( this ) )
			activity = LoginActivity.class;
		else if( !UserPreferences.isSynchronized( this ) )
			activity = SynchronizeActivity.class;
		else
			activity = ItineraryActivity.class;
		
			
		TestFlight.log( "Activity Dispatched: "+activity );

		
		Intent intent = new Intent( this,activity );
		intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
		startActivity(  intent );
	}
	
	
}

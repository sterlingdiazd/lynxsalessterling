package com.lynxsales.wakeup.activities.dto;

public class DrawerEntry {
	
	private int icon;
	private String title;
	private boolean hasBudget;
	private int budgetCount;
	private Class<?> backedActivity;
	
	
	public DrawerEntry(  int icon,String title,Class<?> backedActivity ){
		this.icon = icon;
		this.title = title;
		this.backedActivity = backedActivity;
	}
	
	
	public DrawerEntry(  int icon, String title, Class<?> backedActivity, boolean hasBudget,int budgetCount ){
		this( icon,title,backedActivity );
		this.hasBudget =  hasBudget;
		this.budgetCount = budgetCount;
	}

	
	public int getIcon(){
		return icon;
	}
	
	public void setBudgetCount(int budgetCount) {
		this.budgetCount = budgetCount;
	}


	public String getTitle(){
		return title;
	}
	
	public boolean hasBudget() {
		return hasBudget;
	}
	
	public int getBudgetCount(){
		return budgetCount;
	}
	
	public Class<?> getBackedActivity(){
		return backedActivity;
	}
}

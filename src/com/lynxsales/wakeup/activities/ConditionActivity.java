package com.lynxsales.wakeup.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.lynxsales.wakeup.utils.CommonUtils;
import com.lynxsales.wakeup.R;
import com.lynx.sales.bundle.dto.prefs.UserPreferences;
import com.testflightapp.lib.TestFlight;

public class ConditionActivity extends BaseActivity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView( R.layout.activity_condition );
		
		init();
		initLogoAnimation();
	}
	
	
	
	private void init(){
		
		TextView text1 = ( TextView ) findViewById( android.R.id.text1 ) ;
		TextView text2 = ( TextView ) findViewById( android.R.id.text2 ) ;
		
		setMovementLinkText( text1,text2 );
		stripUnderlineText(text1,text2 );
	}
	
	private void setMovementLinkText( TextView...texts ){
		if( texts == null )
			return;
		for( TextView text : texts )
			setLinkMovement(text);				
	}
	
	private void stripUnderlineText( TextView...texts ){
		if( texts == null )
			return;
		for( TextView text : texts )
			CommonUtils.stripUnderline(text);		
	}
	
	public void onAccept( View view ){
		TestFlight.passCheckpoint( "Privacy Terms Passed.!" );		
		UserPreferences.setAcceptedCondition(this, true);		
		passActivity();
	}
	
	
	public void onClose( View view ){
		finish();
	}
	
	
	private void passActivity(){
		Intent intent = new Intent( this,LoginActivity.class );
		intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
		startActivity( intent );
	}
	
	
	private void setLinkMovement( TextView text ){		
		text.setMovementMethod( LinkMovementMethod.getInstance() );
	}
	
	
}


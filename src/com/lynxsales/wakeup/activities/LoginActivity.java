package com.lynxsales.wakeup.activities;

import java.util.ArrayList;
import java.util.List;

import org.lyxar.envelope.Broker;
import org.lyxar.envelope.CompletionHandler;
import org.lyxar.envelope.sp.BrokersServices;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.lynxsales.wakeup.R;
import com.lynx.sales.bundle.app.LynxApplication;
import com.lynx.sales.bundle.app.ProviderService;
import com.lynx.sales.bundle.dto.prefs.BrokersPreferences;
import com.lynx.sales.bundle.dto.prefs.UserPreferences;
import com.lynx.sales.bundle.entities.LoginResult;
import com.lynx.sales.bundle.http.request.ErrorStub;
import com.lynx.sales.bundle.http.request.PhaseHandler;
import com.lynx.sales.bundle.http.request.Requestor;
import com.lynx.sales.bundle.http.request.ResponseBinder;
import com.lynx.sales.bundle.http.request.ResponseStub;
import com.lynx.sales.bundle.http.request.ServiceLocator;
import com.lynx.sales.bundle.http.request.ServiceName;
import com.lynx.sales.bundle.http.request.ServiceStub;
import com.lynxsales.printer.Configuration;
import com.lynxsales.wakeup.activities.bprocess.ItineraryActivity;
import com.lynxsales.wakeup.noapi.request.DefaultRequestor;
import com.testflightapp.lib.TestFlight;

public class LoginActivity extends BaseActivity
{

    private static final int FLIP_LOADING = 0;
    private static final int FLIP_ERROR = 1;

    private static final int FLIP_DIRECTION_UP = 0;
    private static final int FLIP_DIRECTION_DOWN = 1;

    private EditText editName;
    private EditText editPass;
    private Spinner spBrokers;
    private Configuration configuracion;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_login);

	init();
	initLogoAnimation();
    }

    private void init()
    {
	/*
	 * if(isGooglePlayServicesAvailable(){
	 * 
	 * }
	 */
	preloadProvider();
    }

    private void initComponent()
    {
	editName = (EditText) findViewById(R.id.edit1);
	editPass = (EditText) findViewById(R.id.edit2);
	spBrokers = (Spinner) findViewById(R.id.spinner1);

	List<Broker> providers = BrokersServices.defaultBrokerService(this).getBrokers();
	List<BrokerEntry> listProviders = new ArrayList<LoginActivity.BrokerEntry>();
	for (Broker provider : providers)
	{
	    listProviders.add(new BrokerEntry(provider.getId(), provider.getName()));
	}
	spBrokers.setAdapter(
		new ArrayAdapter<BrokerEntry>(this, android.R.layout.simple_dropdown_item_1line, listProviders));
    }

    private void enabledWidgets(boolean enabled)
    {
	Button sigin = (Button) findViewById(android.R.id.button1);
	sigin.setEnabled(enabled);
    }

    private void preloadProvider()
    {
	enabledWidgets(false);
	TextView flipLoadingText = (TextView) findViewById(R.id.feedback_loading_text);
	flipLoadingText.setText(getString(R.string.resolving_brokers));
	flipFeedbackView(FLIP_DIRECTION_UP);

	BrokersServices.defaultInvariantChecker().resolve(this, new CompletionHandler()
	{
	    @Override
	    public void onCompleted()
	    {
		LynxApplication app = (LynxApplication) getApplication();
		BrokersPreferences.setBrokersResolved(app, true);

		initComponent();
		enabledWidgets(true);
		flipFeedbackView(FLIP_DIRECTION_DOWN);
	    }
	});

	configuracion = Configuration.getInstance();
	configuracion.configureSharePreference(getBaseContext());
    }

    public void onLogin(View v)
    {
	if (!validBroker())
	{
	    showFeedbackBadBroker();
	    return;
	}

	Requestor req = new DefaultRequestor(this, handler);
	ServiceStub stub = ServiceLocator.lookup(ServiceName.LOGIN);
	req.request(stub, editName.getText().toString(), editPass.getText().toString());
    }

    // Needed for loadng_status_view.xml layout
    public void onRetry(View v)
    {
	onLogin(v);
    }

    private void flipFeedbackView(int direction)
    {
	float translation = 0.0f;
	if (FLIP_DIRECTION_DOWN == direction)
	    translation = getResources().getDimension(R.dimen.dp_60);

	ViewGroup container = (ViewGroup) findViewById(R.id.feedback_content);
	container.animate().translationY(translation).setDuration(500).start();
    }

    private void passActivity()
    {
	TestFlight.passCheckpoint("User Logged correctly.!");

	UserPreferences.setLogged(this, true);
	Class<?> activity = null;

	if (UserPreferences.isSynchronized(this))
	    activity = ItineraryActivity.class;
	else
	    activity = DownloadActivity.class;

	// SharedPreferences.Editor editor =
	// getSharedPreferences(GAME_PREFERENCES, MODE_PRIVATE);
	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
	prefs.edit().putBoolean("key_center_filter_enabled", true).commit();

	Intent intent = new Intent(this, activity);
	intent.putExtra("first_use", true);
	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
	startActivity(intent);
    }

    private void loginFaseUnPassed()
    {
	TextView error = (TextView) findViewById(R.id.feedback_error_text);
	error.setText(getResources().getString(R.string.unable_authentication));

	fadeFlipView(R.id.feedback_flipper, FLIP_ERROR);
    }

    private boolean validBroker()
    {
	BrokerEntry broker = (BrokerEntry) spBrokers.getSelectedItem();
	if (null != broker)
	{
	    ProviderService.getInstance().register(BrokersServices.defaultBrokerService(this).getBrokerById(broker.id));

	    BrokersPreferences.setDefaultBrokerService(this, broker.id);
	    if (broker.name.equalsIgnoreCase("lynxdb"))
	    {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		configuracion.getPrefsEditor().putBoolean("key_sanut", true).commit();
		if (prefs.edit().putBoolean("key_client_creation", true).commit())
		{
		    Log.e("Pref", "Success");
		} else
		{
		    Log.e("Pref", "Error");
		}
	    }

	    return true;
	} else
	{
	    return false;
	}

    }

    private void showFeedbackBadBroker()
    {
	TextView error = (TextView) findViewById(R.id.feedback_error_text);
	error.setText(getResources().getString(R.string.provider_not_supported));

	flipFeedbackView(FLIP_DIRECTION_UP);
	fadeFlipView(R.id.feedback_flipper, FLIP_ERROR);
    }

    private PhaseHandler handler = new PhaseHandler()
    {
	@Override
	public void onFeedback(String feed)
	{
	}

	@Override
	public void onError(ErrorStub stub)
	{
	}

	@Override
	public void onCompleted(ResponseStub response)
	{
	    LoginResult result = ResponseBinder.bindLogin(response);

	    if (result.isLogged())
	    {
		LynxApplication app = (LynxApplication) getApplication();
		app.setUser(result.getUser());

		UserPreferences.saveUserLogged(LoginActivity.this, result.getUser().getId());
		passActivity();
	    } else
		loginFaseUnPassed();

	    enabledWidgets(true);
	}

	@Override
	public void onBegan()
	{
	    TextView loadingStatus = (TextView) findViewById(R.id.feedback_loading_text);
	    loadingStatus.setText(getResources().getText(R.string.authenticating));

	    flipFeedbackView(FLIP_DIRECTION_UP);
	    enabledWidgets(false);
	    fadeFlipView(R.id.feedback_flipper, FLIP_LOADING);
	}
    };

    private class BrokerEntry
    {
	String id;
	String name;

	public BrokerEntry(String id, String name)
	{
	    this.id = id;
	    this.name = name;
	}

	@Override
	public String toString()
	{
	    return name;
	}
    }
}
package com.lynxsales.wakeup.activities.prefs;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.MenuItem;

import com.lynxsales.wakeup.R;

//@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class LynxPreferences extends PreferenceActivity
{
	public LynxPreferences() {
		super();
	}


	private static List<String> fragments = new ArrayList<String>();
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setTitle( R.string.settings );
		getActionBar().setDisplayHomeAsUpEnabled( true );
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		onBackPressed();
		return super.onOptionsItemSelected(item);
	}
	
	
	@Override
	public void onBuildHeaders(List<Header> target) {
		super.onBuildHeaders(target);			
		loadHeadersFromResource( R.xml.main_prefs,target );	
		fragments.clear();
        for (Header header : target) {
            fragments.add(header.fragment);
        }
	}	
	
	
	@SuppressLint("Override")
	protected boolean isValidFragment(String fragmentName)
    {
        return fragments.contains(fragmentName);
    }
    
}

package com.lynxsales.wakeup.activities.prefs;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.lynxsales.wakeup.R;

public class LynxCompacPreferences extends PreferenceActivity{

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource( R.xml.compact_main_prefs );
	}
	
}

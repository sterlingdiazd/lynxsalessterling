package com.lynxsales.wakeup.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.lynx.sales.bundle.app.LynxApplication;
import com.lynx.sales.bundle.app.LynxApplication;
import com.lynx.sales.bundle.dto.prefs.UserPreferences;
import com.lynx.sales.bundle.dto.prefs.UserPreferences;
import com.lynx.sales.bundle.entities.Sync;
import com.lynx.sales.bundle.entities.UserEntity;
import com.lynx.sales.bundle.entities.UserEntity;
import com.lynx.sales.bundle.http.request.ErrorStub;
import com.lynx.sales.bundle.http.request.ErrorStub;
import com.lynx.sales.bundle.http.request.PhaseHandler;
import com.lynx.sales.bundle.http.request.PhaseHandler;
import com.lynx.sales.bundle.http.request.Requestor;
import com.lynx.sales.bundle.http.request.ResponseStub;
import com.lynx.sales.bundle.http.request.ResponseStub;
import com.lynx.sales.bundle.http.request.SynchronizeRequest;
import com.lynx.sales.bundle.http.request.SynchronizeRequestDelegate;
import com.lynx.sales.bundle.http.request.SynchronizeRequest;
import com.lynxsales.wakeup.R;
import com.lynxsales.wakeup.activities.bprocess.ItineraryActivity;
import com.lynxsales.wakeup.noapi.request.SynchronizationDelegate;
import com.lynxsales.wakeup.noapi.request.SynchronizationDelegateAll;
import com.lynxsales.wakeup.noapi.request.SynchronizationDelegateClient;
import com.lynxsales.wakeup.noapi.request.SynchronizationDelegateCondiciones;
import com.lynxsales.wakeup.noapi.request.SynchronizationDelegateItinerary;
import com.lynxsales.wakeup.noapi.request.SynchronizationDelegateProductos;
import com.testflightapp.lib.TestFlight;
import com.testflightapp.lib.TestFlight;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Handler;
import android.util.Log;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView;

public class DownloadActivity extends Activity implements AdapterView.OnItemClickListener
{

    public static final String EXTRA_REENTRANT_SYNC = "com.lynx.sales.activities.SynchronizeActivity.EXTRA_REENTRANT_SYNC";
    private Activity activity;
    private View currentSelectedView;
    private TextView feedback;
    private boolean first_use;
    private final PhaseHandler handler;
    private final PhaseHandler handlerSync;
    private final PhaseHandler handlerUnsync;
    private ListView listSyncType;
    private String syncItem;
    private List<Sync> unsyncList;
    private final Handler osHandler;


    public DownloadActivity()
    {
	this.osHandler = new Handler()
	{
	    public void handleMessage(Message message)
	    {
		if (message != null)
		{
		    DownloadActivity.this.feedback.setText((CharSequence) message.obj.toString());
		}
	    }
	};
	this.handler = new PhaseHandler()
	{
	    @Override
	    public void onBegan()
	    {
	    }

	    @Override
	    public void onCompleted(ResponseStub responseStub)
	    {
		DownloadActivity.this.passActivity();
	    }

	    @Override
	    public void onError(ErrorStub errorStub)
	    {
		Log.e((String) "$$$$$$$$$$$$$$$$", (String) ("Error: " + errorStub.getReason()));
	    }

	    @Override
	    public void onFeedback(String string)
	    {
		if (string != null)
		{
		    DownloadActivity.this.osHandler
			    .sendMessage(DownloadActivity.this.osHandler.obtainMessage(1, (Object) string));
		}
	    }
	};
	this.handlerSync = new PhaseHandler()
	{
	    @Override
	    public void onBegan()
	    {
	    }

	    @Override
	    public void onCompleted(ResponseStub responseStub)
	    {
		DownloadActivity.this.chooseSyncEntities((Activity) DownloadActivity.this);
	    }

	    @Override
	    public void onError(ErrorStub errorStub)
	    {
		Log.e((String) "$$$$$$$$$$$$$$$$", (String) ("Error: " + errorStub.getReason()));
	    }

	    @Override
	    public void onFeedback(String string)
	    {
		if (string != null)
		{
		    DownloadActivity.this.osHandler
			    .sendMessage(DownloadActivity.this.osHandler.obtainMessage(1, (Object) string));
		}
	    }
	};
	this.handlerUnsync = new PhaseHandler()
	{
	    @Override
	    public void onBegan()
	    {
	    }

	    @Override
	    public void onCompleted(ResponseStub responseStub)
	    {
		Intent intent = new Intent((Context) DownloadActivity.this.activity, (Class<?>) ItineraryActivity.class);
		intent.setFlags(268468224);
		DownloadActivity.this.startActivity(intent);
	    }

	    @Override
	    public void onError(ErrorStub errorStub)
	    {
	    }

	    @Override
	    public void onFeedback(String string)
	    {
		if (string != null)
		{
		    DownloadActivity.this.osHandler
			    .sendMessage(DownloadActivity.this.osHandler.obtainMessage(1, (Object) string));
		}
	    }
	};
    }

    
    
    
    //Hacer getters para syncItem y para handler
    
    
    
    
    private void checkSync(List<Sync> listSync)
    {
      SynchronizeRequest.delegates = new HashMap<String, SynchronizeRequestDelegate<String>>();
      
      for(Sync sync : listSync)
      {	  
	switch (sync.getEntity())
	{
	case "products":
	    SynchronizeRequest.registerDelegate(new SynchronizationDelegateProductos(this.activity));
	    break;
	case "conditions":
	    SynchronizeRequest.registerDelegate(new SynchronizationDelegateCondiciones(this.activity));
	    break;
	case "itineraries":
	    SynchronizeRequest.registerDelegate(new SynchronizationDelegateItinerary(this.activity));
	    break;
	case "clients":
	    SynchronizeRequest.registerDelegate(new SynchronizationDelegateClient(this.activity));
	    break;
	}
	
	downloadData(handlerUnsync);
      }
    }
    
    private void downloadData(PhaseHandler phaseHandler) {
        SynchronizeRequest synchronizeRequest = new SynchronizeRequest((Activity)this, false, phaseHandler);
        UserEntity userEntity = ((LynxApplication)this.getApplication()).getUser();
        String string = userEntity == null ? UserPreferences.getUserLogged((Context)this) : userEntity.getId();
        synchronizeRequest.request(null, string);
    }
    

    private void init()
    {
	new AsyncTask<Void, Void, Void>()
	{
	    protected void onPreExecute()
	    {
	    }

	    @Override
	    protected Void doInBackground(Void... params)
	    {
		Log.e((String) "e", (String) "doInBackground start");
		DownloadActivity.this.downloadData(DownloadActivity.this.handlerSync);
		Log.e((String) "e", (String) "doInBackground end");
		return null;
	    }
	    
	    protected void onPostExecute(Void void_)
	    {
		super.onPostExecute((Void) void_);
		Log.e((String) "e", (String) "onPostExecute");
	    }
	}.execute();
    }
    
    private void passActivity()
    {
	TestFlight.passCheckpoint("Synchronization executed.!");

	UserPreferences.setSynchronized(this, true);

	((LynxApplication) getApplication()).reloadUser();

	Intent intent = new Intent(this, ItineraryActivity.class);
	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
	startActivity(intent);
    }
    
    public final Dialog chooseSyncEntities(final Activity activity) 
   	{	    	
   	        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
   	        builder.setTitle((CharSequence)"Sincronizar Datos");
   	        View view = LayoutInflater.from((Context)activity).inflate(R.layout.selection_sync_item, null);
   	        ((TextView)view.findViewById(16908292)).setVisibility(8);
   	        this.listSyncType = (ListView)view.findViewById(2131624190);
   	        this.listSyncType.setOnItemClickListener(this);
   	        ArrayList<Object> arrayList = new ArrayList<Object>();
   	        arrayList.add((Object)"clients");
   	        arrayList.add((Object)"itineraries");
   	        arrayList.add((Object)"products");
   	        arrayList.add((Object)"conditions");
   	        ArrayAdapter<Object> arrayAdapter = new ArrayAdapter<Object>((Context)activity, 17367043, (List<Object>)arrayList);
   	        this.listSyncType.setAdapter((ListAdapter)arrayAdapter);
   	        this.listSyncType.refreshDrawableState();
   	        builder.setView(view);
   	        builder.setPositiveButton((CharSequence)"Seleccionar", (DialogInterface.OnClickListener)new DialogInterface.OnClickListener(){
   	            public void onClick(DialogInterface dialogInterface, int n) {
   	            }
   	        });
   	        final AlertDialog alertDialog = builder.create();
   	        alertDialog.show();
   	        alertDialog.getButton(-1).setOnClickListener((View.OnClickListener)new View.OnClickListener(){
   	            
   	            public void onClick(View view) 
   	            {
   	                alertDialog.cancel();
   	                SynchronizeRequest.delegates = new HashMap<String, SynchronizeRequestDelegate<String>>();
   	                
   	                List<Sync> listSync = new ArrayList<Sync>();
   	                Sync sync = new Sync();
   	                sync.setEntity(syncItem);
   	                listSync.add(sync);
   	                checkSync(listSync);
   	            }
   	        });
   	        return alertDialog;
   	    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_synchronize);
	
	this.activity = this;
        this.setFinishOnTouchOutside(false);
        feedback = (TextView) findViewById(R.id.sync);
        
        try {
            this.first_use = this.getIntent().getBooleanExtra("first_use", false);
            this.unsyncList = (List<Sync>)this.getIntent().getExtras().getSerializable("unsync");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if (this.first_use) {
            SynchronizeRequest.delegates = new HashMap<String, SynchronizeRequestDelegate<String>>();
            SynchronizeRequest.registerDelegate(new SynchronizationDelegateAll(this.activity));
            this.downloadData(this.handler);
            return;
        } else {
            if (this.unsyncList == null) {
                SynchronizeRequest.delegates = new HashMap<String, SynchronizeRequestDelegate<String>>();
                SynchronizeRequest.registerDelegate(new SynchronizationDelegate(this.activity));
                this.init();
                return;
            }
            if (this.unsyncList.size() <= 0) return;
            {
        	this.checkSync(this.unsyncList);
                return;
            }
        }
    }

    public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
	try
	{
	    if ((this.listSyncType != null) && (paramAdapterView.getId() == this.listSyncType.getId()))
	    {
		setSelectableChoiceConfig(paramView);
		this.syncItem = ((String) paramAdapterView.getItemAtPosition(paramInt));
	    }
	} 
	catch (Exception localException)
	{
	    localException.printStackTrace();
	}
    }

    public void setSelectableChoiceConfig(View paramView)
    {
	if ((this.currentSelectedView != null) && (this.currentSelectedView != paramView))
	    unhighlightCurrentRow(this.currentSelectedView);
	this.currentSelectedView = paramView;
	highlightCurrentRow(this.currentSelectedView);
    }

    private void highlightCurrentRow(View paramView)
    {
	if (paramView != null)
	    paramView.setBackgroundColor(getResources().getColor(2131492870));
    }

    private void unhighlightCurrentRow(View paramView)
    {
	if (paramView != null)
	    paramView.setBackgroundColor(0);
    }
    


}

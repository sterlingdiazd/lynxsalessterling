package com.lynxsales.wakeup.activities.adapter;

import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.lynxsales.wakeup.foreing.cache.ImageDownloader;
import com.lynxsales.wakeup.foreing.cache.ImageUtil;
import com.lynxsales.wakeup.R;
import com.lynx.sales.bundle.entities.ProductEntity;
import com.lynx.sales.bundle.http.request.ServiceLocator;
import com.lynx.sales.bundle.http.request.ServiceName;
import com.lynx.sales.bundle.http.request.ServiceStub;

/**
 * @author jansel rodriguez
 *
 */
@SuppressLint("UseSparseArrays")
public class ProductBillAdapter extends ArrayAdapter< ProductEntity >{

	private int resource;
	private LayoutInflater inflater;
	private List< ProductEntity > products;
		
	private SparseBooleanArray selectedItemsIds;
	
	
	public ProductBillAdapter(Context context, int resource,List< ProductEntity > entries ) 
	{
		super(context, resource,entries );		
		this.resource = resource;
		this.products = entries;
		
		selectedItemsIds = new SparseBooleanArray();
	}
	
	@SuppressLint("DefaultLocale")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		if( inflater == null )
			inflater = ( LayoutInflater ) getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		
		ProductEntity product = products.get( position );
		
		if( convertView == null )
			convertView = inflater.inflate( resource , parent,false );

        	convertView.setBackgroundColor( selectedItemsIds.get(position) ? 0x9934B5E4 :  getContext().getResources().getColor( R.color.gray_light_d1));
        
        	ViewHolder holder = ( ViewHolder ) convertView.getTag();
		
		if( holder == null )
		{	
			holder = new ViewHolder();
			holder.checkbox = ( CheckBox ) convertView.findViewById( android.R.id.checkbox );
			
			holder.id		 = ( TextView )  convertView.findViewById( R.id.name );
			holder.name		 = ( TextView )  convertView.findViewById( R.id.idClient );
			holder.price	 = ( TextView )  convertView.findViewById( R.id.product_price );
			holder.discount	 = ( TextView )  convertView.findViewById( R.id.textViewDiscount);
			holder.offer	 = ( TextView )  convertView.findViewById( R.id.offer );	
			holder.stock	 = ( TextView )  convertView.findViewById( R.id.stock );
			holder.thumb 	 = ( ImageView ) convertView.findViewById( android.R.id.icon );
			holder.amount	 = ( TextView )  convertView.findViewById( R.id.amount_added );
			
			convertView.setTag( holder );
		}
		
		holder.checkbox.setVisibility( View.GONE );
				
		holder.id.setTag( product.getId() );
		holder.id.setText( product.getMaterial() );
		holder.name.setText( product.getDescription().toLowerCase( Locale.getDefault() ) );
		holder.price.setText( "$"+product.getPrice() /* + " " +product.getBaseUnit().toLowerCase( Locale.getDefault() )*/ );
		
		String stockLabel = (convertView.getContext().getResources().getString(R.string.stock) == null) ? "" : convertView.getContext().getResources().getString(R.string.stock);
		String stock = (product.getStock() != null) ? product.getStock() : "";
		String salesUnit = null;
		
		if(product.getSalesUnits() != null)
		{
			if(product.getSalesUnits().get(0) != null)
			{
				salesUnit = (product.getSalesUnit() != null) ? product.getSalesUnit() : "";
			} else {
				Log.e("ERROR", "product.getSalesUnits().get(0) == NULL");
			}
		} else {
			salesUnit = product.getSelectedSalesUnit().getMeinh();
			Log.e("ERROR", "product.getSalesUnits() == NULL");
		}
		
		
		holder.stock.setText(stockLabel + " " + stock +" "+ salesUnit.toLowerCase( Locale.getDefault() ));
		
		String discString = product.getDiscount();
		String discount = "";
		if(discString != null && !discString.equalsIgnoreCase(""))
		{
			int start = product.getDiscount().indexOf("-") + 1;
			int end = product.getDiscount().length();
			String doubleValue = null;
			try 
			{
				doubleValue = discString.substring(start, end);
				double disc = (Double.parseDouble(doubleValue) / 10);
				discount = (product.getDiscount() != null) ? "Descuento: "+ disc +" %" : "" ; 
				holder.discount.setText(discount);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		} 
		
		
		/*
		ServiceStub service = ServiceLocator.lookup( ServiceName.PRODUCT_THUMB );		
		String imageName = service.getUrl()+service.resolveParams(product.getId().concat(".jpg"));
		
		Bitmap cachedImage = ImageUtil.getBitmapFromFile( imageName );
		if (cachedImage != null){
			holder.thumb. setImageBitmap(cachedImage);
		}else {
				qUITAR 
			ImageDownloader imageDown = new ImageDownloader();
			imageDown.download( imageName, holder.thumb, getContext());
			
		}		
		*/
		if( !TextUtils.isEmpty( product.getOffer() ) ){
			holder.offer.setText( product.getOffer().toUpperCase() );
		}else{
			holder.offer.setVisibility( View.GONE );
		}
		
		if( !TextUtils.isEmpty( product.getAmount() ) ){
			holder.amount.setVisibility( View.VISIBLE );
			holder.amount.setText( product.getAmount()+ " "  + product.getSelectedSalesUnit().getMeinh() + " " + convertView.getContext().getResources().getString(R.string.units_add) );
		}else{
			holder.amount.setVisibility( View.GONE );
		}
		
		return convertView;
	}

	
	
    public void toggleSelection(int position) {
        selectView(position, !selectedItemsIds.get(position));
    }
 
    
    public void removeSelection() {
    	selectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }
 
    
    public void selectView(int position, boolean value) 
    {
        if (value)
        	selectedItemsIds.put(position, value);
        else
        	selectedItemsIds.delete(position);
 
        notifyDataSetChanged();
    }
    
    
    
    public int getSelectedCount() {
        return selectedItemsIds.size();
    }
 
    
    
    public SparseBooleanArray getSelectedIds() {
        return selectedItemsIds;
    }
    
    
	private class ViewHolder{
		ImageView thumb;
		TextView name;
		CheckBox checkbox;
		TextView id;
		TextView stock;		
		TextView price;
		TextView discount;
		TextView offer;		
		TextView amount;
	}
}

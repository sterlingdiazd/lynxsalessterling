package com.lynxsales.wakeup.activities.adapter;

import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.internal.ar;
import com.lynxsales.wakeup.R;
import com.lynx.sales.bundle.dto.model.ClientDataModel;
import com.lynx.sales.bundle.entities.Archive;
import com.lynx.sales.bundle.entities.ClientEntity;

public class ArchiveAdapter extends BaseAdapter{

	private Context context ;
	private List<Archive> archives;
	private TextView archiveType;
	private TextView textViewIdArchive;
	private TextView textViewDateArchive;
	private TextView textViewOrderType;
	private TextView textViewArchiveStatus;
	private TextView textViewArchiveClientName;
	private TextView textViewIdClient;
	private ClientEntity client;
	private boolean isFromClient;
	
	public ArchiveAdapter(Context context, List<Archive> archives, boolean isFromClient, ClientEntity client) {
		super();
		this.context = context;
		this.archives = archives;
		this.isFromClient = isFromClient;
		this.client = client;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {		
		
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		Archive archive = archives.get( position );
		
		View view = null;
		if(isFromClient)
		{
			view = layoutInflater.inflate(R.layout.adapter_archive, null);
			textViewIdArchive 						= (TextView) view.findViewById( R.id.textViewIdArchive);
			textViewIdArchive.setText( archive.getId() + "" );				
		} 
		else 
		{
			view = layoutInflater.inflate(R.layout.adapter_archive_from_client, null);
			textViewIdClient 						= (TextView) view.findViewById( R.id.textViewIdClient);	
			textViewArchiveClientName 						= (TextView) view.findViewById( R.id.textViewArchiveClientName);
					
			ClientEntity client = ClientDataModel.getClientByExternalID(context, archive.getOwner());
			if(client.getAddress1() == null)
				client = ClientDataModel.getClient(context, archive.getOwner());
			String id = ( client.getStatus().equalsIgnoreCase("CREADO") ) ? client.getId() : client.getKunnr();
			textViewIdClient.setText(id);
			textViewArchiveClientName.setText(client.getName());
		}
		
		textViewDateArchive 		= (TextView) view.findViewById( R.id.textViewDateArchive);
		textViewOrderType	= (TextView) view.findViewById( R.id.textViewArchiveOrderType);
		textViewArchiveStatus	= (TextView) view.findViewById( R.id.textViewArchiveStatus);
			
		textViewOrderType.setText( archive.getArchiveType());
		textViewDateArchive.setText(archive.getDate());
		textViewArchiveStatus.setText("Archived");
		
		return view;
	}

	@Override
	public int getCount() {
		return archives.size();
	}


	@Override
	public Object getItem(int position) {
		return archives.get(position);
	}


	@Override
	public long getItemId(int position) {
		return archives.get(position).getId();
	}
	
}

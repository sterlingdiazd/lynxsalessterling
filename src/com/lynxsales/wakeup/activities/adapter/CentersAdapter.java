package com.lynxsales.wakeup.activities.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lynx.sales.bundle.entities.CenterEntity;
import com.lynxsales.wakeup.R;

public class CentersAdapter extends BaseAdapter {

	private Context context;
	private List<CenterEntity> centers;
	private TextView textViewCenterName;
	private TextView textViewCenter;
	
	public CentersAdapter(Context context, List<CenterEntity> centers2) {
		super();
		this.context = context;
		this.centers = centers2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view =layoutInflater.inflate(R.layout.adapter_center, null);
		textViewCenterName = (TextView) view.findViewById(R.id.textViewCenterName);
		textViewCenter = (TextView) view.findViewById(R.id.textViewCenter);
		
		if(centers != null)
		{
			CenterEntity center = centers.get(position);
			textViewCenterName.setText(center.getDescription() + "");
			textViewCenter.setText(center.getName());
		}
		
		return view;
	}

	@Override
	public int getCount() {
		return centers.size();
	}

	@Override
	public Object getItem(int position) {
		return centers.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

}

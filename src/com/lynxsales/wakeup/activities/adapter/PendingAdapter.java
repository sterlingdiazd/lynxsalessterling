package com.lynxsales.wakeup.activities.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.widget.TextView;
import com.lynxsales.wakeup.utils.CommonUtils;
import com.lynxsales.wakeup.R;

public class PendingAdapter extends ArrayAdapter<ScheduleJob>{

	private Context context ;
	private int resource;
	private LayoutInflater inflater;
	private List<ScheduleJob> pendings;
	
	
	public PendingAdapter(Context context, int resource,List<ScheduleJob> objects) {
		super(context, resource, objects);
		this.context = context;
		this.pendings = objects;
		this.resource=  resource;
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{	
		if( null == inflater )
		{
			inflater = ( LayoutInflater ) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );			
		}
		
		ViewHolder holder = null;
		if( null == convertView )
		{
			convertView = inflater.inflate( resource, parent, false );
			holder = new ViewHolder();
			holder.textViewPendingIdClient 			= (TextView) convertView.findViewById( R.id.textViewPendingIdClient);
			holder.textViewPendingClientName 		= (TextView) convertView.findViewById( R.id.textViewPendingClientName );
			holder.textViewPendingDate				= (TextView) convertView.findViewById( R.id.textViewPendingDate );
			holder.textViewPendingOrderType			= (TextView) convertView.findViewById( R.id.textViewPendingOrderType );
			holder.textViewPendingStatus			= (TextView) convertView.findViewById( R.id.textViewPendingStatus);
			convertView.setTag( holder );			
		}else{
			holder = ( ViewHolder ) convertView.getTag(); 
		}
		
		
		
		ScheduleJob job = pendings.get( position );
		ClientEntity client = CommonUtils.getClientByQuery(context, job.getQuery());
		String status = job.getStatus().toString();
		
		holder.textViewPendingIdClient.setText( ( client.getStatus().equalsIgnoreCase("CREADO") ) ? client.getId() : client.getKunnr() );
		holder.textViewPendingClientName.setText( client.getName() );
		holder.textViewPendingDate.setText( job.getDate() );
		holder.textViewPendingOrderType.setText(job.getType().toString());
		holder.textViewPendingStatus.setText(status);
		
		if(status.equalsIgnoreCase(ScheduleJob.Status.PENDING.toString()))
		{
			
			holder.textViewPendingStatus.setTextColor(convertView.getContext().getResources().getColor( R.color.status_pending) );
		} 
		else if(status.equalsIgnoreCase(ScheduleJob.Status.PROCCESED.toString()))
		{
			holder.textViewPendingStatus.setTextColor(convertView.getContext().getResources().getColor(R.color.status_processed));
		} 
		else if(status.equalsIgnoreCase(ScheduleJob.Status.REJECTED.toString()))
		{
			holder.textViewPendingStatus.setTextColor(convertView.getContext().getResources().getColor(R.color.status_rejected));
		}
		else if(status.equalsIgnoreCase(ScheduleJob.Status.WARNING.toString()))
		{
			holder.textViewPendingStatus.setTextColor(convertView.getContext().getResources().getColor(R.color.yellow));
		}
		return convertView;
	}
	
	
	private class ViewHolder{
		
		/*package*/ TextView textViewPendingIdClient;
		/*package*/ TextView textViewPendingClientName;
		/*package*/ TextView textViewPendingDate;
		/*package*/ TextView textViewPendingOrderType;
		/*package*/ TextView textViewPendingStatus;		
	}
	
}

package com.lynxsales.wakeup.activities.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lynx.sales.bundle.entities.CenterEntity;
import com.lynx.sales.bundle.entities.DocTypeEntity;
import com.lynxsales.wakeup.R;

public class DocTypeAdapter extends BaseAdapter {

	private Context context;
	private List<DocTypeEntity> docs;
	private TextView textViewDoc;
	private TextView textViewDocDesc;
	
	public DocTypeAdapter(Context context, List<DocTypeEntity> doc) {
		super();
		this.context = context;
		this.docs = doc;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		DocTypeEntity doc = docs.get(position);

		View view = null;

		view = layoutInflater.inflate(R.layout.adapter_doctypes, null);
		textViewDoc = (TextView) view.findViewById(R.id.textViewDoc);
		textViewDocDesc = (TextView) view.findViewById(R.id.textViewDocDesc);
		textViewDoc.setText(doc.getDocType()+ "");
		textViewDocDesc.setText(doc.getDescription() + "");
		return view;
	}

	@Override
	public int getCount() {
		return docs.size();
	}

	@Override
	public Object getItem(int position) {
		return docs.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

}

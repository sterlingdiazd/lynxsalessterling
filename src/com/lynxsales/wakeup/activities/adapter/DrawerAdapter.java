package com.lynxsales.wakeup.activities.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lynxsales.wakeup.activities.dto.DrawerEntry;
import com.lynxsales.wakeup.R;

public class DrawerAdapter extends ArrayAdapter<  DrawerEntry > {

	private int resource;
	private LayoutInflater inflater;
	private List< DrawerEntry > entries;
	
	public DrawerAdapter(Context context, int resource,List< DrawerEntry > entries ) {
		super(context, resource,entries );
		
		this.resource = resource;
		this.entries = entries;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if( inflater == null )				
			inflater = ( LayoutInflater ) getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		
		TextView title = null;
		ImageView icon = null;		
		TextView  budget = null;
		ViewHolder holder = null;
				
		if( convertView == null ){
			convertView = inflater.inflate( resource, parent,false );
			
			title = ( TextView )  convertView.findViewById( R.id.title );
			icon  = ( ImageView ) convertView.findViewById( R.id.icon );
			budget = ( TextView ) convertView.findViewById( R.id.budget_count );
			
			holder = new ViewHolder(icon, title,budget );
			convertView.setTag(holder);			

		}else{
			holder = ( ViewHolder ) convertView.getTag();
			title  = holder.title;
			icon   = holder.icon;
			budget = holder.budget;			
		}
		
		DrawerEntry entry = entries.get( position );
		
		title.setText( entry.getTitle() );
		icon.setImageDrawable( getContext().getResources().getDrawable( entry.getIcon() ) );
		
		if( entry.hasBudget() ){
			budget.setVisibility( View.VISIBLE );
			budget.setText( String.valueOf( entry.getBudgetCount() ) );
		}
			
		return convertView;
	}
	
	
	
	
	private class ViewHolder{
		public ImageView icon;
		public TextView  title;
		public TextView  budget;		
		
		public ViewHolder( ImageView icon, TextView title,TextView budget ){
			this.icon  = icon;
			this.title = title;
			this.budget = budget;
		}
	}
		
	
}





























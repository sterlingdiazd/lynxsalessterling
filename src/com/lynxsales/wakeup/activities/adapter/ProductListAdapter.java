package com.lynxsales.wakeup.activities.adapter;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.dto.model.ProductDataModel;
import com.lynx.sales.bundle.dto.model.UnitsDataModel;
import com.lynx.sales.bundle.entities.CenterEntity;
import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynx.sales.bundle.entities.ProductEntity;
import com.lynx.sales.bundle.entities.UnitEntity;
import com.lynxsales.wakeup.utils.AlertDialogManager;
import com.lynxsales.wakeup.R;
import com.lynxsales.wakeup.activities.bprocess.ProductListActivity;
import com.testflightapp.lib.TestFlight;

/**
 * @author jansel rodriguez
 *
 */
@SuppressLint("UseSparseArrays")
public class ProductListAdapter extends ArrayAdapter< ProductEntity >{

	private static final int TAG_KEY_ID_1 = R.id.tag_key_id_1;
	private static final int TAG_KEY_ID_2 = R.id.tag_key_id_2;
	
	private int resource;
	private LayoutInflater inflater;
	private List< ProductEntity > products;
	private List< ProductEntity > cachedProducts;
	private Activity activity;	
	private HashMap<String, ProductEntry> selections;
	private ClientEntity client;
	private CenterEntity center;
	private SharedPreferences prefs;
	private ArrayList<Integer> sequences = new ArrayList<Integer>();
	
	public ProductListAdapter(Activity activity, int resource,List< ProductEntity > entries, ClientEntity client, CenterEntity center ) 
	{
		super(activity, resource,entries );	
		this.activity = activity;
		this.resource = resource;
		this.products = entries;	
		this.client = client;
		this.center = center;
		
		cachedProducts = new ArrayList<ProductEntity>();
		cachedProducts.addAll( products );		
		selections = new HashMap<String, ProductEntry>();
		prefs = PreferenceManager.getDefaultSharedPreferences(activity);
	}
	
	@SuppressLint("DefaultLocale")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {		
		
		if( inflater == null )
			inflater = ( LayoutInflater ) getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		
		final ProductEntity product = products.get( position );
		
		if( convertView == null )
			convertView = inflater.inflate( resource , parent,false );
		
		ViewHolder holder = ( ViewHolder ) convertView.getTag();
		
		if( holder == null ){
			
			holder = new ViewHolder();			
			
			holder.checkbox  = ( CheckBox ) convertView.findViewById( android.R.id.checkbox );
			holder.amount 	 = ( EditText )  convertView.findViewById( R.id.quantity );
			
			final EditText edit = holder.amount;
			holder.checkbox.setOnCheckedChangeListener( new OnCheckedChangeListener() {				
				@Override
				public void onCheckedChanged(CompoundButton button, boolean checked ) {					


					String entryId = (String) button.getTag( TAG_KEY_ID_1 );
				    /*
					int id = Integer.valueOf(entryId);
					ArrayList<ProductEntity> ccpro = (ArrayList<ProductEntity>) cachedProducts;
					//ProductEntity mProduct = ccpro.get(id-1);
					ProductEntity product = null;
					for(int x = 0; x < ccpro.size();x++)
					{
						if(ccpro.get(x).getMaterial().equalsIgnoreCase(entryId))
						{
							product = ccpro.get(x);
							break;
						}
					}
					*/
					ProductEntity product = null;
					
					for(int x = 0; x < products.size(); x++)
					{
					    if(products.get(x).getMaterial().equalsIgnoreCase(entryId))
						{
							product = products.get(x);
							break;
						}
					}
					
					
					//ProductEntity product = ProductDataModel.getProductsByIdOrDescription(context, centerEntity, clientEntity, productIdOrName)(activity, center.getName(), entryId);
					if(product != null)
					{
						Double stock = (product.getStock() != null ) ? Double.valueOf( product.getStock() ) : 0;
						Double price = (product.getPrice() != null ) ? Double.valueOf( product.getPrice() ) : 0;
						
						if( (stock.intValue() > 0  && price != null) && (price.intValue() > 0  && price != null)) 
						{
							ProductEntry entry = null;
							
							if( !selections.containsKey(entryId) && checked ){
//								entry = new ProductEntry(index,checked, "");
								entry = new ProductEntry(checked, "");
								selections.put( entryId,entry );						
								edit.setText( "" );						
							}else if(!checked){
								selections.remove( entryId );				
							}
							edit.setVisibility( selections.get(entryId) != null && selections.get(entryId).selected ?  View.VISIBLE : View.GONE );		
						} 
						else 
						{
							if(!prefs.getBoolean("no_query", true))
							{
								new AlertDialogManager().showAlertDialog(activity, "Producto no disponible", "No hay stock o el precio es cero", false);
								button.setChecked(false);
							}
							
						}
					}
					
					
								
				}				
			});      

			
			holder.amount.addTextChangedListener( new TextWatcher() {				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {					
				}			
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,int after) {					
				}				
				@Override
				public void afterTextChanged(Editable s) {
					
					String entryId = (String) edit.getTag(TAG_KEY_ID_1);
					
					ProductEntry productEntry = selections.get( entryId );
					if( productEntry != null )
					{
						/*
						ProductEntity searchProduct = new ProductEntity();
						searchProduct.setId( entryId );
						
						int index = cachedProducts.indexOf( searchProduct );
						
						if( 0<=index )
						{
						*/
							ProductEntity mProduct = /*cachedProducts.get(index);*/ ProductDataModel.getProductsByCenterAndMaterial(activity, center, entryId);
							
							if( edit.getText().toString().length() > 0 ){	
								
								double quantity = 0;
								try {
									quantity = Double.valueOf( edit.getText().toString() );	
								} catch(NumberFormatException n)
								{
									new AlertDialogManager().showAlertDialog(activity, "Cantidad Incorrecta", n.getLocalizedMessage() , false);
									n.printStackTrace();
									quantity = 0;
								}
																			
							    int stock = 0;						    						    
							    try{		
							    	Log.e("***********", "Accessing to product "+mProduct.getMaterial() );
							    	NumberFormat format = NumberFormat.getInstance( Locale.US );						    	
							    	stock = format.parse( mProduct.getStock() ).intValue();
							    }catch( Exception ex ){
							    	TestFlight.log( "Error parsing string-to-number: "+ex.getMessage() );
							    	Log.e("*******", "Error parsiong stock", ex);
							    	ex.printStackTrace();
							    }
							    if( stock < quantity )
							    {
							    	String mesg = getContext().getString( R.string.not_accepted_quantity );
							    	new AlertDialogManager().showAlertDialog(activity, "Cantidad Incorrecta", mesg, false);
							    	//Toast.makeText( getContext(), mesg+" "+stock, Toast.LENGTH_LONG ).show();
							    	return;
							    }
							}
							selections.get( entryId ).amount = edit.getText().toString();
					//}
					}
				}
			});		
			
			holder.id		 = ( TextView )  convertView.findViewById( R.id.name );
			holder.name		 = ( TextView )  convertView.findViewById( R.id.idClient );			
 			holder.price	 = ( TextView )  convertView.findViewById( R.id.product_price );
 			holder.discount	 = ( TextView )  convertView.findViewById( R.id.textViewDiscount);
			holder.offer	 = ( TextView )  convertView.findViewById( R.id.offer );		
			holder.stock	 = ( TextView )  convertView.findViewById( R.id.stock );
			holder.thumb 	 = ( ImageView ) convertView.findViewById( android.R.id.icon );
			
			convertView.setTag( holder );
		}
		
		
		//holder.checkbox.setTag( position );
		holder.checkbox.setTag(TAG_KEY_ID_1, product.getMaterial() );
		//holder.amount.setTag( position );
		//holder.amount.setTag( TAG_KEY_ID_2, product.getId() );
		holder.amount.setTag(TAG_KEY_ID_1, product.getMaterial() );
		if( selections.get( product.getMaterial() ) != null ){	
			holder.amount.setText( selections.get( product.getMaterial() ).amount );			
		}		
		
		boolean state = selections.containsKey( product.getMaterial() ) ? selections.get(product.getMaterial()).selected : false;
		
		holder.checkbox.setChecked( state );
		holder.amount.setVisibility( state ? View.VISIBLE : View.GONE );
		
		holder.id.setTag( product.getMaterial() );
		holder.id.setText( product.getMaterial() );
		holder.name.setText( product.getDescription().toLowerCase( Locale.getDefault() ) );
		holder.price.setText( "$"+product.getPrice() );
		
		String discString = product.getDiscount();
		String discount = "";
		if(discString != null && !discString.equalsIgnoreCase(""))
		{
			int start = product.getDiscount().indexOf("-") + 1;
			int end = product.getDiscount().length();
			String doubleValue = null;
			try 
			{
				doubleValue = discString.substring(start, end);
				double disc = (Double.parseDouble(doubleValue) / 10);
				discount = (product.getDiscount() != null) ? "Descuento: "+ disc +" %" : "" ; 
				holder.discount.setText(discount);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		} 
		
		String stockLabel = (convertView.getContext().getResources().getString(R.string.stock) == null) ? "" : convertView.getContext().getResources().getString(R.string.stock);
		String stock = (product.getStock() != null) ? product.getStock() : "";
		String salesUnit = (product.getSalesUnit() != null) ? product.getSalesUnit() : "";
		holder.stock.setText(stockLabel + " " + stock +" "+ salesUnit.toLowerCase( Locale.getDefault() ));
		product.setSelectedSalesUnit(new UnitEntity(product.getSalesUnit()));
		//Poner primera ocpion de los materiales
		
		/*
		ServiceStub service = ServiceLocator.lookup( ServiceName.PRODUCT_THUMB );		
		String imageName = service.getUrl()+service.resolveParams(product.getMaterial().concat(".jpg"));
		
		
		Bitmap cachedImage = ImageUtil.getBitmapFromFile( imageName );
		if (cachedImage != null){
			holder.thumb. setImageBitmap(cachedImage);
		}else {
			ImageDownloader imageDown = new ImageDownloader();
			imageDown.download( imageName, holder.thumb, getContext());
		}
		*/
		
		if( !TextUtils.isEmpty( product.getOffer() ) ){
			holder.offer.setVisibility( View.VISIBLE );
			holder.offer.setText( product.getOffer().toUpperCase() );
		}else{
			holder.offer.setVisibility( View.GONE );
		}
		
		return convertView;
	}

	
	
    @SuppressLint("DefaultLocale")
    @Override
    public Filter getFilter()
    {
	return new Filter()
	{
	    @SuppressWarnings("unchecked")
	    @Override
	    protected void publishResults(CharSequence constraint, FilterResults results)
	    {
		List<ProductEntity> values = (List<ProductEntity>) results.values;
		if (values != null && values.size() > 0)
		{
		    
		    // products = cachedProducts;
		    products.clear();
		    products.addAll(values);
		    //notifyDataSetInvalidated();
		    notifyDataSetChanged();
		    /*
		    List<ProductEntity> productsReady = new ArrayList<ProductEntity>();
		    List<ProductEntity> productsWithData = cachedProducts;
		    ProductEntity product = null;

		    for (int indexValue = 0; indexValue < values.size(); indexValue++)
		    {
			for (int indexProduct = 0; indexProduct < productsWithData.size(); indexProduct++)
			{
			    String selectedMaterialID = values.get(indexValue).getMaterial();
			    String productMaterialID = productsWithData.get(indexProduct).getMaterial();
			    if (selectedMaterialID.equalsIgnoreCase(productMaterialID))
			    {
				product = productsWithData.get(indexProduct);
				productsReady.add(product);
				product = null;
				break;
			    }
			}
		    }

		    if (null == products)
			products = productsWithData;
		    else
		    {
			if (values != null)
			{
			    products.clear();
			    if (productsReady.size() > 0)
				products.addAll(productsReady);
			}

		    }
		    
		    Log.e("************", "After constraint: " + constraint + ", products.size(): " + products.size());
		    notifyDataSetInvalidated();
		    notifyDataSetChanged();
		     */
		}
	    }

	    @Override
	    protected FilterResults performFiltering(CharSequence constraint)
	    {

		FilterResults results = new FilterResults();
		List<ProductEntity> filtered = new ArrayList<ProductEntity>();

		List<ProductEntity> pro = ProductDataModel.getProductsByIdOrDescription(activity, center, client,
			constraint.toString().toLowerCase());

		if (constraint.length() == 0 || TextUtils.isEmpty(constraint.toString().trim()))
		{
		    Log.e("**************", "Adding all products");
		    List<ProductEntity> cacproducts = cachedProducts;
		    filtered.addAll(cacproducts);
		} 
		else
		{
		    for (int x = 0; x < pro.size(); x++)
		    {
			ProductEntity productFull = calculatePrice(pro.get(x));
			pro.get(x).setPrice(productFull.getPrice());
			pro.get(x).setOffer(productFull.getOffer());
			List<UnitEntity> salesUnits = UnitsDataModel.getUnitsByProduct(activity,
				productFull.getMaterial());
			pro.get(x).setSalesUnits(salesUnits);
			if (salesUnits.size() > 0)
			    pro.get(x).setSelectedSalesUnit(salesUnits.get(0));
			pro.get(x).setDiscount(productFull.getDiscount());
		    }
		    filtered.addAll(pro);
		}

		results.count = filtered.size();
		results.values = filtered;

		return results;
	    }
	};
    }
	
	

	    private ProductEntity calculatePrice(ProductEntity product)
	    {
		SQLiteDatabase db = Database.getReadableDatabase(activity);
		Cursor cursorConditionType = null;

		try
		{
		    cursorConditionType = db.rawQuery(
			    "select kschl from conditions where vkorg = '" + client.getOrganizacion() + "' "
				    + "and vtweg = '" + client.getCanal() + "' group by kschl order by kschl desc",
			    null);

		    if (cursorConditionType != null)
		    {
			for (cursorConditionType.moveToFirst(); !cursorConditionType.isAfterLast(); cursorConditionType
				.moveToNext())
			{
			    String conditionType = cursorConditionType
				    .getString(cursorConditionType.getColumnIndexOrThrow("kschl"));
			    if (conditionType.equalsIgnoreCase("PR00"))
			    {
				Cursor cursorSequence = db.rawQuery(
					"select sequence from conditions where vkorg = '" + client.getOrganizacion()
						+ "' " + "and vtweg = '" + client.getCanal() + "' and kschl = '"
						+ conditionType + "' group by sequence order by sequence asc",
					null);

				try
				{
				    if (cursorSequence != null)
				    {
					sequences.clear();
					for (cursorSequence.moveToFirst(); !cursorSequence.isAfterLast(); cursorSequence
						.moveToNext())
					{
					    String sequence = cursorSequence
						    .getString(cursorSequence.getColumnIndexOrThrow("sequence"));
					    sequences.add(Integer.valueOf(sequence));
					}
				    }
				} catch (Exception e)
				{
				    e.printStackTrace();
				} finally
				{
				    if (cursorSequence != null)
				    {
					cursorSequence.close();
				    }
				}

				for (Integer currentPriceSequence : sequences)
				{
				    if (currentPriceSequence == 1)
				    {
					Cursor cursorClienteMaterial = db
						.rawQuery("select kbetr from conditions where vkorg = '"
							+ client.getOrganizacion() + "' " + "and vtweg = '"
							+ client.getCanal() + "' and kschl = '" + conditionType
							+ "' and sequence = '" + currentPriceSequence + "' "
							+ "and kunnr like '%" + client.getKunnr()
							+ "%' and matnr like '%" + product.getMaterial() + "%'"
							+ " order by sequence asc", null);

					try
					{
					    if (cursorClienteMaterial != null)
					    {
						for (cursorClienteMaterial.moveToFirst(); !cursorClienteMaterial
							.isAfterLast(); cursorClienteMaterial.moveToNext())
						{
						    String kbetr = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("kbetr"));
						    if (product.getPrice() == null)
						    {
							product.setPrice(kbetr);
							break;
						    } else
						    {
							break;
						    }
						}
					    }
					} catch (Exception e)
					{
					    e.printStackTrace();
					} finally
					{
					    if (cursorClienteMaterial != null)
					    {
						cursorClienteMaterial.close();
					    }
					}

				    } else if (currentPriceSequence == 2)
				    {
					Cursor cursorSectorRamoMaterial = db
						.rawQuery("select kbetr from conditions where vkorg = '"
							+ client.getOrganizacion() + "' " + "and vtweg = '"
							+ client.getCanal() + "' and kschl = '" + conditionType
							+ "' and sequence = '" + currentPriceSequence + "' "
							+ "and spart like '%" + client.getSector()
							+ "%' and brsch like '%" + client.getRamo()
							+ "%'  and matnr like '%" + product.getMaterial() + "%'"
							+ " order by sequence asc", null);

					try
					{
					    if (cursorSectorRamoMaterial != null)
					    {
						for (cursorSectorRamoMaterial.moveToFirst(); !cursorSectorRamoMaterial
							.isAfterLast(); cursorSectorRamoMaterial.moveToNext())
						{
						    String kbetr = cursorSectorRamoMaterial.getString(
							    cursorSectorRamoMaterial.getColumnIndexOrThrow("kbetr"));
						    cursorSectorRamoMaterial.close();
						    if (product.getPrice() == null)
						    {
							product.setPrice(kbetr);
							break;
						    } else
						    {
							break;
						    }
						}
					    }
					} catch (Exception e)
					{
					    e.printStackTrace();
					} finally
					{
					    if (cursorSectorRamoMaterial != null)
					    {
						cursorSectorRamoMaterial.close();
					    }
					}
				    } else if (currentPriceSequence == 3)
				    {
					Cursor cursorMaterial = db
						.rawQuery("select kbetr from conditions where vkorg = '"
							+ client.getOrganizacion() + "' " + "and vtweg = '"
							+ client.getCanal() + "' and kschl = '" + conditionType
							+ "' and sequence = '" + currentPriceSequence + "' "
							+ "and matnr like '%" + product.getMaterial() + "%'"
							+ " order by sequence asc", null);

					try
					{
					    if (cursorMaterial != null)
					    {
						for (cursorMaterial
							.moveToFirst(); !cursorMaterial.isAfterLast(); cursorMaterial
								.moveToNext())
						{
						    String kbetr = cursorMaterial
							    .getString(cursorMaterial.getColumnIndexOrThrow("kbetr"));
						    cursorMaterial.close();
						    if (product.getPrice() == null)
						    {
							product.setPrice(kbetr);
							break;
						    } else
						    {
							break;
						    }
						}
					    }
					} catch (Exception e)
					{
					    e.printStackTrace();
					} finally
					{
					    if (cursorMaterial != null)
					    {
						cursorMaterial.close();
					    }
					}

				    }

				}

			    } else if (conditionType.equalsIgnoreCase("NA00"))
			    {

				Cursor cursorSequence = db.rawQuery(
					"select sequence from conditions where vkorg = '" + client.getOrganizacion()
						+ "' " + "and vtweg = '" + client.getCanal() + "' and kschl = '"
						+ conditionType + "' group by sequence order by sequence asc",
					null);

				try
				{
				    if (cursorSequence != null)
				    {
					sequences.clear();
					for (cursorSequence.moveToFirst(); !cursorSequence.isAfterLast(); cursorSequence
						.moveToNext())
					{
					    String sequence = cursorSequence
						    .getString(cursorSequence.getColumnIndexOrThrow("sequence"));
					    sequences.add(Integer.valueOf(sequence));
					}
				    }
				} catch (Exception e)
				{
				    e.printStackTrace();
				} finally
				{
				    if (cursorSequence != null)
				    {
					cursorSequence.close();
				    }
				}

				for (Integer currentBonusSequence : sequences)
				{

				    if (currentBonusSequence == 1)
				    {
					Cursor cursorClienteMaterial = db
						.rawQuery(
							"select knrmm, knrnm, knrme, knrzm, knrez, knrmat from conditions"
								+ " where kschl = '" + conditionType + "'"
								+ " and vkorg = '" + client.getOrganizacion() + "'"
								+ " and vtweg = '" + client.getCanal() + "'"
								+ " and sequence = '" + currentBonusSequence + "'"
								+ " and kunnr like '%" + client.getKunnr() + "%'"
								+ " and matnr like '%" + product.getMaterial() + "%'",
							null);

					try
					{
					    if (cursorClienteMaterial != null)
					    {
						for (cursorClienteMaterial.moveToFirst(); !cursorClienteMaterial
							.isAfterLast(); cursorClienteMaterial.moveToNext())
						{
						    String minimunQuantity = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrmm"));
						    String bonificableQuantity = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrnm"));
						    String unitMeasureQuantity = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrme"));
						    String bonificatedQuantity = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrzm"));
						    String unitMeasureBonificated = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrez"));
						    String matAdcBonificable = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrmat"));
						    cursorClienteMaterial.close();

						    String offer = "De: " + minimunQuantity + ": " + bonificableQuantity
							    + " " + unitMeasureQuantity + " + " + bonificatedQuantity
							    + " " + unitMeasureBonificated;

						    if (matAdcBonificable.length() > 0)
							product.setKnrmat(matAdcBonificable);

						    if (product.getOffer() == null)
						    {
							product.setOffer(offer.trim());
							break;
						    } else
						    {
							break;
						    }
						}
					    }
					} catch (Exception e)
					{
					    e.printStackTrace();
					} finally
					{
					    if (cursorClienteMaterial != null)
					    {
						cursorClienteMaterial.close();
					    }
					}

				    } else if (currentBonusSequence == 2)
				    {
					Cursor cursorSectorGrupoClienteMaterial = db
						.rawQuery(
							"select knrmm, knrnm, knrme, knrzm, knrez, knrmat from conditions"
								+ " where kschl = '" + conditionType + "'"
								+ " and vkorg = '" + client.getOrganizacion() + "'"
								+ " and vtweg = '" + client.getCanal() + "'"
								+ " and sequence = '" + currentBonusSequence + "'"
								+ " and spart like '%" + client.getSector() + "%'"
								+ " and kdgrp like '%" + client.getKdgrp() + "%'"
								+ " and matnr like '%" + product.getMaterial() + "%'",
							null);

					try
					{
					    if (cursorSectorGrupoClienteMaterial != null)
					    {
						for (cursorSectorGrupoClienteMaterial
							.moveToFirst(); !cursorSectorGrupoClienteMaterial
								.isAfterLast(); cursorSectorGrupoClienteMaterial
									.moveToNext())
						{
						    String minimunQuantity = cursorSectorGrupoClienteMaterial
							    .getString(cursorSectorGrupoClienteMaterial
								    .getColumnIndexOrThrow("knrmm"));
						    String bonificableQuantity = cursorSectorGrupoClienteMaterial
							    .getString(cursorSectorGrupoClienteMaterial
								    .getColumnIndexOrThrow("knrnm"));
						    String unitMeasureQuantity = cursorSectorGrupoClienteMaterial
							    .getString(cursorSectorGrupoClienteMaterial
								    .getColumnIndexOrThrow("knrme"));
						    String bonificatedQuantity = cursorSectorGrupoClienteMaterial
							    .getString(cursorSectorGrupoClienteMaterial
								    .getColumnIndexOrThrow("knrzm"));
						    String unitMeasureBonificated = cursorSectorGrupoClienteMaterial
							    .getString(cursorSectorGrupoClienteMaterial
								    .getColumnIndexOrThrow("knrez"));
						    String matAdcBonificable = cursorSectorGrupoClienteMaterial
							    .getString(cursorSectorGrupoClienteMaterial
								    .getColumnIndexOrThrow("knrmat"));
						    cursorSectorGrupoClienteMaterial.close();

						    String offer = "De: " + minimunQuantity + ": " + bonificableQuantity
							    + " " + unitMeasureQuantity + " + " + bonificatedQuantity
							    + " " + unitMeasureBonificated;

						    if (matAdcBonificable.length() > 0)
							product.setKnrmat(matAdcBonificable);

						    if (product.getOffer() == null)
						    {
							product.setOffer(offer.trim());
							break;
						    } else
						    {
							break;
						    }
						}
					    }
					} catch (Exception e)
					{
					    e.printStackTrace();
					} finally
					{
					    if (cursorSectorGrupoClienteMaterial != null)
					    {
						cursorSectorGrupoClienteMaterial.close();
					    }
					}

				    } else if (currentBonusSequence == 3)
				    {
					Cursor cursorJerarquiaProductoMaterial = db
						.rawQuery(
							"select knrmm, knrnm, knrme, knrzm, knrez, knrmat from conditions"
								+ " where kschl = '" + conditionType + "'"
								+ " and vkorg = '" + client.getOrganizacion() + "'"
								+ " and vtweg = '" + client.getCanal() + "'"
								+ " and sequence = '" + currentBonusSequence + "'"
								+ " and spart like '%" + client.getSector() + "%'"
								+ " and PRODH like '%" + product.getProdh() + "%'"
								+ " and KUNNR like '%" + product.getMaterial() + "%'",
							null);

					try
					{
					    if (cursorJerarquiaProductoMaterial != null)
					    {
						for (cursorJerarquiaProductoMaterial
							.moveToFirst(); !cursorJerarquiaProductoMaterial
								.isAfterLast(); cursorJerarquiaProductoMaterial
									.moveToNext())
						{
						    String minimunQuantity = cursorJerarquiaProductoMaterial
							    .getString(cursorJerarquiaProductoMaterial
								    .getColumnIndexOrThrow("knrmm"));
						    String bonificableQuantity = cursorJerarquiaProductoMaterial
							    .getString(cursorJerarquiaProductoMaterial
								    .getColumnIndexOrThrow("knrnm"));
						    String unitMeasureQuantity = cursorJerarquiaProductoMaterial
							    .getString(cursorJerarquiaProductoMaterial
								    .getColumnIndexOrThrow("knrme"));
						    String bonificatedQuantity = cursorJerarquiaProductoMaterial
							    .getString(cursorJerarquiaProductoMaterial
								    .getColumnIndexOrThrow("knrzm"));
						    String unitMeasureBonificated = cursorJerarquiaProductoMaterial
							    .getString(cursorJerarquiaProductoMaterial
								    .getColumnIndexOrThrow("knrez"));
						    String matAdcBonificable = cursorJerarquiaProductoMaterial
							    .getString(cursorJerarquiaProductoMaterial
								    .getColumnIndexOrThrow("knrmat"));

						    String offer = "De: " + minimunQuantity + ": " + bonificableQuantity
							    + " " + unitMeasureQuantity + " + " + bonificatedQuantity
							    + " " + unitMeasureBonificated;

						    if (matAdcBonificable.length() > 0)
							product.setKnrmat(matAdcBonificable);

						    if (product.getOffer() == null)
						    {
							product.setOffer(offer.trim());
							break;
						    } else
						    {
							break;
						    }
						}
					    }
					} catch (Exception e)
					{
					    e.printStackTrace();
					} finally
					{
					    if (cursorJerarquiaProductoMaterial != null)
					    {
						cursorJerarquiaProductoMaterial.close();
					    }
					}

				    } else if (Integer.valueOf(currentBonusSequence) == 4)
				    {
					Cursor cursorClienteMaterial = db
						.rawQuery(
							"select knrmm, knrnm, knrme, knrzm, knrez, knrmat from conditions"
								+ " where kschl = '" + conditionType + "'"
								+ " and vkorg = '" + client.getOrganizacion() + "'"
								+ " and vtweg = '" + client.getCanal() + "'"
								+ " and sequence = '" + currentBonusSequence + "'"
								+ " and spart like '%" + client.getSector() + "%'"
								+ " and BRSCH like '%" + client.getRamo() + "%'"
								+ " and matnr like '%" + product.getMaterial() + "%'",
							null);

					try
					{
					    if (cursorClienteMaterial != null)
					    {
						for (cursorClienteMaterial.moveToFirst(); !cursorClienteMaterial
							.isAfterLast(); cursorClienteMaterial.moveToNext())
						{
						    String minimunQuantity = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrmm"));
						    String bonificableQuantity = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrnm"));
						    String unitMeasureQuantity = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrme"));
						    String bonificatedQuantity = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrzm"));
						    String unitMeasureBonificated = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrez"));
						    String matAdcBonificable = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrmat"));
						    cursorClienteMaterial.close();

						    String offer = "De: " + minimunQuantity + ": " + bonificableQuantity
							    + " " + unitMeasureQuantity + " + " + bonificatedQuantity
							    + " " + unitMeasureBonificated;

						    if (matAdcBonificable.length() > 0)
							product.setKnrmat(matAdcBonificable);

						    if (product.getOffer() == null)
						    {
							product.setOffer(offer.trim());
							break;
						    } else
						    {
							break;
						    }
						}
					    }
					} catch (Exception e)
					{
					    e.printStackTrace();
					} finally
					{
					    if (cursorClienteMaterial != null)
					    {
						cursorClienteMaterial.close();
					    }
					}

				    } else if (currentBonusSequence == 5)
				    {
					Cursor cursorClienteMaterial = db
						.rawQuery(
							"select knrmm, knrnm, knrme, knrzm, knrez, knrmat from conditions"
								+ " where kschl = '" + conditionType + "'"
								+ " and vkorg = '" + client.getOrganizacion() + "'"
								+ " and vtweg = '" + client.getCanal() + "'"
								+ " and sequence = '" + currentBonusSequence + "'"
								+ " and spart like '%" + client.getSector() + "%'"
								+ " and BRSCH like '%" + client.getRamo() + "%'"
								+ " and PRODH like '%" + product.getProdh() + "%'",
							null);

					try
					{
					    if (cursorClienteMaterial != null)
					    {
						for (cursorClienteMaterial.moveToFirst(); !cursorClienteMaterial
							.isAfterLast(); cursorClienteMaterial.moveToNext())
						{
						    String minimunQuantity = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrmm"));
						    String bonificableQuantity = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrnm"));
						    String unitMeasureQuantity = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrme"));
						    String bonificatedQuantity = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrzm"));
						    String unitMeasureBonificated = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrez"));
						    String matAdcBonificable = cursorClienteMaterial.getString(
							    cursorClienteMaterial.getColumnIndexOrThrow("knrmat"));
						    cursorClienteMaterial.close();

						    String offer = "De: " + minimunQuantity + ": " + bonificableQuantity
							    + " " + unitMeasureQuantity + " + " + bonificatedQuantity
							    + " " + unitMeasureBonificated;

						    if (matAdcBonificable.length() > 0)
							product.setKnrmat(matAdcBonificable);

						    if (product.getOffer() == null)
						    {
							product.setOffer(offer.trim());
							break;
						    } else
						    {
							break;
						    }
						}
					    }
					} catch (Exception e)
					{
					    e.printStackTrace();
					} finally
					{
					    if (cursorClienteMaterial != null)
					    {
						cursorClienteMaterial.close();
					    }
					}

				    }
				}

			    } else if (conditionType.equalsIgnoreCase("KA00"))
			    {
				Cursor cursorSequence = db.rawQuery(
					"select sequence from conditions where vkorg = '" + client.getOrganizacion()
						+ "' " + "and vtweg = '" + client.getCanal() + "' and kschl = '"
						+ conditionType + "' group by sequence order by sequence asc",
					null);

				try
				{
				    if (cursorSequence != null)
				    {
					sequences.clear();
					for (cursorSequence.moveToFirst(); !cursorSequence.isAfterLast(); cursorSequence
						.moveToNext())
					{
					    String sequence = cursorSequence
						    .getString(cursorSequence.getColumnIndexOrThrow("sequence"));
					    sequences.add(Integer.valueOf(sequence));
					}
				    }
				} catch (Exception e)
				{
				    e.printStackTrace();
				} finally
				{
				    if (cursorSequence != null)
				    {
					cursorSequence.close();
				    }
				}

				for (Integer currentDiscountSequence : sequences)
				{
				    if (currentDiscountSequence == 1)
				    {
					Cursor cursorClienteMaterial = db
						.rawQuery("select kbetr from conditions where vkorg = '"
							+ client.getOrganizacion() + "' " + "and vtweg = '"
							+ client.getCanal() + "' and kschl = '" + conditionType
							+ "' and sequence = '" + currentDiscountSequence + "' "
							+ "and kunnr like '%" + client.getKunnr()
							+ "%' and matnr like '%" + product.getMaterial() + "%'"
							+ " order by sequence asc", null);

					while (cursorClienteMaterial.moveToNext())
					{
					    String kbetr = cursorClienteMaterial
						    .getString(cursorClienteMaterial.getColumnIndexOrThrow("kbetr"));
					    cursorClienteMaterial.close();

					    if (product.getDiscount() == null)
					    {
						product.setDiscount(kbetr);
						break;
					    } else
					    {
						break;
					    }
					}
				    } else if (currentDiscountSequence == 2)
				    {
					Cursor cursorSectorRamoMaterial = db
						.rawQuery("select kbetr from conditions where vkorg = '"
							+ client.getOrganizacion() + "' " + "and vtweg = '"
							+ client.getCanal() + "' and kschl = '" + conditionType
							+ "' and sequence = '" + currentDiscountSequence + "' "
							+ "and spart like '%" + client.getSector()
							+ "%' and brsch like '%" + client.getRamo()
							+ "%'  and matnr like '%" + product.getMaterial() + "%'"
							+ " order by sequence asc", null);

					while (cursorSectorRamoMaterial.moveToNext())
					{
					    String kbetr = cursorSectorRamoMaterial
						    .getString(cursorSectorRamoMaterial.getColumnIndexOrThrow("kbetr"));
					    cursorSectorRamoMaterial.close();

					    if (product.getDiscount() == null)
					    {
						product.setDiscount(kbetr);
						break;
					    } else
					    {
						break;
					    }
					}
				    } else if (currentDiscountSequence == 3)
				    {
					Cursor cursorMaterial = db
						.rawQuery("select kbetr from conditions where vkorg = '"
							+ client.getOrganizacion() + "' " + "and vtweg = '"
							+ client.getCanal() + "' and kschl = '" + conditionType
							+ "' and sequence = '" + currentDiscountSequence + "' "
							+ "and matnr like '%" + product.getMaterial() + "%'"
							+ " order by sequence asc", null);

					while (cursorMaterial.moveToNext())
					{
					    String kbetr = cursorMaterial
						    .getString(cursorMaterial.getColumnIndexOrThrow("kbetr"));
					    cursorMaterial.close();

					    if (product.getDiscount() == null)
					    {
						product.setDiscount(kbetr);
						break;
					    } else
					    {
						break;
					    }
					}
				    }
				}

			    }
			}
		    }
		} catch (Exception e)
		{
		    e.printStackTrace();
		} finally
		{
		    if (cursorConditionType != null)
		    {
			cursorConditionType.close();
		    }

		}

		return product;
	    }
	    
	
	public HashMap<String, ProductEntry> getSelections() {
		return selections;
	}
		
	public List<ProductEntity> getProducts() {
		return products;
	}
		
	private class ViewHolder{
		ImageView thumb;
		TextView name;
		TextView id;
		CheckBox checkbox;
		TextView unit;
		TextView price;
		TextView discount;
		TextView offer;			
		TextView stock;
		EditText amount;		
	}
		
	@SuppressWarnings("serial")
	public static class ProductEntry  implements Serializable{
//		public int index;
		public boolean selected;
		public String amount;
		
//		public ProductEntry( int index,boolean selected,String amount ){
//			this.index = index;
//			this.selected = selected;
//			this.amount = amount;
//		}
		
		public ProductEntry( boolean selected,String amount ){
//			this.index = index;
			this.selected = selected;
			this.amount = amount;
		}
		
		
//		@Override
//		public String toString() {
//			return "{idx:"+index+",ck:"+selected+",am:"+amount+"}";
//		}
	}

	public void setCachedProducts(List<ProductEntity> products)
	{
	    this.cachedProducts = products;
	}
	
	
}

package com.lynxsales.wakeup.activities.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lynx.sales.bundle.entities.ClientEntity;
import com.lynxsales.wakeup.R;
import com.lynxsales.printer.Configuration;

import java.util.ArrayList;
import java.util.List;

public class AdapterListViewClient extends BaseAdapter {

    private Activity activity;
    private List<ClientEntity> clients;
    private TextView         textViewNewClientId,
    textViewNewClientSociety,
    textViewNewClientInterlocutor,
    textViewNewClientPhone,
    textViewNewClientCreationDate,
    textViewNewClientStatus,
    textViewNewClientName,
    textViewNewClientAddress,
    textViewNewClientLastname;

    public List<ClientEntity> getClients()
    {
        return clients;
    }

    public void setClients(List<ClientEntity> clients)
    {
        this.clients = clients;
    }

    public AdapterListViewClient(Activity activity) {
        this.activity = activity;
        this.clients = new ArrayList<ClientEntity>();
        Configuration.getInstance().configureSharePreference(activity);
    }

    public View getView(int position, View view, ViewGroup viewGroup)
    {
        LayoutInflater layoutInflater = (LayoutInflater)  activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View product_item = layoutInflater.inflate(R.layout.adapter_client, null);

        textViewNewClientId = (TextView) product_item.findViewById(R.id.textViewNewClientId);
        textViewNewClientSociety = (TextView) product_item.findViewById(R.id.textViewNewClientSociety);
        textViewNewClientInterlocutor = (TextView) product_item.findViewById(R.id.textViewNewClientInterlocutor);
        textViewNewClientPhone = (TextView) product_item.findViewById(R.id.textViewNewClientPhone);
        textViewNewClientCreationDate = (TextView) product_item.findViewById(R.id.textViewNewClientCreationDate);
        textViewNewClientStatus = (TextView) product_item.findViewById(R.id.textViewNewClientStatus);
        textViewNewClientName = (TextView) product_item.findViewById(R.id.textViewNewClientName);
        textViewNewClientAddress = (TextView) product_item.findViewById(R.id.textViewNewClientAddress);
        textViewNewClientLastname = (TextView) product_item.findViewById(R.id.textViewNewClientLastname);
        
        ClientEntity client = clients.get(position);

        textViewNewClientId.setText( client.getId() );
        textViewNewClientSociety.setText( client.getName() );
        textViewNewClientInterlocutor.setText( client.getInterlocutor() );
        textViewNewClientPhone.setText( client.getPhone() );
        textViewNewClientCreationDate.setText( client.getTodayDate() );
        textViewNewClientStatus.setText( client.getStatus() );
        textViewNewClientName.setText( client.getContactName() );
        textViewNewClientAddress.setText( client.getAddress1() );
        textViewNewClientLastname.setText( client.getContactLastName() );
       
        return product_item;
    }

    public int getCount() {
        return clients.size();
    }

    public Object getItem(int position) {
        return clients.get(position);
    }

    public long getItemId(int id) {
        return id;
    }



}

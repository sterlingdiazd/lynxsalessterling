package com.lynxsales.wakeup.activities.adapter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.lynx.sales.bundle.dto.model.ItineraryDataModel;
import com.lynx.sales.bundle.dto.model.UserRetrieval;
import com.lynx.sales.bundle.entities.ItineraryEntity;
import com.lynx.sales.bundle.entities.RouteEntity;
import com.lynx.sales.bundle.entities.RouteEntry;
import com.lynxsales.wakeup.utils.AlertDialogManager;
import com.lynxsales.wakeup.R;

public class RouteAdapter extends ArrayAdapter<RouteEntity>
{

    private int resource;
    private LayoutInflater inflater;
    private List<RouteEntity> routes;
    private List<RouteEntity> cachedRoutes;
    private Activity activity;

    public RouteAdapter(Activity activity, int resource, List<RouteEntity> entries)
    {
	super(activity, resource, entries);
	this.activity = activity;
	this.resource = resource;
	this.routes = entries;

	cachedRoutes = new ArrayList<RouteEntity>();
	cachedRoutes.addAll(this.routes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

	if (inflater == null)
	    inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	TextView id = null;
	TextView name = null;
	TextView createdBy = null;
	TextView createdAt = null;
	ViewHolder holder = null;

	if (convertView == null)
	{
	    convertView = inflater.inflate(resource, parent, false);

	    id = (TextView) convertView.findViewById(R.id.id_client_num);
	    name = (TextView) convertView.findViewById(R.id.name);
	    createdBy = (TextView) convertView.findViewById(R.id.created_by);
	    createdAt = (TextView) convertView.findViewById(R.id.created_at);

	    holder = new ViewHolder(id, name, createdBy, createdAt);
	    convertView.setTag(holder);

	} else
	{
	    holder = (ViewHolder) convertView.getTag();
	    id = holder.id;
	    name = holder.name;
	    createdBy = holder.createdBy;
	    createdAt = holder.createdAt;
	}

	RouteEntity entry = routes.get(position);

	id.setText(entry.getLazzyClientId());
	name.setText(entry.getLazzyClientName());
	createdBy.setText(entry.getCreatedBy());
	createdAt.setText(entry.getCreatedDate());

	return convertView;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public Filter getFilter()
    {
	return new Filter()
	{
	    @SuppressWarnings("unchecked")
	    @Override
	    protected void publishResults(CharSequence constraint, FilterResults results)
	    {
		if (TextUtils.isEmpty(constraint.toString()))
		    return;

		routes.clear();
		List<RouteEntity> values = (List<RouteEntity>) results.values;

		if (values.size() > 0)
		{
		    routes.addAll(values);
		}
		else
		{
		    routes = cachedRoutes;
		}
		notifyDataSetChanged();
	    }

	    @Override
	    protected FilterResults performFiltering(CharSequence constraint)
	    {
		FilterResults results = new FilterResults();

		List<RouteEntity> filtered = new ArrayList<RouteEntity>();
		constraint = constraint.toString().toLowerCase();

		if(activity != null)
		{
		    List<ItineraryEntity> itineraries = ItineraryDataModel.getSellerItineraries(UserRetrieval.getUser(activity).getId(), activity, 0, 0);
		    List<RouteEntity> routes = ItineraryDataModel.getRoutesByIdOrName(activity, itineraries.get(0).getId(), constraint.toString());
		    filtered.addAll(routes);
		}
		results.count = filtered.size();
		results.values = filtered;
		return results;		
	    }
	};
    }

    private class ViewHolder
    {
	public TextView id;
	public TextView name;
	public TextView createdBy;
	public TextView createdAt;

	public ViewHolder(TextView id, TextView name, TextView createdBy, TextView createdAt)
	{
	    this.id = id;
	    this.name = name;
	    this.createdBy = createdBy;
	    this.createdAt = createdAt;
	}
    }

}

package com.lynxsales.wakeup.activities.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.lynxsales.wakeup.R;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob.Status;
import com.lynx.sales.bundle.widget.TextView;

public class ResponseLogAdapter extends ArrayAdapter<ScheduleJob >{

	private Context context ;
	private int resource;
	private LayoutInflater inflater;
	private List<ScheduleJob> pendings;
	
	
	public ResponseLogAdapter(Context context, int resource,List<ScheduleJob> objects) {
		super(context, resource, objects);
		this.context = context;
		this.pendings = objects;
		this.resource=  resource;
	}

	
	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {		
		if( null == inflater ){
			inflater = ( LayoutInflater ) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );			
		}
		
		ViewHolder holder = null;
		if( null == convertView ){
			convertView = inflater.inflate( resource, parent, false );
			holder = new ViewHolder();
			holder.responsible 	= (TextView) convertView.findViewById( R.id.responsible );
			holder.status 		= (TextView) convertView.findViewById( R.id.status );
			holder.type			= (TextView) convertView.findViewById( R.id.type );
			holder.date			= (TextView) convertView.findViewById( R.id.date );
			convertView.setTag( holder );			
		}else{
			holder = ( ViewHolder ) convertView.getTag(); 
		}
		
		ScheduleJob job = pendings.get( position );
		holder.responsible.setText( job.getResponsible() );
		
		if( Status.PROCCESED == job.getStatus() )
			holder.status.setTextColor( android.R.color.holo_green_dark );
		if( Status.REJECTED == job.getStatus() )
			holder.status.setTextColor( android.R.color.holo_orange_dark );
		if( Status.WARNING == job.getStatus() )
			holder.status.setTextColor( R.color.yellow);
		
		holder.status.setText( job.getStatus().toString() );
		holder.type.setText( job.getType().toString() );
		holder.date.setText(job.getDate() );
		
		return convertView;
	}
	
	private class ViewHolder{
		/*package*/ TextView responsible;
		/*package*/ TextView status;
		/*package*/ TextView type;
		/*package*/ TextView date;		
	}
}

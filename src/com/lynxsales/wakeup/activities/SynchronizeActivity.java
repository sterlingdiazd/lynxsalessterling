package com.lynxsales.wakeup.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import com.lynxsales.wakeup.activities.bprocess.ItineraryActivity;
import com.lynxsales.wakeup.noapi.request.SynchronizationDelegate;
import com.lynxsales.wakeup.R;
import com.lynx.sales.bundle.app.LynxApplication;
import com.lynx.sales.bundle.dto.prefs.UserPreferences;
import com.lynx.sales.bundle.dto.provider.agent.ScheduleJob;
import com.lynx.sales.bundle.entities.UserEntity;
import com.lynx.sales.bundle.http.request.ErrorStub;
import com.lynx.sales.bundle.http.request.PhaseHandler;
import com.lynx.sales.bundle.http.request.Requestor;
import com.lynx.sales.bundle.http.request.ResponseStub;
import com.lynx.sales.bundle.http.request.SynchronizeRequest;
import com.testflightapp.lib.TestFlight;



public class SynchronizeActivity extends Activity{
	
	public static final String EXTRA_REENTRANT_SYNC = "com.lynx.sales.activities.SynchronizeActivity.EXTRA_REENTRANT_SYNC";
	
	private TextView feedback;
	private ScheduleJob job;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView( R.layout.activity_synchronize );		
		setFinishOnTouchOutside( false );
		
		Bundle bundle = (Bundle) getIntent().getExtras();
		if(bundle != null)
			job = (ScheduleJob) bundle.getSerializable("jobs");
		
		init();
	}
	
	
	private void init()
	{		
		feedback = (TextView ) findViewById( R.id.sync );
		try {
			downloadData();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	private void downloadData()
	{	
		SynchronizeRequest.registerDelegate( new SynchronizationDelegate(this));
		
		//boolean isReSync = getIntent().getBooleanExtra( EXTRA_REENTRANT_SYNC, false );	
		boolean isReSync = true;	
		
		Requestor req = null;
		
		if(job != null)
		{
			req = new SynchronizeRequest(this,isReSync,job, handler);
		} else {
			req = new SynchronizeRequest(this,isReSync,handler);
		}
		LynxApplication app = ( LynxApplication ) getApplication();
		UserEntity entity = app.getUser();
		
		/**
		 * esto ocurre cuando la aplicacion es detenida, antes de terminar el proceso de sinchronziacion, en cuyo 
		 * caso trata de restaurarse con la session del ultimo usuario que se logeo.  
		 */
		
		String userId = null;
		
		if( null == entity )
			userId = UserPreferences.getUserLogged( this );
		else
			userId = entity.getId();
		
		req.request( null,userId );
	}
	
	
	
	private void passActivity() 
	{
		TestFlight.passCheckpoint("Synchronization executed.!");
	    
		UserPreferences.setSynchronized(this, true );
		
		(( LynxApplication )getApplication()).reloadUser();
		
		Intent intent = new Intent(this, ItineraryActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
	
	
	private  final Handler osHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			feedback.setText( msg.obj.toString() );
		}
	};
	
	
	private final PhaseHandler handler = new PhaseHandler() {		
		@Override
		public void onError(ErrorStub stub) 
		{
			Log.e( "$$$$$$$$$$$$$$$$", "Error: "+stub.getReason() );
			SynchronizeActivity.this.finish();
		}
		
		@Override
		public void onCompleted(ResponseStub response) {
			passActivity();
		}
		
		@Override
		public void onBegan() {			
		}
		
		public void onFeedback(String feed) {
			osHandler.sendMessage( osHandler.obtainMessage(1, feed) );			
		};
	};
}

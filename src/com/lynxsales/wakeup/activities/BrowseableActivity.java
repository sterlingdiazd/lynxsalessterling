package com.lynxsales.wakeup.activities;

import com.lynxsales.wakeup.R;
import com.testflightapp.lib.TestFlight;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;

public class BrowseableActivity extends Activity{
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView( R.layout.browseable_layout );
	
		attendBrowseIntent();
	}
	
	
	
	private void attendBrowseIntent(){
		Uri data = getIntent().getData();
		String frName = null;
		
		if( data != null )
			frName = data.getLastPathSegment();
		
		if( frName != null ){
			Fragment fragment = getFragmentByClassName( frName );
			
			FragmentTransaction trans = getFragmentManager().beginTransaction();
			
			trans.setCustomAnimations( R.animator.fade_in, R.animator.fade_out );
			trans.replace( R.id.content, fragment);
			trans.commit();
		}
	}
	
	
	
	private Fragment getFragmentByClassName( String fname  ){
		Fragment fragment = null;
		if( fname != null ){
			try{
				fragment =  Fragment.instantiate(this, fname);
			}catch( Exception ie ){				
				TestFlight.sendCrash(0, "Not possible to instantiate "+fname, ie.getMessage() );
				TestFlight.log( "Not possible to instantiate"+fname );
				
				ie.printStackTrace();
			}
		}
		return fragment;
	}
	
}

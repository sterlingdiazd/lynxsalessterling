package com.lynxsales.wakeup.activities;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.ViewFlipper;

import com.lynx.sales.bundle.mn.connection.ConnectionListener;
import com.lynx.sales.bundle.mn.connection.ConnectionState;
import com.lynxsales.wakeup.R;

public abstract class BaseActivity extends Activity implements
		ConnectionListener {

	protected ConnectionState state = ConnectionState.DISCONNECTED;
	private ViewFlipper flipper;
	protected DrawerLayout drawerLayout;

	@Override
	protected void onStart() {
		super.onStart();
		// ConnectionManager.initWatch(this);
		// ConnectionManager.registerConenctionListener(this);
	}
	

	@Override
	protected void onPause() {
		super.onPause();
//		ConnectionManager.stopWatch(this);
//		ConnectionManager.unregisterConnectionListener(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return false;
		}
	}

	protected void fadeFlipView(int flipperId, int child) {
		if (flipper == null) {
			flipper = (ViewFlipper) findViewById(flipperId);
			flipper.setInAnimation(this, android.R.anim.fade_in);
			flipper.setOutAnimation(this, android.R.anim.fade_out);
		}
		flipper.setDisplayedChild(child);
	}

	protected void initLogoAnimation() {
		animateViewAnimator(R.id.logo_name, R.animator.rotate_xaxis_fadein);
		animateViewAnimator(R.id.logo_arrow, R.animator.fade_in_logo);
		animateViewAnimator(R.id.logo_pointer,
				R.animator.translate_yaxix_fadein);
	}

	protected void animateViewAnimator(int view, final int anim) {

		final View target = findViewById(view);

		if (target == null)
			return;

		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Animator set = (Animator) AnimatorInflater.loadAnimator(
						BaseActivity.this, anim);
				set.setTarget(target);
				set.start();
			}
		});
	}

	@Override
	public void onConnectionChange(ConnectionState state) {
		this.state = state;
	}

}

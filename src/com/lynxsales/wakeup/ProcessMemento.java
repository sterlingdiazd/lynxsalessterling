package com.lynxsales.wakeup;

import static com.lynx.sales.bundle.dto.model.Constants.ARCHIVE_TYPE;
import static com.lynx.sales.bundle.dto.model.Constants.DATE;
import static com.lynx.sales.bundle.dto.model.Constants.ID;
import static com.lynx.sales.bundle.dto.model.Constants.OWNER;
import static com.lynx.sales.bundle.dto.model.Constants.STATE;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.lynx.sales.bundle.provider.SalesContract;


public class ProcessMemento {

	public static int save( Context context, ContentValues values ){
		ContentResolver resolver = context.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "archives" ).build();
		Uri uriInserted = resolver.insert(uri, values);
		return Integer.valueOf( uriInserted.getLastPathSegment() );
	}
	
	public static void update( Context context, ContentValues values, int idArchive ){
		ContentResolver resolver = context.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "archives" ).appendPath( String.valueOf( idArchive ) ).build();	
		resolver.update(uri, values, "id = ?", new String[]{ String.valueOf( idArchive ) });
		
	}
	
	public static String restore( Context context,int idArchive ){
		String state = null;
		
		 ContentResolver resolver = context.getContentResolver();
		 Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "archives" ).appendPath( String.valueOf( idArchive ) ).build();	
		 
		 String[] projection = { ID,ARCHIVE_TYPE,OWNER,STATE, DATE };
		 try {
			 Cursor cursor = resolver.query(uri, projection,  null, null, null);
			 if( cursor.moveToFirst() ){
				 /**
				  * Needed only one state to client;
				  */
				 state = cursor.getString( cursor.getColumnIndexOrThrow( "state" ) );						
			 }
			 cursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return state;
	}
	
	public static void delete( Context context,int idArchive ){
		ContentResolver resolver = context.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "archives" ).appendPath( String.valueOf( idArchive )).build();
		resolver.delete( uri, null, null);
	}
	
}
